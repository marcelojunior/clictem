class Sms::ContactSerializer < ActiveModel::Serializer
  attributes :id, :name, :phone, :cellphone, :birthday
  has_one :user
end
