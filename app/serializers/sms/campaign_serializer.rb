class Sms::CampaignSerializer < ActiveModel::Serializer
  attributes :id, :message, :link, :contacts_count, :schedule_date, :schedule_time
end
