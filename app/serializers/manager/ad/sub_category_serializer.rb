class Manager::Ad::SubCategorySerializer < ActiveModel::Serializer
  attributes :id, :name, :category_id, :department_id

  def department_id
    object.category.department_id
  end
end
