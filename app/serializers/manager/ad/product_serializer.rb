class Manager::Ad::ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :price

  def price
    if object.on_request
        0.0
    else
        object.price
    end
  end
end
