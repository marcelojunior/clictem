class Manager::Ad::CategorySerializer < ActiveModel::Serializer
  attributes :id, :name, :department_id
  # has_many :sub_categories
end
