class Manager::Ad::DepartmentSerializer < ActiveModel::Serializer
  attributes :id, :name, :icon, :on_menu
  has_many :categories
  has_many :brands

  def icon
    "icons-#{object.slug}"
  end
end
