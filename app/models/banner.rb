class Banner < ActiveRecord::Base
  mount_uploader :image_topo, BannerTopoUploader
  mount_uploader :image_menu, BannerTopoUploader
  mount_uploader :image_user, BannerUserUploader

  belongs_to :user
  belongs_to :banner_type
  belongs_to :department, class_name: Manager::Ad::Department

  scope :custom, -> { where('image_user is not null') }
  scope :published, -> { where('start_date <= ? and end_date >= ?', Date.today.beginning_of_day, Date.today.end_of_day) }

  def value_monthly(date)
    value = self.banner_type.value.to_f
    # value_by_day = value / date.end_of_month.day
    # if self.start_date.to_date > date.beginning_of_month
    #   value = ((date.end_of_month - self.start_date.to_date).to_i + 1) * value_by_day
    # end

    (value - discount).to_f
  end
end
