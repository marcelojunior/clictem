class State < ActiveRecord::Base
  has_many :cities
  has_many :products, class_name: Manager::Ad::Product
end
