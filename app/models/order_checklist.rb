class OrderChecklist < ActiveRecord::Base
  belongs_to :order
  scope :completed, -> { where(done: true) }
  scope :open, -> { where(done: false) }
  default_scope -> { order(raw_order: :asc) }
end
