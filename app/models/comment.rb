class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :product, class_name: Manager::Ad::Product
  belongs_to :question, class_name: Comment, foreign_key: 'question_id'
  has_many :anwsers, class_name: Comment, foreign_key: 'question_id'
  belongs_to :seller, class_name: User, foreign_key: 'seller_id'

  scope :only_comments, -> { where(question_id: nil) }

  # É um comentário ou uma resposta?
  def is_anwser?
  	question_id != nil
  end


  # Total de comentários em um período
  def self.total_comments(user, date1, date2)
    Comment.where(seller_id: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).count
  end
end
