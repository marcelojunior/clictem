class City < ActiveRecord::Base
  belongs_to :state
  has_many :products, class_name: Manager::Ad::Product

  default_scope { order(:name) }
end
