class Customer < ActiveRecord::Base
    belongs_to :user
    belongs_to :owner, class_name: User, foreign_key: :owner

    # has_and_belongs_to_many :groups
    has_many :customers_groups
    has_many :groups, through: :customers_groups

    validates :name, presence: true

    include PgSearch
    pg_search_scope :search, against: [:name, :phone, :cellphone, :email],
                  ignoring: :accents,
                  using: {
                    tsearch: { prefix: true, dictionary: "portuguese"},
                    trigram: { only: [:name] }
                  }

    scope :birthday_today, -> { where('EXTRACT(DAY FROM birthday) = ? and EXTRACT(MONTH FROM birthday) = ?', Date.today.day, Date.today.month) }
    scope :birthday_month, -> { where('EXTRACT(MONTH FROM birthday) = ?', Date.today.month).order("extract(day from birthday) ASC") }
    scope :by_group, ->(group) { joins(:groups).where(groups: { id: group }) }


    # Retorna o primeiro nome do contato
    def first_name
        self.name.split(' ')[0]
    end


    # Retorna o estágio do funil
    def funnel
      self.groups.funnel.first
    end



    # Cria lead na base
    def self.create_lead(seller, email, options={})
      customer = seller.customers.where(email: email).first

      if options[:funnel]
        funnel = seller.groups.funnel.where(level: options[:funnel]).first
      else
        funnel = seller.groups.funnel.where(level: 1).first
      end


      # Cria o cliente
      if customer.blank?
        customer = Customer.new(email: email)
        customer.owner = seller
        customer.groups << funnel
      else # Atualiza cliente
        if options[:funnel]
          customer.groups.delete(customer.groups.funnel.first.id) if customer.groups.funnel.first
          customer.groups << funnel
        end
      end

      # Grupo
      if options[:groups]
        options[:groups].split(',').each do |group|
          group = seller.groups.where(name: group).first_or_create

          if !customer.groups.map{|g| g.id}.include?(group.id)
            customer.groups << group
          end

        end
      end



      customer.name      = options[:name] if options[:name]
      customer.cellphone = options[:cellphone] if options[:cellphone]
      customer.phone     = options[:phone] if options[:phone]
      customer.company   = options[:company] if options[:company]
      customer.genre   = options[:genre] if options[:genre]
      customer.birthday  = Date.parse(options[:birthday]) if options[:birthday]
      customer.save

      seller.customers.where(email: email).first
    end



end
