class BannerType < ActiveRecord::Base
    mount_uploader :placeholder, ImageUploader
    has_many :banners
    has_many :orders
end
