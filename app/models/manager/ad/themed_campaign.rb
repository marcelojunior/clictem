class Manager::Ad::ThemedCampaign < ActiveRecord::Base
    mount_uploader :img_bg, ImageUploader
    mount_uploader :img_3,  ImageUploader
    mount_uploader :img_7,  ImageUploader

    validates :subdomain, uniqueness: true

    has_many :campaigns
    accepts_nested_attributes_for :campaigns, reject_if: :all_blank, allow_destroy: true
    has_and_belongs_to_many :users, class_name: 'User'

    usar_como_dinheiro :value

    # Todos os anúnciantes da campanha
    def sellers
        ids = self.campaigns.pluck(:user_id).uniq
        User.where(id: ids)
    end


    # Verifica se a campanha está dento do período
    def is_valid?
        self.date_start <= Date.today and self.date_end >= Date.today
    end


    # Adiciona ou remove produtos da campanha
    def toggle_product_campaign(product)
        campaigns = self.campaigns.where(product_id: product)
        if campaigns.present?
          campaigns.delete_all
          return :remove
        else
          self.campaigns.create(product_id: product)
          return :add
        end
    end
end
