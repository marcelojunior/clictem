class Manager::Ad::OfferCampaign < ActiveRecord::Base
  belongs_to :user
  has_many :offers, dependent: :destroy
  accepts_nested_attributes_for :offers, reject_if: proc { |attributes| attributes['product_id'].blank? }, allow_destroy: true

  scope :all_offers, -> { where('date_start <= :date and date_end >= :date', date: Date.today) }

  validates :title, presence: true


  # Retorna todos os dias da semana
  def wdays
    days = []
    7.times do |i|
      days << I18n.t(:"date.day_names")[i] if self.send("wday#{i}")
    end
    days.join(', ')
  end



  # Verifica se é uma oferta válida para o dia
  def is_valid?
    if self.repeat?
      if self.send("wday#{Date.today.wday}") == true
        true
      else
        false
      end
    elsif self.date_start <= Date.today and self.date_end >= Date.today
      true
    else
      false
    end
  end



  # Atualiza informações das ofertas com a campanha
  def update_offers
    self.offers.each do |offer|
      offer.date_start = self.date_start
      offer.date_end   = self.date_end
      offer.repeat     = self.repeat
      # offer.has_cupom  = self.has_cupom
      7.times do |i|
        offer.send("wday#{i}=", self.send("wday#{i}"))
      end
      offer.save
      offer.product.update_offer
    end
  end
end
