class Manager::Ad::Brand < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders, :history]

  include PgSearch
  pg_search_scope :search, against: [:name],
                  ignoring: :accents,
                  using: { tsearch: { prefix: true, dictionary: "portuguese"} }

  # has_and_belongs_to_many :departments
  has_many :brands_departments
  has_many :departments, through: :brands_departments

  has_many :products

  validates :name, presence: true
end
