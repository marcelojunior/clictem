class Manager::Ad::Image < ActiveRecord::Base
  belongs_to :product

  mount_base64_uploader :file, ProductUploader

  default_scope { order(:id) }
end
