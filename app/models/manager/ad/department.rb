class Manager::Ad::Department < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders, :history]

  validates :name, presence: true, uniqueness: { case_sensitive: false }

  # has_and_belongs_to_many :brands
  has_many :brands_departments
  has_many :brands, through: :brands_departments

  has_many :categories, dependent: :restrict_with_exception
  has_many :products, dependent: :restrict_with_exception
  has_and_belongs_to_many :users, class_name: 'User'
  has_and_belongs_to_many :users_premium, class_name: 'User', join_table: 'departments_users_premium'
  has_many :posts, class_name: ::Blog::Post

  default_scope { order('manager_ad_departments.raw_order ASC, manager_ad_departments.name') }

  usar_como_dinheiro :banner_value

  scope :on_menu, -> {
    includes(:categories)
    .where("manager_ad_categories.on_menu = true")
    .references(:categories)
    .where(on_menu: true)
    .limit(8)
  }

  scope :with_products, -> { where("manager_ad_categories.products_count > 0") }
  scope :home, -> { where(is_home: true) }
  # Retorna um hash com os departamentos mais visualizados
  def self.most_view(date1, date2)
    date1       = date1.beginning_of_day
    date2       = date2.end_of_day
    deps        = []
    days_offset = 0

    while deps.size < 4 and days_offset <= 60 do
      ids = Visit.where(created_at: ((date1 - days_offset.days)..date2))
              .where(ad_deleted: false).select(:department_id)
              .group(:department_id)
              .order(count: :desc)
              .count

      ids.each do |id|
        if id[0]
          deps << Manager::Ad::Department.find(id[0])
        end
      end

      days_offset += 30
    end
    deps
  end

  # retorna os deptos
  def self.on_menu_cached
    Rails.cache.fetch("departments_on_menu", :expires_in => 7.days) do
      Manager::Ad::Department.on_menu
    end
  end

  def slug_candidates
    [
      :name,
      [:name, :id]
    ]
  end

end
