class Manager::Ad::SubCategory < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders, :history]

  belongs_to :category
  has_many :products

  default_scope { order('raw_order ASC, name') }

  scope :on_menu, -> { where(on_menu: true).limit(5) }

  validates :category_id, presence: true

  def slug_candidates
    [
      :name,
      [:name, :id]
    ]
  end
end
