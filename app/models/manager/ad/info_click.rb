class Manager::Ad::InfoClick < ActiveRecord::Base
  belongs_to :product
  belongs_to :user

  # Total de cliques em ver telefone em um período
  def self.total_clicks(user, date1, date2)
    Manager::Ad::InfoClick.where(seller_id: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).count
  end
end


