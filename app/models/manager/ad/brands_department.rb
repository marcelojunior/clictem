class Manager::Ad::BrandsDepartment < ActiveRecord::Base
  belongs_to :brand
  belongs_to :department
end
