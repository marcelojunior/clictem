class Manager::Ad::Offer < ActiveRecord::Base
  belongs_to :product
  belongs_to :user
  belongs_to :offer_campaign, class_name: Manager::Ad::OfferCampaign

  has_many :cupoms

  scope :all_offers, -> { where('date_start <= :date and date_end >= :date', date: Date.today) }

  default_scope { order(:id) }

  usar_como_dinheiro :price

  # Total de ofertas criadas em um período
  def self.total_offers(user, date1, date2)
    Manager::Ad::Offer.where(user_id: user.id)
      .where('date_start <= :date1 and date_end >= :date2',
        date1: date1.beginning_of_day, date2: date2.end_of_day).count
  end
end
