class Manager::Ad::Campaign < ActiveRecord::Base
  belongs_to :user
  belongs_to :themed_campaign
  belongs_to :product

  before_save do
    self.user_id = self.product.user_id
  end


end
