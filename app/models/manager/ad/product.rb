class Manager::Ad::Product < ActiveRecord::Base
  acts_as_paranoid

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders, :history]

  include PgSearch
  pg_search_scope :search, against: [:name, :description],
                  :associated_against => {
                    user: [:name]
                  },
                  ignoring: :accents,
                  using: {
                    tsearch: { prefix: true, dictionary: "portuguese"},
                    trigram: { only: [:name] }
                  }

  paginates_per 24

  belongs_to :department, counter_cache: true
  belongs_to :category, counter_cache: true
  # belongs_to :sub_category, counter_cache: true
  belongs_to :user, counter_cache: true
  belongs_to :brand
  belongs_to :state
  belongs_to :city
  belongs_to :offer, class_name: Manager::Ad::Offer, foreign_key: 'current_offer_id'


  has_many :interests, dependent: :delete_all
  has_many :info_clicks, dependent: :delete_all
  has_many :comments, dependent: :delete_all
  has_many :cupoms
  has_many :offers, dependent: :delete_all
  accepts_nested_attributes_for :offers, reject_if: proc { |attributes| attributes['price'].blank? or attributes['price'].to_f <= 0 }, allow_destroy: true
  has_many :images, dependent: :delete_all
  accepts_nested_attributes_for :images, reject_if: :all_blank, allow_destroy: true

  # ALTERAR PARA HAS_MANY THROUGH
  # has_and_belongs_to_many :themed_campaigns


  delegate :slug, to: :department, prefix: true
  delegate :name, to: :department, prefix: true
  delegate :slug, to: :category, prefix: true
  # delegate :slug, to: :sub_category, prefix: true
  delegate :kind, to: :user, prefix: true
  delegate :name, to: :user, prefix: true
  delegate :id, to: :user, prefix: true
  delegate :subdomain, to: :user, prefix: true
  delegate :last_name, to: :user, prefix: true

  validates :category, presence: true
  validates :images, presence: true
  validates :name, length: { maximum: 100 }
  validate :valid_offer_price
  # validates :name, presence: true, on: :update

  scope :ordered, -> { order([:raw_order, created_at: :desc]) }
  scope :published, -> { where(published: true).joins(:user).where( users: { cancel_date: nil, plan: [ 'top', 'basico'] } ) }
  scope :services, -> { where(is_service: true) }
  scope :all_offers, -> { where(is_offer: true) }
  scope :all_cupoms, -> { where(has_cupom: true) }
  scope :valid_offers, -> { where(is_offer: true).where('offer_start <= :date and offer_end >= :date', date: Date.today) }
  scope :only_sellers, -> { joins("inner join users u on manager_ad_products.user_id = u.id and u.plan != 'free'") }
                  
  mount_uploader :image1, ProductUploader
  mount_uploader :image2, ProductUploader
  mount_uploader :image3, ProductUploader
  mount_uploader :image4, ProductUploader

  usar_como_dinheiro :price, :offer_price

  before_destroy do
    Visit.where(ad: self.id).update_all(ad_deleted: true)
  end


  after_create do

    # Torna o usuáio um vendedor
    user = self.user
    user.seller = true
    user.save
  end


  # Total de anúncios publicados em um período
  def self.total_publish(user, date1, date2)
    Manager::Ad::Product.published.where(user_id: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).count
  end


  # Calcula a porcentagem de desconto
  def percentage_off
    percentage = 0
    if price > 0 and offer_price > 0
      percentage = (Float(price.to_f - offer_price.to_f) / price.to_f * 100).ceil
    end
    percentage
  end



  # Resposta para o upload
  def to_jq_upload(image)
    {
      "name" => read_attribute(image),
      "size" => self.send("#{image}").size,
      "url" => self.send("#{image}_url", :list),
      "delete_url" => "/documents/#{id}",
      "delete_type" => "DELETE"
    }
  end



  # Imagem de destaque
  def main_image
    self.images.first
  end



  # Verifica se a oferta ainda é válida
  def offer_valid?
    if self.is_offer?
      self.offer_start <= Date.today and
      self.offer_end >= Date.today and
      self.price > self.offer_price and
      self.offer_price > 0 and
      !self.sold?
    else
      false
    end
  end



  # Verificar se o produto está com todos os campos cadastrados
  # Ainda não coloquei como validates para não interferir na
  # tela de cadastro.
  def is_valid?
    !name.blank? and image1?
  end



  # Retorna um hash com os produtos mais visualziados
  def self.most_view(date1, date2, department, limit = 20)
    date1       = date1.beginning_of_day
    date2       = date2.end_of_day
    prods       = []
    days_offset = 0

    while prods.size < 4 and days_offset <= 60 do
      ids = Visit.where(created_at: ((date1 - days_offset.days)..date2), department_id: department)
              .where(ad_deleted: false).limit(limit).select(:ad).group(:ad)
              .order(count: :desc)
              .count

      ids.each do |id|
        if id[0]
          prods << Manager::Ad::Product.find(id[0])
        end
      end
      days_offset += 30
    end
    prods
  end



  def description
    if super.blank?
      if user.city
        return "#{name} | #{user.name} em #{user.city.name}"
      else
        return "#{name} | #{user.name}"
      end
    else
      return super
    end

  end



  def description_strip
    ActionController::Base.helpers.strip_tags(self.description.gsub('<br/>', ' ').gsub('<br>', ' ').gsub('&nbsp;', ' ').gsub('</p>', ' ').gsub('&trade;',' ').gsub('</li>', ' '))
  end


  # gera o slug a partir dessa lógia
  def slug_candidates
    [
      :name,
      [:name, :id_for_slug],
    ]
  end

  def id_for_slug
    SecureRandom.hex(3)
  end


  # vai gerar novamente o SLUG quando alterar o nome do produto
  def should_generate_new_friendly_id?
    name_changed? || super
  end



  # Atualiza os valores e data de oferta do anúncio
  def update_offer
    self.is_offer = false
    self.has_cupom = false
    self.offers.where('date_start <= :date and date_end >= :date', date: Date.today).each do |offer|
      if offer.repeat?
        if offer.send("wday#{Date.today.wday}") == true
          build_offer(offer)
        end
      else
        build_offer(offer)
      end
    end
    self.save
  end



  # Salva a oferta
  def build_offer(offer)
    self.current_offer_id = offer.id
    self.offer_start      = offer.date_start
    self.offer_end        = offer.date_end
    self.offer_price      = offer.price
    self.has_cupom        = offer.has_cupom
    self.is_offer         = true
    self.cupom_limit      = offer.cupom_limit
    self.cupom_note       = offer.cupom_note
    self.has_cupom_limit  = offer.has_cupom_limit
  end



  # Atualiza informações sobre o cupom de desconto
  def update_cupom
    self.cupom_generated = self.cupoms.size
    self.save
  end



  # Verifica se o cupom é válido
  def cupom_valid?
    self.has_cupom and self.offer_valid?
  end



  # Criar versões das imagens
  def create_images
    self.images.where(waiting: true).each do |image|
      begin
        tmp = Manager::Ad::ImageTemp.where(token: image.token).first

        if tmp
          image.base64 = tmp.base64
          image.file = tmp.base64
          image.waiting = false

          image.save!
          tmp.destroy
        end
      rescue
        next
      end
    end
  end

  # Retorna um produto aleatório por cliente
  def self.client_produtcs
    prods = []
    clients = User.clients

    clients.each do |client|
      ids = client.products.published.pluck(:id)
      product = client.products.published.where(id: ids.shuffle.first).first
      if product && product.main_image.file?
        prods << product
      end
    end

    prods
  end



  protected



  # Preço de oferta não pode ser maior que preço normal
  def valid_offer_price
    if offer_price.to_f > price.to_f
      errors.add(:offer_price, "não pode ser maior que o campo Preço")
    end
  end



end
