class Manager::Ad::Cupom < ActiveRecord::Base
  belongs_to :user
  belongs_to :product, -> { with_deleted }
  belongs_to :seller, class_name: User
  belongs_to :offer

  validates :user, uniqueness: { scope: :product_id }

  after_create :set_info

  # Cupons válidos
  scope :valid, -> { where('due >= ? or due is null', Date.today) }

  # Emitidos nos últimos 30 dias
  scope :last_month, -> { where(created_at: ((Time.now - 30.days)..Time.now)) }

  usar_como_dinheiro :price, :offer_price

  include PgSearch
  pg_search_scope :search, against: [:code],
                  :associated_against => {
                    :user => [:name],
                    :product => [:name]
                  },
                  ignoring: :accents,
                  using: { tsearch: { prefix: true} }

  delegate :fullname, to: :user, prefix: true, allow_nil: true
  delegate :fullname, to: :seller, prefix: true, allow_nil: true
  delegate :name, to: :product, prefix: true, allow_nil: true


  # Verifica se cupom é válido
  def is_valid?
    if self.due
      self.due >= Date.today and self.used? == false
    else
      true
    end
  end


  # Total de cupons emitidos em um período
  def self.total_cupons(user, date1, date2)
    Manager::Ad::Cupom.where(seller_id: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).count
  end


  protected

  def set_info
    self.price       = self.product.price
    self.offer_price = self.product.offer_price
    self.code        = Digest::SHA1.hexdigest("#{self.id}#{Time.zone.now.to_s}")[8..16]
    self.save
  end
end
