class Manager::Ad::Category < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders, :history]

  belongs_to :department
  has_many :products, dependent: :restrict_with_exception

  default_scope { order([:raw_order, :name]) }

  scope :on_menu, -> { where(on_menu: true).where('products_count > 0') }

  def slug_candidates
    [
      :name,
      [:name, :id]
    ]
  end

  def url
    "#{department.slug}/categoria/#{slug}"
  end
end
