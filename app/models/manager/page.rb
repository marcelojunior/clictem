class Manager::Page < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  validates :title, presence: true, uniqueness: { case_sensitive: false }
end
