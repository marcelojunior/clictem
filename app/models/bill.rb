class Bill < ActiveRecord::Base
  belongs_to :user
  belongs_to :order
  belongs_to :sms_buy


  # Total a pagar para Clictem
  def total_clictem
    commission = self.commission / 100
    self.value.to_f - (self.value.to_f * commission)
  end



  # Total a pagar para whitelabel
  def total_whitelabel
    commission = self.commission / 100
    self.value.to_f * commission
  end


  # Gera as cobranças do mês.
  def Bill.generate_bills(date = Date.today)
    clients = User.where('cancel_date is null or cancel_date <= ?', date.end_of_month)
                  .where("plan != 'free'").where(roles_mask: nil)


    clients.each do |client|
      generate_bill_plan(client, date)
      generate_sms(client, date)
      generate_banners(client, date)
    end
    generate_campaigns(date)
  end


  # Gera a cobrança de plano do cliente
  def self.generate_bill_plan(user, date)
    if (!user.cancel? or
      (user.cancel_date.month == date.month and user.cancel_date.year == date.year)) and
      user.plan != 'banner'
      Bill.create(
        user: user,
        order_id: nil,
        value: user.value_monthly(date),
        commission: current_org.commission_plan,
        due: date,
        note: "Plano mensal #{user.label_plan}",
        discount: user.discount,
        discount_type: user.discount_type
      )

      if user.premium
        Bill.create(
          user: user,
          order_id: nil,
          value: WHITELABEL_PLAN_2_VALUE,
          commission: 0,
          due: date,
          note: "Parceiro de sucesso"
        )
      end
    end
  end

  # Gera as cobranças dos SMS excedentes
  def self.generate_sms(user, date)
    total = user.sms_total_balance(date)
    note = "#{total} SMSs enviado no período"

    if total > 0
      Bill.create(
        user: user,
        order_id: nil,
        value: (total * WHITELABEL_PLAN_0_SMS.to_f),
        commission: 0,
        due: date,
        note: note
      )
    end
  end

  # Gera a cobrança em cima dos banners contratados
  def self.generate_banners(user, date)
    start_date = date.end_of_month + 1.day
    end_date = date.beginning_of_month - 1.day
    banners = user.banners.where('start_date < ? and end_date > ?',
              start_date, end_date)

    banners.each do |banner|
      tipo = banner.banner_type.name
      note = "Banner do tipo #{tipo}"
      Bill.create(
        user: user,
        order_id: nil,
        value: banner.value_monthly(date),
        commission: current_org.commission_banner,
        due: date,
        note: note,
        discount: banner.discount,
        discount_type: banner.discount_type
      )
    end
  end

  # Gera a cobrança das campanhas participantes
  def self.generate_campaigns(date)
      campaigns = Manager::Ad::ThemedCampaign.where('date_start <= :date and date_end >= :date', date: date)
      campaigns.each do |campaign|
        campaign.sellers.each do |seller|
          Bill.create(
            user: seller,
            order_id: nil,
            value: campaign.value.to_f,
            due: date,
            note: "Campanha:  #{campaign.title}"
          )
        end
      end


  end


  private

  def self.current_org
    Organization.find_by_domain(Apartment::Tenant.current)
  end
end
