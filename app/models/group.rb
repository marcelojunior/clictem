class Group < ActiveRecord::Base
  belongs_to :user

  # has_and_belongs_to_many :customers
  has_many :customers_groups
  has_many :customers, through: :customers_groups

  # ALTERAR PARA HAS_MANY THROUGH
  # has_and_belongs_to_many :sms_campaigns, class_name: 'Sms::Campaign', foreign_key: 'campaign_id'

  validates :name, presence: true

  scope :funnel,    -> { where(funnel: true).order(:level) }
  scope :no_funnel, -> { where(funnel: false) }

  # Níveis do funil
  # Contato: 1
  # Oportunidade: 2
  # Cliente: 3

  def self.customers(user, group_ids, with_cellphone = false)
    if group_ids == 'all'
        customers = user.customers
    else
        customers = user.customers.joins(:groups).where('customers_groups.group_id' => group_ids).uniq
    end

    if with_cellphone
      customers = customers.where("cellphone is not null and cellphone != ''")
    end

    customers
  end



end
