class Order < ActiveRecord::Base
    belongs_to :user
    belongs_to :banner_type
    has_many :order_checklists, dependent: :delete_all
    accepts_nested_attributes_for :order_checklists

    validates :user, presence: true
    validates :note, presence: true

    usar_como_dinheiro :value


    # Criar checklist
    def create_checklist(current_org)
        if self.kind == 'Plano'
            current_org.order_plan_checklist.each do |list|
                done = (self.user.associated_code.present? == true and list[:name] == 'Associar a CDL')
                self.order_checklists.create(name: list[:name], resp: list[:resp], raw_order: list[:order], done: done)
            end
        else
            current_org.order_banner_checklist.each do |list|
                done = (self.user.associated_code.present? == true and list[:name] == 'Associar a CDL')
                self.order_checklists.create(name: list[:name], resp: list[:resp], raw_order: list[:order], done: done)
            end
        end
    end



    # Diferença de meses entre inicio e final do contrato
    def months
        (self.date_end.year * 12 + self.date_end.month) - (self.date_start.year * 12 + self.date_start.month)
    end



    # Caso o pedido seja feito no mês, é calculado o valor proporsional
    def relative_value(date)
        if date.month == Date.today.month and date.year == Date.today.year
            value_day = self.value / 30
            remaining_days = (Date.today.end_of_month - Date.today).to_i
            if remaining_days == 0
                self.value
            else
                self.value + (self.value / remaining_days)
            end

        else
            self.value
        end
    end



    # Listar ordens em aberto
    def self.open_orders
        orders = OrderChecklist.open.pluck(:order_id).uniq
        Order.where(id: orders)
    end

end
