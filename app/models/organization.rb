class Organization < ActiveRecord::Base
  belongs_to :city
  belongs_to :state

  after_create :create_tenant

  serialize :order_plan_checklist
  serialize :order_banner_checklist

  # Verifica se a organização logada é Clictem
  def clictem?
    self.name.downcase == 'clictem'
  end

  # Método estático para verificar se a organização logada é o clictem
  def self.clictem?
    Organization.find_by_domain(Apartment::Tenant.current).name.downcase == 'clictem'
  end

  # Verifica se a organização é Clictem e se o usuário é free
  def clictem_free?(current_user)
    clictem? or !current_user.free?
  end

  private

    def create_tenant
      Apartment::Tenant.create(domain)
    end
end
