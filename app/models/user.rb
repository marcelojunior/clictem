class User < ActiveRecord::Base
  attr_accessor :show_phone
  DISCOUNT_TYPES =  [:clictem, :cdl]
  require 'roo'
  require 'roo-xls'

  acts_as_paranoid
  include Sms::UserModule

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders, :history]

  include PgSearch
  pg_search_scope :search,
                  against: { name: 'A', business_name: 'B' },
                  ignoring: :accents,
                  using: {
                    tsearch: { prefix: true, dictionary: "portuguese", any_word: true },
                    trigram: { only: [:name] }
                  }


  attr_accessor :skip_validates # Não validar campos quando esta propriedade estiver preenchida

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise  :database_authenticatable, :registerable, :timeoutable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => [:facebook] # , :confirmable

  belongs_to :city
  belongs_to :state
  belongs_to :department, class_name: Manager::Ad::Department
  belongs_to :mall, class_name: User, foreign_key: 'mall_id'
  has_many :offer_campaigns, class_name: Manager::Ad::OfferCampaign
  has_many :products, class_name: Manager::Ad::Product, dependent: :destroy
  has_many :comments, class_name: Comment, foreign_key: 'seller_id', dependent: :destroy
  has_many :my_comments, class_name: Comment, foreign_key: 'user_id', dependent: :destroy
  has_many :banners
  has_many :notifications
  has_many :customers, class_name: Customer, foreign_key: :owner
  has_many :offers, class_name: Manager::Ad::Product, dependent: :destroy
  has_many :groups, class_name: Group
  has_many :sms_campaigns, class_name: Sms::Campaign
  has_many :sms_messages, class_name: Sms::Message
  has_many :sms_buys, class_name: Sms::Buy
  has_many :campaigns, class_name: Manager::Ad::Campaign, dependent: :destroy
  accepts_nested_attributes_for :campaigns, reject_if: :all_blank, allow_destroy: true
  has_many :my_cupoms, class_name: Manager::Ad::Cupom, foreign_key: :user_id
  has_many :cupoms, class_name: Manager::Ad::Cupom, foreign_key: :seller_id
  has_many :visits, class_name: Visit, foreign_key: :owner
  has_and_belongs_to_many :departments, class_name: 'Manager::Ad::Department'
  has_and_belongs_to_many :departments_premium, class_name: 'Manager::Ad::Department', join_table: 'departments_users_premium'
  has_and_belongs_to_many :themed_campaigns, class_name: 'Manager::Ad::ThemedCampaign'

  validates :name, :email, presence: true
  validates :email, uniqueness: true

  # Validação para pessoa física - Somente na atualização
  validates :last_name, :genre, :birthday, :cellphone,
    presence: true, on: :update, if: Proc.new { |user| user.kind == 'pf' }, unless: :skip_validates

  # Validação para pessoa jurídica - Somente na atualização
  validates :business_name,
    presence: true, on: :update, if: Proc.new { |user| user.kind == 'pj' }, unless: :skip_validates

  # Validação de endereço - Somente na atualização
  validates :city_id, presence: true, on: :update, unless: :skip_validates

  mount_uploader :logo, UserUploader
  mount_uploader :bg_image, BackgroundUploader
  mount_uploader :stamp, StampUploader

  scope :premium, -> { where(premium: true).order('RANDOM()') }
  scope :essencial, -> { where(plan: 'top') }
  scope :sellers, -> { where(seller: true, cancel_date: nil) }
  scope :actives, -> { where(active: true, premium: true, cancel_date: nil).order(priority: :desc) }
  scope :clients, -> { where("plan != 'free'").where(roles_mask: nil, cancel_date: nil) }
  scope :clients_and_associate, -> { where("plan != 'free' or associated_code <> ''").where(roles_mask: nil, cancel_date: nil) }
  scope :associate, -> { where("associated_code <> ''").where(roles_mask: nil, cancel_date: nil) }

  delegate :name, to: :city, prefix: true, allow_nil: true
  delegate :symbol, to: :state, prefix: true, allow_nil: true

  usar_como_dinheiro :deal_suggest_plan_value,
                     :deal_suggest_banner_value,
                     :discount

  ROLES = %w[admin moderator author banned]
  DEAL_LEVELS = ['Stand By', 'Visita', 'Negociação', 'Recusado', 'Cliente']
  PLANS = [ 'top', 'basico', 'free', 'banner']

  before_create :create_subdomain, :default_limit
  after_create :send_welcome, :create_group
  before_save :set_priority

  after_initialize do |user|
    user.sms_hired_due ||= Date.today if new_record?
  end


  def roles=(roles)
    self.roles_mask = (roles & ROLES).map { |r| 2**ROLES.index(r) }.inject(0, :+)
  end



  def roles
    ROLES.reject do |r|
      ((roles_mask.to_i || 0) & 2**ROLES.index(r)).zero?
    end
  end

  # Exibe o telefone no site. Se não tiver fone comercial, aparece o celular
  def show_phone
    if self.phone.to_s.empty?
      return self.cellphone
    else
      return self.phone
    end
  end

  def fullname
    the_name = self.name
    the_name = "#{self.name} #{self.last_name}" if self.pf?
    the_name
  end


  def is?(role)
    roles.include?(role.to_s)
  end


  # Define se o usuário é cliente ou não
  def is_client?
    self.deal_level == 'Cliente'
  end

  # Verifica o plano
  def free?
    self.plan == 'free' or cancel?
  end

  def basico?
    self.plan == 'basico'
  end

  def pf?
    self.kind == 'pf'
  end

  def pj?
    self.kind == 'pj'
  end

  def associado?
    return !self.associated_code.to_s.empty?
  end

  # Pega o tipo de desconto pelo simbolo
  def get_discount_type(type)
    i = 0
    DISCOUNT_TYPES.each do |dt|
      if dt == type
        return i
      else
        i += 1
      end
    end
  end


  # Exibe o nome do plano
  def label_plan
    case self.plan
      when ('free' or cancel?) then 'Grátis'
      when 'basico' then 'Básico'
      when 'top' then 'Top'
      when 'banner' then 'Somente Banner'
      else 'Grátis'
    end
  end

  # Valores dos planos
  def value_plan
    if !self.free?
      return User.plan_value(self.plan)
    elsif self.free? and self.associado?
      return WHITELABEL_PLAN_3_VALUE.to_f
    else
      return 0.0
    end
  end

  # Valor do parceiro de sucesso
  def value_premium
    if self.premium
       return WHITELABEL_PLAN_2_VALUE.to_f
    else
      return 0.0
    end
  end

  # Valor do parceiro de sucesso para departamento
    def value_premium_dept
      if self.premium
         return WHITELABEL_PLAN_4_VALUE.to_f
      else
        return 0.0
      end
    end

  # Valor total de SMS enviado no mês
  def sms_value(date = Date.today)
    enviados = self.sms_total_balance(date)
    return (enviados * WHITELABEL_PLAN_0_SMS.to_f).to_f
  end

  # Valores dos banners contratados no mês
  def banner_value(date, banner_type)
      start_date = date.beginning_of_month
      end_date = date.end_of_month
      banners = self.banners.where('start_date < ? and end_date > ?',start_date, end_date)
                           .where(banner_type_id: banner_type.id)
      if banners and banners.count > 0
        return banners.count * banner_type.value.to_f
      else
        return 0.0
      end
  end

  # Permuta em plano
  def discount_plan(type)
    if self.discount_type == get_discount_type(type)
      return self.discount
    else
      return 0.0
    end
  end

  # Total de permutas em banners
  def discount_banners(date, type)
    start_date = date.beginning_of_month
    end_date = date.end_of_month
    banners = self.banners.where('start_date < ? and end_date > ?',start_date, end_date)
                          .where(discount_type: get_discount_type(type))

    if banners and banners.count > 0
      return banners.sum(:discount).to_f
    else
      return 0.0
    end
  end

  def self.plan_value(plan_name)
    case plan_name
      when ('free' or cancel?) then 0.0
      when 'basico' then WHITELABEL_PLAN_0_VALUE.to_f
      when 'top' then WHITELABEL_PLAN_1_VALUE.to_f
      when 'premium' then WHITELABEL_PLAN_2_VALUE.to_f
      # when 'total' then WHITELABEL_PLAN_3_VALUE.to_f
    end
  end

  # Atualiza as informações do usuário quando ele se cadastra pelo Facebook
  def self.from_omniauth(auth)
    user = where('email = ? or uid = ?', auth.info.email, auth.uid).first_or_create do |user|
      user.provider  = auth.provider
      user.uid       = auth.uid
      user.fb_token  = auth.credentials.token
      user.email     = auth.info.email
      user.password  = Devise.friendly_token[0,20]
      user.name      = auth.info.first_name
      user.last_name = auth.info.last_name
    end

    unless user.fb_token
      user.fb_token = auth.credentials.token
      user.save
    end

    user
  end



  # Resposta para o upload
  def to_jq_upload(image)
    {
      "name" => read_attribute(image),
      "size" => self.send("#{image}").size,
      "url" => self.send("#{image}_url", :thumb),
      "delete_url" => "/documents/#{id}",
      "delete_type" => "DELETE"
    }
  end

  # Verifica se tem site cadastrado
  def has_website
    self.website != nil and !self.website.strip.empty?
  end

  # Inclui o http no site se não tiver
  def website_com_http
    if self.website != nil and !self.website.empty? and !self.website.include? 'http'
      "http://#{self.website}"
    else
      self.website
    end
  end



  # Gerar código aleatório para subdominio de usuário
  def self.generate_subdomain
    sub = "anunciante#{rand(1234..9876)}"
    while User.first_by_subdomain(sub) != nil
      sub = "anunciante#{rand(1234..9876)}"
    end
    sub
  end

  # Verifica se o subdominio é "anunciante"
  def subdominio_anunciante
    self.subdomain.include? 'anunciante'
  end



  # Buscar usuário pelos dois campos do subdominio
  def self.first_by_subdomain(subdomain)
    User.where('origin_subdomain = :subdomain or subdomain = :subdomain', subdomain: subdomain).first
  end


  # Login permanente
  def timeout_in
    2.years
  end


  # Impersonate
  def staff?
    self.roles.include? "admin"
    # case self.email
    #   when 'marcelo.machado.jr@gmail.com'  then true
    #   when 'guilherme@clictem.com' then true
    #   when 'sm@mattiolo.com.br' then true
    #   when 'gamattiolo@gmail.com' then true
    #   else
    #     false
    # end
  end



  def slug_candidates
    [
      :subdomain,
      [:subdomain, :id]
    ]
  end



  # Se for um centro comercial, lista todas as áreas cadastradas
  def areas
    users = User.where(mall_id: self.id).pluck(:id) #Usuários que fazem parte deste centro comercial
    users = Manager::Ad::Product.published.where('user_id in (?) or id in (?)', users, self.campaigns.pluck(:product_id)).pluck(:user_id)
    User.where(id: users).pluck(:area).compact.uniq.sort
  end



  # Se for um centro comercial, lista todas as lojas por área
  def stores_by_areas(area)
    users = User.where(mall_id: self.id).pluck(:id) #Usuários que fazem parte deste centro comercial
    users = Manager::Ad::Product.published.where('user_id in (?) or id in (?)', users, self.campaigns.pluck(:product_id)).pluck(:user_id)
    User.where(id: users, area: area).order(:name)
  end



  # Exirou ou não o limite de anúncios publicados
  def expired_limit
    self.products.published.size >= self.ad_limit
  end


  # Ofertas criadas
  def offers
    Manager::Ad::Offer.where(product_id: self.products.pluck(:id))
  end



  # Limite default
  def default_limit
    if Organization.clictem? and self.free?
      limites_clictem
    else
      if self.plan == "basico"
        self.ad_limit = 3
        self.seller = true
        self.plan
      elsif self.plan == "basico"
        self.ad_limit = 10000
        self.seller = true
        self.plan
      else
        self.ad_limit = 0
        self.seller = false
        self.plan
      end
    end
  end

  # Limites para usuário Clictem
  def limites_clictem
    self.ad_limit = 50
    self.seller = true
  end



  # Libera o acesso ao Manager
  def add_admin
    self.roles = ['admin']
    self.save!
  end


  # Bloqueia o acesso ao Manager
  def remove_admin
    self.roles = []
    self.roles_mask = nil
    self.save!
  end



  # Importa arquivo XLS / XLSX
  def import_xls(tmp_path, org_file)
    user = self
    funnel = user.groups.funnel.where(level: 3).first

    type = org_file.split('.').last
    if type == 'xlsx'
      xls = Roo::Excelx.new(tmp_path)
    elsif type == 'xls'
      xls = Roo::Excel.new(tmp_path)
    end

    groups = Hash.new
    6.upto(xls.last_column) do |column|
      category = user.groups.find_by_name(xls.cell(1, column))
      if category
        groups[column] = category
      end
    end

    2.upto(xls.last_row) do |line|
      cellphone = xls.cell(line, 'B').to_s.split('.')[0] || ''
      phone     = xls.cell(line, 'C').to_s.split('.')[0] || ''

      cellphone = cellphone.pretty_phone_with_ddd if cellphone.present?
      phone     = phone.pretty_phone_with_ddd if phone.present?

      contact = user.customers.find_by_cellphone(cellphone)

      unless contact
        $stdout.puts "ADICIONANDO ==== #{cellphone}"
        contact = Customer.new(
          name: xls.cell(line, 'A'),
          cellphone: cellphone,
          phone: phone,
          email: xls.cell(line, 'D'),
        )
        contact.owner = user
        contact.birthday = xls.cell(line, 'E') if xls.cell(line, 'E')

        6.upto(xls.last_column) do |column|
          if groups[column] and xls.cell(line, column) and xls.cell(line, column).upcase == 'X'
            contact.groups << groups[column]
          end
        end

        contact.save
        # contact.groups << funnel
      else
        6.upto(xls.last_column) do |column|
          if groups[column] and xls.cell(line, column) and xls.cell(line, column).upcase == 'X'
            if !contact.groups.include?(groups[column])
              contact.groups << groups[column]
              $stdout.puts "ATUALIZANDO ==== #{cellphone}"
            end
          end
        end
        contact.save
      end
    end
  end


  # Cria acontratação do plano caso o usuário seja diferente de free
  def create_order_plan(current_org)
    if self.plan != 'free'
      order = Order.create(
          user_id: self.id,
          value: self.value_plan,
          installment: 12,
          note: "Aquisição do plano #{self.label_plan}",
          plan: self.plan
      )
      order.create_checklist(current_org)

      Bill.create(
        user: self,
        order: order,
        value: order.relative_value(Date.today),
        commission: WHITELABEL_COMMISSION_PLAN,
        due: Date.today + 1.month,
        note: "Plano mensal #{order.plan} de #{I18n.l(Date.today, format: :short)} até #{I18n.l(Date.today.end_of_month, format: :short)}"
      )
    end
  end



  # Verifica se o usuário está cancelado
  def cancel?
    self.cancel_date != nil
  end

  # Verifica se o cadastro foi feito no mesmo mês
  def new_client?(date)
    self.created_at.month == date.day and self.created_at.year == date.year
  end


  # Calcula o valor mensal do cliente
  def value_monthly(date)
    value = self.value_plan.to_f
    value_by_day = value / date.end_of_month.day
    if self.created_at.to_date > date.beginning_of_month
      value = ((date.end_of_month - self.created_at.to_date).to_i + 1) * value_by_day
    end

    if self.cancel?
      if self.cancel_date.day < 25
        value = ((self.cancel_date - date.beginning_of_month).to_i + 1) * value_by_day
      end
    end

    value
  end

  # Verifica se uma campnha está paga ou não
  def campanha_paga(campanhaId)
    connection = ActiveRecord::Base.connection
    pago = connection.execute("select pago from manager_ad_themed_campaigns_users where user_id = #{self.id} and themed_campaign_id = #{campanhaId}")
    if pago.first == nil
      return true
    else
      return pago.first["pago"] == "t"
    end
  end

  # Altera o status de pagamento de uma campanha
  def campanha_altera_pagamento(campanhaId, status)
    pago = (status ? "t" : "f")
    connection = ActiveRecord::Base.connection
    connection.execute("update manager_ad_themed_campaigns_users set pago = '#{pago}' where user_id = #{self.id} and themed_campaign_id = #{campanhaId}")
  end

  private

  def create_subdomain
    self.origin_subdomain = self.subdomain = User.generate_subdomain
  end


  # Envia e-mail de boas vindas
  def send_welcome
    if Rails.env.production?
      Mails::UserMailer.send("welcome", self).deliver
    end
  end


  # Cria as segmentações do funil
  # def create_funnels
  #   self.groups.create!(name: 'Contato', funnel: true, level: 1)
  #   self.groups.create!(name: 'Oportunidade', funnel: true, level: 2)
  #   self.groups.create!(name: 'Cliente', funnel: true, level: 3)
  # end


  # Cria um grupo de contato padrão
  def create_group
    self.groups.create!(name: 'Contato', funnel: false)
  end


  # Alter a prioridade do usuário
  def set_priority
    case self.plan
      when 'total' then self.priority = 3
      when 'top' then self.priority = 2
      when 'basico' then self.priority = 1
      when ('free' or cancel?) then self.priority = 0
    end
  end




end
