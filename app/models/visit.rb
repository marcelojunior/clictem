class Visit < ActiveRecord::Base
  belongs_to :category
  belongs_to :department
  belongs_to :banner
  # belongs_to :product, class_name: Manager::Ad::Product, foreign_key: :ad
  require 'google_chart'

  # Cores utilizadas nos gráficos
  COLORS = ['FF1A00', '008C00', '4096EE', 'FF0084', '356AA0', '3F4C6B', 'D01F3C']

  # Total de visitas em um período
  def self.total_visits(user, date1, date2)
    Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).count
  end

  # Retorna a URL como gráfico para sexo
  def self.email_chart_gender(user, date1, date2)
    total  = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day), gender: ['Male', 'Female']).count
    male   = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day), gender: 'Male').count
    female = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day), gender: 'Female').count

    url = ''
    GoogleChart::PieChart.new('255x150', "",false) do |pc|
      pc.data "Homens (#{(male.to_f / total.to_f * 100.0).round(2)}%)", male, COLORS[2]
      pc.data "Mulheres (#{(female.to_f / total.to_f * 100.0).round(2)}%)", female, COLORS[6]
      pc.show_labels = false
      pc.show_legend = true
      url = pc.to_url
    end

    url
  end


  # Retorna a URL como gráfico para faixa etária
  def self.email_chart_age(user, date1, date2)
    colors = COLORS.shuffle
    # Todas as idades coletadas no período
    ages = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
                .where("age != ''").select(:age).group(:age).pluck(:age)
    # Total de registros
    total = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
                .where("age != ''").count
    # Armazenar a quandidade de idades, separado por idade
    h_ages = Hash.new
    ages.each do |age|
        h_ages[age] = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day), age: age).count
    end

    url = ''
    GoogleChart::PieChart.new('255x150', "",false) do |pc|
        i = 0
        h_ages.each do |age, sum|
            pc.data "#{Navegg::Translate.age(age.to_s)} (#{(sum.to_f / total.to_f * 100.0).round(2)}%)", sum, colors[i]
            i += 1
        end
        pc.show_labels = false
        pc.show_legend = true
        url = pc.to_url
    end

    url
  end


  # Retorna a URL como gráfico para perfil
  def self.email_chart_cluster(user, date1, date2)
    colors = COLORS.shuffle
    # Total de registros
    total = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
                .where("cluster != ''").count
    if total < 20
      nil
    else
      # Todas as perfis coletadas no período
      clusters = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
                  .where("cluster != ''").select(:cluster)
                  .group(:cluster).order('count_cluster desc')
                  .count(:cluster).map{|k,v| k}

      # Armazenar a quandidade de perfis, separado por idade
      h_clusters = Hash.new
      clusters.each do |cluster|
          h_clusters[cluster] = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day), cluster: cluster).count
      end

      url = ''
      GoogleChart::PieChart.new('255x150', "",false) do |pc|
          i = 0
          h_clusters.each do |cluster, sum|
              pc.data "#{Navegg::Translate.cluster(cluster)} (#{(sum.to_f / total.to_f * 100.0).round(2)}%)", sum, colors[i]
              i += 1
          end
          pc.show_labels = false
          pc.show_legend = true
          url = pc.to_url
      end

      url
    end
  end



  # Retorna URL com gráfico de intenções de compra
  def self.email_chart_product(user, date1, date2)
    colors = COLORS.shuffle
    # Total de registros
    total = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
                .where("product != ''").count

    if total < 20
      nil
    else
      # Todas as intenções de compra coletadas no período
      products = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
                .where("product != ''").select(:product)
                .map{|prod| prod.product.split('-')}.flatten
                .group_by{|x| x}.sort_by{|k, v| -v.size}.map(&:first).uniq[0..4]
      # Armazenar a quandidade de intenções de compra, separado por idade
      h_products = Hash.new
      products.each do |product|
          h_products[product] = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).where("product like ?", "%#{product}%").count
      end

      url = ''
      GoogleChart::PieChart.new('500x150', "",false) do |pc|
          i = 0
          h_products.each do |product, sum|
              pc.data "#{Navegg::Translate.product(product)} (#{(sum.to_f / total.to_f * 100.0).round(2)}%)", sum, colors[i]
              i += 1
          end
          pc.show_labels = false
          pc.show_legend = true
          url = pc.to_url
      end

      url
    end
  end


  # Retorna a URL como gráfico para marcas de interesse
  def self.email_chart_brand(user, date1, date2)
    colors = COLORS.shuffle
    # Total de registros
    total = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
                .where("brand != ''").count

    if total < 20
      nil
    else
      # Todas as marcas coletadas no período
      brands = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
                  .where("brand != ''").select(:brand)
                  .group(:brand).order('count_brand desc')
                  .count(:brand).map{|k,v| k}

      # Armazenar a quandidade de marcas, separado por idade
      h_brands = Hash.new
      brands.each do |brand|
          h_brands[brands] = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day), brand: brand).count
      end

      url = ''
      GoogleChart::PieChart.new('255x150', "",false) do |pc|
          i = 0
          h_brands.each do |brand, sum|
              pc.data "#{brand} (#{(sum.to_f / total.to_f * 100.0).round(2)}%)", sum, colors[i]
              i += 1
          end
          pc.show_labels = false
          pc.show_legend = true
          url = pc.to_url
      end

      url
    end
  end


  # Retorna a URL como gráfico para conteúdos de interesse
  def self.email_chart_interest(user, date1, date2)
    colors = COLORS.shuffle
    # Total de registros
    total = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).where("interest != ''").count

    if total < 20
      nil
    else
      # Todas as perfis coletadas no período
      interests = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
                .where("interest != ''").select(:interest)
                .map{|prod| prod.interest.split('-')}.flatten
                .group_by{|x| x}.sort_by{|k, v| -v.size}.map(&:first).uniq[0..4]

      # Armazenar a quandidade de perfis, separado por idade
      h_interests = Hash.new
      interests.each do |interest|
          h_interests[interest] = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).where("interest like ?", "%#{interest}%").count
      end

      url = ''
      GoogleChart::PieChart.new('255x150', "",false) do |pc|
          i = 0
          h_interests.each do |interest, sum|
              pc.data "#{Navegg::Translate.interest(interest)} (#{(sum.to_f / total.to_f * 100.0).round(2)}%)", sum, colors[i]
              i += 1
          end
          pc.show_labels = false
          pc.show_legend = true
          url = pc.to_url
      end

      url
    end
  end


  # Gênero que mais visualiza um produto
  def self.gender_by_product(product, user, date1, date2)
    male   = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day), gender: 'Male', ad: product).count
    female = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day), gender: 'Female', ad: product).count
    if male > female
      'Masculino'
    elsif female > male
      'Feminino'
    else
      'Ambos'
    end
  end


  # Idade que mais visualiza um produto
  def self.age_by_product(product, user, date1, date2)

    # Todas as idades coletadas no período
    ages = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
                .where("age != ''").where(ad: product).select(:age).group(:age).pluck(:age)

    max_count = 0
    max_age   = ''
    ages.each do |age|
        count = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day), age: age, ad: product.id).count
        if count > max_count
          max_age = age
        end
    end

    Navegg::Translate.age(max_age)
  end


  # Retorna os produtos mais visualizados do período
  def self.ads_most_views(user, date1, date2)
      ads = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
                 .where('ad in (select id from manager_ad_products)').group(:ad).order('count_ad desc').limit(5).count(:ad)

     sm2_date1 = date2 - 7.days
     sm2_date2 = sm2_date1 - 7.days

      ads.map do |key,val|
        product = Manager::Ad::Product.with_deleted.where(id: key)

        if product.present?
          product = product.first
          {
            product: product,
            count: val,
            count_lastweek: Visit.where(owner: user.id, created_at: (sm2_date1.beginning_of_day..sm2_date2.end_of_day), ad: product.id).count,
            gender: Visit.gender_by_product(product, user, date1, date2),
            age: Visit.age_by_product(product, user, date1, date2),
            cupoms: product.cupoms.count
          }
        end
      end.compact
  end


  # Retorna as estatísticas de visualização dos produtos selecionados
  def self.views_products(ids, user, date1, date2)
    ads = Visit.where(ad: ids, created_at: (date1.beginning_of_day..date2.end_of_day))
               .where('ad in (select id from manager_ad_products)').group(:ad).order('count_ad desc').limit(5).count(:ad)
    ads.map do |key,val|
      product = Manager::Ad::Product.find(key)
      {
        product: product,
        count: val,
        gender: Visit.gender_by_product(product, user, date1, date2),
        age: Visit.age_by_product(product, user, date1, date2),
        cupoms: product.cupoms.where(created_at: (date1.beginning_of_day..date2.end_of_day)).count
      }
    end
  end


  # Retorna as estatístias para sexo
  def self.chart_gender(user, date1, date2)
    count = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day), gender: ['Male', 'Female']).group(:gender).count
    translate = {'Male' => 'Masculino', 'Female' => 'Feminino'}
    Hash[count.map {|k, v| [translate[k], v] }]
  end


  # Retorna as estatístias para faixa hetária
  def self.chart_age(user, date1, date2)
    count = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).where("age != ''").group(:age).count

    Hash[count.map {|k, v| [Navegg::Translate.age(k), v] }]
  end


  # Retorna as estatístias para perfil
  def self.chart_cluster(user, date1, date2)
    total = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).where("cluster != ''").count
    count = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).where("cluster != ''").group(:cluster).count.sort {|a1,a2| a2[1]<=>a1[1]}

    Hash[count.map {|k, v| [Navegg::Translate.cluster(k), (v.to_f / total.to_f * 100.0).round(2)] }]
  end


  # Retorna as estatístias para conteúdos de interesse
  def self.chart_interest(user, date1, date2)
    total = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).where("interest != ''").count

    count = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
                .where("interest != ''").select(:interest)
                .map{|prod| prod.interest.split('-')}.flatten
                .group_by{|x| x}.sort_by{|k, v| -v.size}.map(&:first).uniq[0..8]

    h_count = Hash.new
    count.each do |data|
        h_count[data] = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).where("interest like ?", "%#{data}%").count
    end

    Hash[h_count.map {|k, v| [Navegg::Translate.interest(k), (v.to_f / total.to_f * 100.0).round(2)] }]
    # raise
  end



  # Retorna as estatístias para intenções de compra
  def self.chart_product(user, date1, date2)
    total = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).where("product != ''").count
    count = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
                .where("product != ''").select(:product)
                .map{|prod| prod.product.split('-')}.flatten
                .group_by{|x| x}.sort_by{|k, v| -v.size}.map(&:first).uniq[0..8]

    h_count = Hash.new
    count.each do |data|
        h_count[data] = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).where("product like ?", "%#{data}%").count
    end

    Hash[h_count.map {|k, v| [Navegg::Translate.product(k), (v.to_f / total.to_f * 100.0).round(2)] }]
  end



  # Retorna as estatístias para intenções de compra
  def self.chart_brand(user, date1, date2)
    total = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).where("brand != '' and brand != 'None'").count

    count = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
                .where("brand != '' and brand != 'None'").select(:brand)
                .map{|visit| visit.brand.split('-')}.flatten
                .group_by{|x| x}.sort_by{|k, v| -v.size}.map(&:first).uniq[0..8]

    h_count = Hash.new
    count.each do |data|
        next if data =~ /\d{1,2}/

        h_count[data] = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).where("brand like ?", "%#{data}%").count
    end

    Hash[h_count.map {|k, v| [k.titleize, (v.to_f / total.to_f * 100.0).round(2)] }]
  end

end
