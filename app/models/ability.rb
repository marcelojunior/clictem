class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)
    if user.is? :admin
      can :manage, :all
    else
      can :read, :all
    end
  end

  # def initialize(user)
  #     can :read, :all                   # allow everyone to read everything
  #     if user && user.admin?
  #       can :access, :rails_admin       # only allow admin users to access Rails Admin
  #       can :dashboard                  # allow access to dashboard
  #       # if user.role? :admin
  #       #   can :manage, :all             # allow admins to do anything
  #       # elsif user.role? :manager
  #       #   can :manage, [User, Product]  # allow managers to do anything to products and users
  #       # elsif user.role? :sales
  #       #   can :update, Product, :hidden => false  # allow sales to only update visible products
  #       # end
  #     end
  #   end
end
