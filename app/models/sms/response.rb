class Sms::Response < ActiveRecord::Base
	belongs_to :campaign
	belongs_to :user
	belongs_to :message
	belongs_to :customer

	scope :unread, -> { where(read: false) }
end
