class Sms::Campaign < ActiveRecord::Base
    belongs_to :user
    has_many :messages
    has_many :responses

    # ALTERAR PARA HAS_MANY THROUGH
    has_and_belongs_to_many :customers

    # ALTERAR PARA HAS_MANY THROUGH
    has_and_belongs_to_many :groups

    validates :message, presence: true
    # validates :groups, presence: true

    # Prontas para o envio
    scope :ready_to_send, -> { where(status: 'aguardando').where('schedule_date <= ? and schedule_time::time <= ?', Time.zone.now.to_date, Time.zone.now.strftime("%R")) }

    # Criadas nos últimos 30 dias
    scope :last_month, -> { where(created_at: ((Time.now - 30.days)..Time.now)) }

    # Criar as mensagens
    def create_messages
        user = self.user
        Sms::Message.delete_all(campaign_id: self.id)

        # Cria as mensagens
        if self.birthday == false
            contacts = Group.customers(self.user, self.groups.pluck(:id), true)
        else
            contacts = user.customers.birthday_today.where("cellphone is not null and cellphone != ''")
        end

        contacts.each_with_index do |contact, i|
            next if contact.active == false
            balance = i + 1
            message = self.message
            message = Sms::Message.parse_nome(message, contact.first_name)
            Sms::Message.create!(
                user_id: self.user_id,
                campaign_id: self.id,
                contact_id: contact.id,
                text: message,
                status: 'aguardando',
                cellphone: contact.cellphone
            )
        end

        self.status = 'aguardando'
        self.save
    end

    # Envia as campanhas programadas
    def self.send_campaigns
        Sms::Campaign.ready_to_send.each do |campaign|
            campaign.status = 'enviando'
            campaign.save

            campaign.messages.each do |message|
                message.send_sms
            end

            campaign.status = 'concluida'
            campaign.save
        end
    end

end
