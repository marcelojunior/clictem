# encoding: utf-8
class Sms::Message < ActiveRecord::Base
  belongs_to :campaign
  belongs_to :user
  belongs_to :customer, foreign_key: :contact_id

  scope :valid_to_count, -> { where(status: ['aguardando', 'enviada', 'transmitida']) }
  scope :invalid_to_count, -> { where(status: ['invalida', 'vetada', 'erro',
    'blacklist', 'duplicada', 'recusado', 'expirado', 'conta bloqueada']) }

    # Todas as mensagens enviadas para a TWW
  scope :in_tww, -> { where(status: ['enviada', 'transmitida', 'invalida', 'vetada', 'erro',
    'blacklist', 'duplicada', 'recusado', 'expirado', 'conta bloqueada']) }

  scope :this_mounth, -> { where(created_at: (Date.today.beginning_of_month..Date.today.end_of_month)) }
  scope :franchise, -> { where(balance_origin: 'franchise') }
  scope :hired, -> { where(balance_origin: 'hired') }

  TWW_USER = 'clictem'
  TWW_PASS = '16cl05'
  TWW_URL  = 'http://ret.twwwireless.com.br/reluzcap/wsreluzcap.asmx/'

  # Total de mensagens enviadas em um período
  def self.total_messages(user, date1, date2)
    Sms::Message.valid_to_count.where(user_id: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).count
  end

  # Todos os clientes que enviaram mensagens em um período
  def self.total_clients(date1, date2)
    user_ids = Sms::Message.valid_to_count.where(created_at: (date1.beginning_of_day..date2.end_of_day)).pluck(:user_id).uniq
    User.where(id: user_ids)
  end

  # Troca a variável %NOME% pelo primeiro nome do contato
  def self.parse_nome(message, name)
      message = message.gsub('%NOME%', name)
      message = message.gsub('%nome%', name)
      message = message.gsub('%Nome%', name)
      message = message.gsub('%nOme%', name)
      message = message.gsub('%noMe%', name)
      message = message.gsub('%nomE%', name)
      message
  end


  # Envia SMS via TWW
  def send_sms
    if Rails.env.production?
      url = "#{TWW_URL}EnviaSMS"
      uri = URI.parse(url)
      response = Net::HTTP.post_form(uri, {
        'NumUsu' => TWW_USER,
        'Senha' => TWW_PASS,
        'SeuNum' => "C#{self.id}",
        'Celular' => "55#{self.strip_phone}",
        'Mensagem' => self.text.removeaccents
      })
    else
      puts "SMS +++++++++++"
      puts self.text.removeaccents
    end

    self.status = 'transmitida'
    self.save
  end


  # Pega o status da mensagem
  def status_sms
    url = "#{TWW_URL}StatusSMS"

    uri = URI.parse(url)
    response = Net::HTTP.post_form(uri, {
      'NumUsu' => TWW_USER,
      'Senha' => TWW_PASS,
      'SeuNum' => "C#{self.id}"
    })

    if Hash.from_xml(response.body)['DataSet']['diffgram']['OutDataSet']
      Hash.from_xml(response.body)['DataSet']['diffgram']['OutDataSet']['StatusSMS']['status']
    else
      'CL'
    end

    # E0 = Inválido
    # E2 = Vetado
    # OP = Enviado para a operadora
    # Caso "CL"
    # textoStatus = "SMS entregue ao celular de destino"
    # Caso "E0"
    # textoStatus = "Numero de celular invalido/sem operadora"
    # Caso "E1"
    # textoStatus = "Blacklist" ( Celular no blacklist )
    # Case "E3"
    # textoStatus = "Duplicada" ( Mensagem rejeitada por excessivas repetições )
    # Caso "E4"
    # textoStatus = "SMS recusado pela operadora"
    # Caso "E5"
    # textoStatus = "SMS expirado sem informe da operadora"
    # Caso "E6"
    # textoStatus = "SMS expirado conforme informe da operadora"
    # Caso "E7"
    # textoStatus="SMS rejeitado por falta de créditos ou conta bloqueada"
  end

  def converte_status_tww(status)
    if status == 'CL' or status == 'OP'
        self.status = 'enviada'
    elsif status == 'E0'
        self.status = 'invalida'
    elsif status == 'E1'
          self.status = 'blacklist'
    elsif status == 'E2'
        self.status = 'vetada'
    elsif status == 'E3'
        self.status = 'duplicada'
    elsif status == 'E4'
          self.status = 'recusado'
    elsif status == 'E5'
          self.status = 'expirado'
    elsif status == 'E6'
          self.status = 'expirado'
    elsif status == 'E7'
          self.status = 'conta bloqueada'
    else
        self.status = 'erro'
    end
  end


  # Busca todas as respostas do dia
  def self.get_responses(time = Time.zone.now)
    response = nil
    count = 0

    while response == nil and count < 3
      response = tww_response(time)
      dataset = Hash.from_xml(response.body)['DataSet']['diffgram']['OutDataSet']

      count += 1

      sleep 120 unless dataset
    end

    begin
      if dataset
        if dataset['BuscaSMS'].class != Array and dataset['BuscaSMS'].has_key?('seunum')
          self.salva_resposta(dataset['BuscaSMS'])
        else
          dataset['BuscaSMS'].each do |resp|
            self.salva_resposta(resp)
          end
        end
      end
    rescue Exception => e
      puts e
    end

  end


  # Limpa o número do telefone
  def strip_phone
    self.cellphone.gsub(/[\-\(\)\s]*/, '')
  end


  private


  def self.tww_response(time)
    time_start = time.beginning_of_day
    time_end   = time.end_of_day
    url = "#{TWW_URL}BuscaSMSMO"
    uri = URI.parse(url)

    Net::HTTP.post_form(uri, {
      'NumUsu' => TWW_USER,
      'Senha' => TWW_PASS,
      'DataIni' => time_start.to_s,
      'DataFim' => time_end.to_s,
    })
  end

  # Salva a reposta recuperada do servidor
  def self.salva_resposta(resp)
    seunum = resp['seunum'].strip.gsub('C', '')
    uniqid = Digest::SHA1.hexdigest("#{resp['datastatus']}#{seunum}")

    puts "seunum: #{seunum}"
    puts "uniqid: #{uniqid}"

    unless Sms::Response.find_by_uniqid(uniqid)
      msg = Sms::Message.where(id: seunum).first
      if msg
        Sms::Response.create(
          message_id: msg.id,
          customer_id: msg.contact_id,
          user_id: msg.user_id,
          campaign_id: msg.campaign_id,
          text: resp['mensagem'],
          uniqid: uniqid
        )

        if resp['mensagem'].strip.downcase == 'sair'
          customer = msg.customer
          customer.active = false
          customer.save
        end
      end
    end
  end

end
