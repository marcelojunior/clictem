class Sms::Buy < ActiveRecord::Base
  belongs_to :user

  # validates :qtd, numericality: { less_than_or_equal_to: 150 }, presence: true

  def self.calc_value(qtd = 150)
    val1 = 0.20 # Até 499
    val2 = 0.18 # De 500 a 999
    val3 = 0.16 # De 1000 a 1999
    val4 = 0.14 # Acima de 1000

    if qtd <= 499
        qtd * val1
    elsif qtd >= 500 and qtd <= 999
        qtd * val2
    elsif qtd >= 1000 and qtd <= 1999
        qtd * val3
    elsif qtd > 2000
        qtd * val4
    end
  end
end
