module Sms::UserModule


    # Retorna o total de mensagens enviadas
    def sms_total_balance(date = Date.today)
      date_start = date.beginning_of_month
      date_end = date.end_of_month
      self.sms_messages.valid_to_count.where(created_at: (date_start..date_end)).size
    end


    # Retorna o total válido de SMS
    def sms_valid_hired_balance
        if sms_hired_due < Date.today
            0
        else
            sms_hired_balance
        end
    end


    # Total de SMS de aniversário eviados nos útimos 30 dias
    def sms_birthday_last_month
      ids = self.sms_campaigns.last_month.where(birthday: true).pluck(:id)
      Sms::Message.where(campaign_id: ids).count
    end

    
end
