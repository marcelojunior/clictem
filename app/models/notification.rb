class Notification < ActiveRecord::Base
    belongs_to :user

    default_scope { order(id: :desc) }
    scope :opens, -> { where(view: false) }

    def save_email(email)
        self.subject = email.subject
        self.body    = email.body.raw_source
        if email.to.class != String
          self.to = email.to.join(', ')
        else
          self.to = email.to
        end
        self.save
    end
end
