class Blog::Author < ActiveRecord::Base
	has_many :posts

	extend FriendlyId
	friendly_id :slug_candidates, use: [:slugged, :finders, :history]

	mount_uploader :image, BlogUploader

  validates :title, :subtitle, :name, presence: true

	def bio_strip
	  ActionController::Base.helpers.strip_tags(self.bio.gsub('<br/>', ' ').gsub('<br>', ' ').gsub('&nbsp;', ' ').gsub('</p>', ' ').gsub('&trade;',' ').gsub('</li>', ' '))
	end

	def slug_candidates
	    [
	      :title,
	      [:title, :id]
	    ]
  	end
end
