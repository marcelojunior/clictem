class Blog::Post < ActiveRecord::Base
	belongs_to :department, class_name: Manager::Ad::Department
	belongs_to :author

	extend FriendlyId
	friendly_id :slug_candidates, use: [:slugged, :finders, :history]

	mount_uploader :image1, BlogUploader
	mount_uploader :image_fb, BlogUploader

	default_scope -> { order(created_at: :desc) }


	def text_strip
	  ActionController::Base.helpers.strip_tags(self.text.gsub('<br/>', ' ').gsub('<br>', ' ').gsub('&nbsp;', ' ').gsub('</p>', ' ').gsub('&trade;',' ').gsub('</li>', ' '))
	end

	def slug_candidates
	    [
	      :title,
	      [:title, :id]
	    ]
  	end



end
