json.array!(@sms_campaigns) do |sms_campaign|
  json.extract! sms_campaign, :id, :message, :link, :contacts_count, :schedule_date, :schedule_time
  json.url sms_campaign_url(sms_campaign, format: :json)
end
