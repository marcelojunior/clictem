cache "sitemap_cache", expires_in: 24.hours do
  base_url = root_url(subdomain: false)
  xml.instruct! :xml, :version=>'1.0'
  xml.tag! 'urlset', 'xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9' do
    xml.url{
        xml.loc(base_url)
        xml.changefreq("daily")
        xml.priority(1.0)
    }

    # Páginas institucionais
    %w(sobre atendimento como-anunciar politica-de-privacidade termos-e-condicoes-de-uso produtos-e-servicos-ilegais acesso).each do |page|
      xml.url{
          xml.loc("#{base_url}#{page}")
          xml.changefreq("weekly")
          xml.priority(0.8)
      }
    end

    # Anunciantes
    @users.each do |user|
      xml.url {
        xml.loc(user_produtos_url(subdomain: user.subdomain))
        xml.changefreq("daily")
        xml.priority(0.9)
      }
    end

    # Departamentos
    @departments.each do |dpto|
      xml.url {
        xml.loc(department_url(dpto.slug,subdomain: false))
        xml.changefreq("daily")
        xml.priority(0.9)
      }

      # Categorias
      dpto.categories.each do |cat|
        xml.url {
          xml.loc(category_url(dpto.slug, cat.slug,subdomain: false))
          xml.changefreq("daily")
          xml.priority(0.9)
        }

        # Produtos
        cat.products.published.each do |product|
          xml.url {
            xml.loc(ad_url(product.department_slug, product.category_slug, product.slug,subdomain: false))
            xml.changefreq("weekly")
            xml.priority(0.9)
          }
        end
      end
    end
  end
end
