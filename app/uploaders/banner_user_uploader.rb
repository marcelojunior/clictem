# encoding: utf-8

class BannerUserUploader < UploaderBase
  process :store_dimensions

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  process :strip


  version :main do
    process :resize_to_fit => [750, 371]
  end


  private

  def store_dimensions
    if file && model
      model.width, model.height = ::MiniMagick::Image.open(file.file)[:dimensions]
    end
  end

end
