# encoding: utf-8

class BannerTopoUploader < UploaderBase
  process :store_dimensions
  process :strip

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :main do
    process :resize_to_fit => [970, 90]
  end

  private

  def store_dimensions
    if file && model
      model.width, model.height = ::MiniMagick::Image.open(file.file)[:dimensions]
    end
  end

end
