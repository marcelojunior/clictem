class UploaderBase < CarrierWave::Uploader::Base

    include CarrierWave::MiniMagick
    include CarrierWave::Processing::MiniMagick

    if Rails.env.development?
      storage :file
    else
      storage :fog
    end

    def filename
      "#{secure_token(10)}.#{file.extension}" if original_filename.present?
    end

    protected
        def secure_token(length=16)
          var = :"@#{mounted_as}_secure_token"
          model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.hex(length/2))
        end

end
