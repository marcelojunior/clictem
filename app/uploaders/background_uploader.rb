# encoding: utf-8

class BackgroundUploader < UploaderBase

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  process :strip


  # Utilizar na tela de edição dos dados
  version :thumb do
    process :resize_to_fit => [240, 240]
  end


  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :resize_to_fit => [50, 50]
  # end

  # version :banner, :if => :is_banner? do
  #   version :banner1
  # end

  # version :product, :if => :is_product? do
  #   version :thumb
  #   version :medium
  #   version :large
  # end

  # version :small_thumb, :from_version => :thumb do
  #   process resize_to_fill: [20, 20]
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # def extension_white_list
  #   %w(jpg jpeg gif png)
  # end

end
