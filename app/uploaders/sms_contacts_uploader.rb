# encoding: utf-8

class SmsContactsUploader < CarrierWave::Uploader::Base

  if Rails.env.development?
    storage :file
  else
    storage :fog
  end

  def store_dir
    "uploads_sms/#{Time.zone.now.to_i}"
  end
end
