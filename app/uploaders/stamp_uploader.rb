# encoding: utf-8

class StampUploader < UploaderBase

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  process :strip
  process :resize_to_fit => [115, 60]

  version :thumb do
    process :resize_to_fit => [115, 60]
  end
end
