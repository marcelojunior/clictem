# encoding: utf-8

class ImageUploader < UploaderBase

  def store_dir
    "uploads/imagens/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :resize_to_fit => [50, 50]
  # end

  # version :banner, :if => :is_banner? do
  #   version :banner1
  # end

  # version :product, :if => :is_product? do
  #   version :thumb
  #   version :medium
  #   version :large
  # end

  # version :small_thumb, :from_version => :thumb do
  #   process resize_to_fill: [20, 20]
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg gif png)
  end

protected

  def is_banner? picture
    model.is_banner?
  end

  def is_product? picture
    model.is_product?
  end

  def is_avatar? picture
    model.has_avatar?
  end

end
