# encoding: utf-8

class ProductUploader < UploaderBase

  def store_dir
    "uploads/produtos/#{mounted_as}/#{model.id}"
  end

  process :strip
  # process :resize_to_fit => [950, 800]
  process :quality => 90


  # Zoom do produto
  version :zoom, :if => :image? do
    # TODO só executa esse processo se a imagem original for maior que as dimensoes abaixo,
    # senão vai um fill ou fit
    process :resize_and_pad => [880, 880, '#ffffff']
  end

  # Imagem principal no detalhe do produto
  version :main, :if => :image? do
    # process :resize_to_fill => [442, 553]
    process :resize_and_pad => [440, 440, '#ffffff']
  end

  # Utilizar nas thumbs no detalhe do produto
  version :thumb, :if => :image? do
    process :resize_and_pad => [100, 100, '#ffffff']
  end

  # Utilizado na lista de produtos
  version :list, :if => :image? do
    process :resize_and_pad => [260, 240, '#ffffff']
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg gif png)
  end

  protected
    def image?(new_file)
      new_file.content_type.start_with? 'image'
    end

end
