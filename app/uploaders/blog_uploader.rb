# encoding: utf-8

class BlogUploader < UploaderBase

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  process :strip

  version :thumb do
    process :resize_to_fit => [200, 200]
  end
end
