class ToggleProductCampaignWorker
  include Sidekiq::Worker

  def perform(campaign_subdomain, product_id)
    campaign = Manager::Ad::ThemedCampaign.find_by_subdomain(campaign_subdomain)
    campaign.toggle_product_campaign(product_id)
  end
end
