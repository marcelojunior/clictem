class SidetiqSmsResponse
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence backfill: true do
    hourly.minute_of_hour(0, 15, 30, 45)
  end

  def perform
    Apartment::Tenant.each do |org|
        Apartment::Tenant.switch!(org)
        Sms::Message.get_responses
    end
  end
end
