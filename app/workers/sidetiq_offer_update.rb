class SidetiqOfferUpdate
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { daily.hour_of_day(2) }

  def perform
    Apartment::Tenant.each do |org|
        Apartment::Tenant.switch!(org)
        Manager::Ad::Product.update_all(is_offer: false, has_cupom: false)
        Manager::Ad::Offer.all_offers.each do |offer|
            offer.product.update_offer if offer.product
        end
    end
  end
end
