class SidetiqSmsSend
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence backfill: true do
    hourly.minute_of_hour(0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55)
  end


  def perform
    Apartment::Tenant.each do |org|
        Apartment::Tenant.switch!(org)
        camps = Sms::Campaign.send_campaigns
        puts "#{camps.size} campanhas de SMS. #{Time.zone.now.to_s}"
    end
  end
end
