class SidetiqReportBasic
  include Sidekiq::Worker
   include Sidetiq::Schedulable
  #
   recurrence backfill: true do
     monthly(1)
   end

  def perform
    Apartment::Tenant.each do |org|
        Apartment::Tenant.switch!(org)

        date_start = Date.today.beginning_of_month
        date_end = Date.today.end_of_month
        bills = Bill.where(due: (date_start..date_end)).order(:user_id)
        Mails::BillMailer.faturamento(bills).deliver
    end
  end
end
