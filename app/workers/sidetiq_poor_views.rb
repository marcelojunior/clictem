class SidetiqPoorViews
  include Sidekiq::Worker
  # include Sidetiq::Schedulable
  #
  # recurrence backfill: true do
  #   weekly.day(:tuesday)
  # end

  def perform
    Apartment::Tenant.each do |org|
        Apartment::Tenant.switch!(org)

        [7,15,30,40].each do |day|
            date1 = (Date.today - day.days).beginning_of_day
            date2 = (Date.today - day.days).end_of_day

            User.where(plan: ['basico', 'top', 'total']).each do |user|
                products = user.products.where(created_at: (date1..date2))
                    .where('description is not null')
                    .where("image1 is not null and image1 != ''")
                    .where('views_count < 20')

                if products.size > 0
                    email = Mails::UserMailer.poor_views(user, products).deliver
                    user.notifications.new.save_email(email)
                end
            end
        end
    end
  end
end
