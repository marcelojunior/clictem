class SidetiqMkt
  include Sidekiq::Worker
   include Sidetiq::Schedulable

   recurrence backfill: true do
     weekly.day(:wednesday)
   end

  def perform
    Apartment::Tenant.each do |org|
        Apartment::Tenant.switch!(org)
        users = User.where(want_email: true)
        products = Manager::Ad::Product.client_produtcs
        if (products.size >= 4)
          users.each do |user|
            Mails::UserMailer.mkt_weekly(user, products).deliver
          end
          sleep(2.hours)
        end
    end
  end
end
