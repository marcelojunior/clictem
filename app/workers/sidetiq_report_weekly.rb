class SidetiqReportWeekly
  include Sidekiq::Worker
  # include Sidetiq::Schedulable

  # recurrence backfill: true do
  #   weekly.day(:monday).hour_of_day(8)
  # end

  def perform
    Apartment::Tenant.each do |org|
        Apartment::Tenant.switch!(org)
        users = User.clients.pluck(:id)
        User.where(id: users).each do |user|
            email = Mails::UserMailer.semanal(user, Date.today).deliver
            user.notifications.new.save_email(email)
        end
    end
  end
end
