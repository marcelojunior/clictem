class CupomEmailWorker
  include Sidekiq::Worker

  def perform(code, current_org)
    Apartment::Tenant.switch!(current_org)
    cupom = Manager::Ad::Cupom.find_by_code(code)
    @current_org = current_org
    if cupom
        Mails::UserMailer.cupom(cupom).deliver
    end
  end
end
