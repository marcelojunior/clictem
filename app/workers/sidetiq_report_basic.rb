class SidetiqReportBasic
  include Sidekiq::Worker
  # include Sidetiq::Schedulable
  #
  # recurrence backfill: true do
  #   weekly.day(:monday)
  # end

  def perform
    Apartment::Tenant.each do |org|
        Apartment::Tenant.switch!(org)

        users = Manager::Ad::Product.pluck(:user_id).uniq
        date1 = Date.today.last_month.beginning_of_month
        date2 = Date.today.last_month.end_of_month
        User.where(id: users).where('created_at < ?', Time.zone.now - 20.days).where(plan: ['basico', 'top']).each do |user|
            # Se os produtos tiverem mais de 10 visitas
            if Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day)).count > 10
                email = Mails::ProductMailer.report_basic(user, date1, date2).deliver
                user.notifications.new.save_email(email)
            end
        end

    end
  end
end
