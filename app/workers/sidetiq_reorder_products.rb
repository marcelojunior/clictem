class SidetiqReorderProducts
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { daily.hour_of_day(6) }

  def perform
    Apartment::Tenant.each do |org|
        Apartment::Tenant.switch!(org)
        order = 0
        plans = User::PLANS

        plans.each do |plan|
          Manager::Ad::Product.where(user_id: User.where(plan: plan).pluck(:id)).order('RANDOM()').each do |product|
            product.raw_order = order
            product.save
            order += 1;
          end
        end
    end
  end
end
