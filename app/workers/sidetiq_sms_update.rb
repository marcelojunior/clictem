class SidetiqSmsUpdate
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { hourly.minute_of_hour(0, 30) }

  def perform
    Apartment::Tenant.each do |org|
        Apartment::Tenant.switch!(org)

        campaigns = []
        date = Date.today - 7.days
        Sms::Message.where(updated_at: (date.beginning_of_day..Date.today.end_of_day),
        status: ['transmitida', 'erro']).each do |message|
            campaigns << message.campaign_id
            status = message.status_sms
            message.converte_status_tww(status)
            message.save
        end
    end
  end
end
