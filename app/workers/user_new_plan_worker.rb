class UserNewPlanWorker
  include Sidekiq::Worker

  def perform(user)
    user = User.find user

    email = Mails::UserMailer.new_plan_to_client(user).deliver
    user.notifications.new.save_email(email)
    user.save

    Mails::UserMailer.new_plan_to_clictem(user).deliver
  end
end
