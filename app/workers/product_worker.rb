class ProductWorker
  include Sidekiq::Worker

  def perform(product, field, url)
    field = field.to_sym
    product = Manager::Ad::Product.find(product)
    product.send("remote_#{field}_url=", url.gsub('+','%20'))
    product.save
  end
end
