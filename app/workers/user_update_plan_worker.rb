class UserUpdatePlanWorker
  include Sidekiq::Worker

  def perform(user, plan, banner, banner_note)
    user = User.find user

    Order.create(
        user_id: user.id,
        value: 0.0,
        installment: 1,
        note: "Alteração do plano #{user.label_plan} para #{plan}"
    )

    if banner != 'Não'
      Order.create(
          user_id: user.id,
          value: 0.0,
          installment: 1,
          note: "Aquisição de banner #{banner}. #{banner_note}."
      )
    end

    Mails::UserMailer.update_plan_to_clictem(user, plan, banner, banner_note).deliver

  end
end
