class SidetiqIntercomUpdate
  include Sidekiq::Worker
  # include Sidetiq::Schedulable

  # recurrence { hourly }

  def perform
    Apartment::Tenant.each do |org|
        Apartment::Tenant.switch!(org)

        # include ActionView::Helpers::NumberHelper
        Intercom.app_id = ENV["INTERCOM_APP_ID"] || "lpy13asw"
        Intercom.app_api_key = 'c032af57eb521b5013aded1f45589ab25eb97e1b'

        c_users = User.clients.pluck(:id)
        i_users = Intercom::User.all.map {|user| user.user_id }

        i_users.each do |id|
            next if User.where(id: id.split('-').last).size == 0
            c_user = User.find(id.split('-').last)
            i_user = Intercom::User.find(user_id: id)

            # Site (CDl Xaxim ou CDL Xanxerê)
            i_user.custom_attributes['site'] = org

            # Plano
            i_user.custom_attributes['plano'] = c_user.label_plan

            # Telefone
            i_user.custom_attributes['telefone'] = c_user.phone

            # Celular
            i_user.custom_attributes['celular'] = c_user.cellphone

            # Código Associado
            i_user.custom_attributes['associado'] = c_user.associated_code

            # Inicio contrato
            i_user.custom_attributes['contrato inicio'] = I18n.l(c_user.contract_start, format: :short) if c_user.contract_start

            # Final de contrato
            i_user.custom_attributes['contrato final'] = I18n.l(c_user.contract_end, format: :short) if c_user.contract_end

            # Limite de Anúncios
            i_user.custom_attributes['anuncios limite'] = c_user.ad_limit

            # Anúncios cadastrados
            i_user.custom_attributes['anuncios cadastrados'] = c_user.products.size

            # Anúncios publicados
            i_user.custom_attributes['anuncios publicados'] = c_user.products.published.size

            # Franquia de SMS
            i_user.custom_attributes['franquia de sms'] = c_user.sms_franchise_hired

            # Total de SMS Enviado
            i_user.custom_attributes['sms enviados'] = c_user.sms_total_balance

            # Total de campanhas SMS criadas nos últimos 30 dias
            i_user.custom_attributes['sms campanhas'] = c_user.sms_campaigns.last_month.count

            # Total de SMS de aniversário eviados nos útimos 30 dias
            i_user.custom_attributes['sms aniverario'] = c_user.sms_birthday_last_month

            # Total de cupons de desconto emitidos nos últimos 30 dias
            i_user.custom_attributes['cupons emitidos'] = c_user.cupoms.last_month.count

            # Total de comentários nos últimos 30 dias
            i_user.custom_attributes['comentarios'] = c_user.comments.count

            # Total de contatos cadastrados nos últimos 30 dias
            i_user.custom_attributes['clientes'] = c_user.customers.count

            begin
              i_user.save
            rescue
              puts ""
            end

            # Sleep para evitar erro de Timeout no Intercom
            sleep(2)

        end
    end
  end
end
