class ProductImagesWorker
  include Sidekiq::Worker

  def perform(product_id)
    product = Manager::Ad::Product.find product_id
    product.images.where(waiting: true).each do |image|
        begin
            tmp = Manager::Ad::ImageTemp.where(token: image.token).first

            if tmp
                image.base64 = tmp.base64
                image.file = tmp.base64
                image.waiting = false
                image.save
                tmp.destroy
            end
        rescue
            image.destroy
        end
    end
  end
end
