class SidetiqSmsConfirm
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence backfill: true do
    daily.hour_of_day(10)
  end


  def perform
    numeros = ['(49) 99923-7800', '(49) 99172-8882', '(49) 98818-3620']

    numeros.for_each do |numero|
      mensagem = Sms::Message.new
      mensagem.cellphone = numero
      mensagem.text = "Confirmacao de SMS Clictem - #{Date.today}. Esta tudo OK."
      mensagem.send_sms
    end
  end
end
