class SidetiqSmsBirthday
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence backfill: true do
    daily.hour_of_day(10)
  end

  def perform
    Apartment::Tenant.each do |org|
        User.where(sms_birthday: true).each do |user|
            if user.customers.birthday_today.count > 0
                campaign = user.sms_campaigns.create!(
                    message: user.sms_birthday_message,
                    schedule_date: Date.today,
                    schedule_time: Time.zone.now,
                    status: 'aguardando',
                    birthday: true
                )
                campaign.create_messages
            end
        end
    end
  end
end
