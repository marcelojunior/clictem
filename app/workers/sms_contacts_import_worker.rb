class SmsContactsImportWorker
  include Sidekiq::Worker

  def perform(tmp_path, org_file, user, domain)
    Apartment::Tenant.switch!(domain)
    user     = User.find(user)
    user.import_xls(tmp_path, org_file)
  end
end
