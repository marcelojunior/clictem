class SmsMessageCreateWorker
  include Sidekiq::Worker

  def perform(campaign)
    campaign = Sms::Campaign.find(campaign)
    user     = campaign.user

    Sms::Message.delete_all(campaign_id: campaign.id)


    # Cria as mensagens
    if campaign.birthday == false
        contacts = Group.customers(campaign.user, campaign.groups.pluck(:id), true).limit(user.sms_total_balance)
    else
        contacts = user.customers.birthday_today.where("cellphone is not null and cellphone != ''")
    end

    contacts.each_with_index do |contact, i|
        balance = i + 1
        message = campaign.message
        message = Sms::Message.parse_nome(message, contact.first_name)
        Sms::Message.create!(
            user_id: campaign.user_id,
            campaign_id: campaign.id,
            contact_id: contact.id,
            text: message,
            cellphone: contact.cellphone,
            balance_origin: 0
        )
    end

    # Envia e-mail para aprovação da campanha
    if campaign.birthday == false and Rails.env.production?
        Mails::SmsMailer.new_campaign(campaign).deliver
    end
  end
end
