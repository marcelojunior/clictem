class UserEmailWorker
  include Sidekiq::Worker

  def perform(user, template)
    user = User.find user
    Mails::UserMailer.send("#{template}", user).deliver
  end
end
