class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    request.env['omniauth.strategy'].options[:client_id] = "770727769658195"
    request.env['omniauth.strategy'].options[:client_secret] = current_org.fb_app_secret

    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
      sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
      # set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format?
      flash.discard
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end

  def setup
    request.env['omniauth.strategy'].options[:client_id] = 'teste'
    request.env['omniauth.strategy'].options[:client_secret] = current_org.fb_app_secret
    render :text => "Setup complete.", :status => 404
  end
end
