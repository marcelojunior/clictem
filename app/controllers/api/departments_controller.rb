class Api::DepartmentsController < ApplicationController

  def index
    render json: Manager::Ad::Department.unscoped.where(is_home: false).order(:name).all
  end

end