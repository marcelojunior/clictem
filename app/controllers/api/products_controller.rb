class Api::ProductsController < Api::BaseController

  def index
    render json: Manager::Ad::Product.all
  end

  def show
    render json: Manager::Ad::Product.find(params[:id])
  end
end
