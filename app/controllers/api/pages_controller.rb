class Api::PagesController < Api::BaseController
  def index
    render json: Manager::Page.all
  end
end