class Site::BrandsController < ApplicationController

  def index
    deparment = Manager::Ad::Department.find(params[:department])
    brands = deparment.brands.order(:name).uniq
    render json: brands
  end


end
