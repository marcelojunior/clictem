class Site::AtrairController < ApplicationController
  before_action :verify_pj

  # Lista de campanhas do clictem
  def campanhas
    @campaigns = Manager::Ad::ThemedCampaign.where(active: true, hidden: false)
          .where('date_end >= ?', Date.today)
    @selecteds = current_user.themed_campaigns.pluck(:id)
  end



  # Tela de seleção produtos nas campanhas clictem
  def anuncios
    @campaign = Manager::Ad::ThemedCampaign.find_by_subdomain(params[:campanha])
    if (!current_user.themed_campaigns.include?(@campaign))
        redirect_to site_atrair_path(:confirmar, campanha: @campaign.subdomain)
    else
      @selecteds = @campaign.campaigns.where(user_id: current_user.id).pluck(:product_id)
      @products = current_user.products.published
    end

  end

  def confirmar
    @campaign = Manager::Ad::ThemedCampaign.find_by_subdomain(params[:campanha])
  end

  def concordar
    campaign = Manager::Ad::ThemedCampaign.find_by_subdomain(params[:campanha])
    current_user.themed_campaigns << campaign
    current_user.save
    redirect_to site_atrair_path(:anuncios, campanha: campaign.subdomain)
  end



  # Adicionar anúncios a campanha
  def add_ad_clictem
    @product_id = params[:product]

    campaign = Manager::Ad::ThemedCampaign.find_by_subdomain(params[:campaign])
    campaign.toggle_product_campaign(@product_id)
  end



end
