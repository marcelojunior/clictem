class Site::AnalisarController < ApplicationController
  before_action :verify_pj

  # Lista de campanhas do clictem
  def publico_alvo
    @user = current_user

    if params[:filter]
      @date1 = Date.parse(params[:filter][:date1])
    else
      @date1 = Date.today - 30.days
    end

    if params[:filter]
      @date2 = Date.parse(params[:filter][:date2])
    else
      @date2 = Date.today
    end

    # raise

    @ads = Visit.ads_most_views(@user, @date1, @date2)

  end




end
