class Site::VisitsController < ApplicationController
  def new
    unless request.env['HTTP_FROM'] =~ /googlebot/
    	visit = Visit.new
    	visit.ad = params[:ad] if params[:ad].present?
      visit.post = params[:post] if params[:post].present?
    	visit.department_id = params[:department] if params[:department].present?
    	visit.category_id = params[:category] if params[:category].present?
    	visit.owner = params[:owner] if params[:owner].present?
      visit.cluster = params[:cluster] if params[:cluster].present?
      visit.product = params[:product] if params[:product].present?
      visit.age = params[:age] if params[:age].present?
      visit.interest = params[:interest] if params[:interest].present?
      visit.brand = params[:brand] if params[:brand].present?
      visit.career = params[:career] if params[:career].present?
      visit.gender = params[:gender] if params[:gender].present?
      visit.income = params[:income] if params[:income].present?
      visit.ip = request.remote_ip

    	if params[:visitor].present?
    		visit.visitor = params[:visitor]
    	end
    	visit.save

      if params[:ad]
        Manager::Ad::Product.where(id: params[:ad]).update_all(views_count: Visit.where(ad: params[:ad]).count)
      end

    end

  	render text: "ok", status: 200
  end
end
