class Site::HomeController < ApplicationController

  def index
  	# TODO Código inserido de forma fixa. Lembrar de tirar
  	user = User.find_by_subdomain('natal')

    # Departamentos mais visualizados nos últimos 7 dias
    @deps = Manager::Ad::Department.unscoped.order('RANDOM()')

    @campaigns = Manager::Ad::ThemedCampaign.where('date_start <= :today and date_end >= :today', today: Date.today).where(active: true)

    # Empresas parceiras
    departamento = Manager::Ad::Department.where(is_home: true).first
    @clients = []
    if departamento != nil
      @clients = departamento.users_premium.where(active: true) 
    end
  end



  def home
    render "site/home/home", layout: false
  end

end
