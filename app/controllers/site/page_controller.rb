class Site::PageController < ApplicationController

  def sobre
    set_page('sobre')
  end





  def como_anunciar
    set_page('como-anunciar')
  end





  def politica
    set_page('politica-de-privacidade')
    render layout: false
  end





  def termos
    set_page('termos-e-condicoes-de-uso')
    render layout: false
  end





  def atendimento
    set_page('atendimento')
  end



  # Lista de planos
  def planos
    set_page('planos')
  end


  # Formulário para contratação
  def contratar
    set_page('contratar')

    @user = current_user || User.new(kind: 'pj', city_id: @current_org.city_id, plan: 'basico')

    if request.method == 'POST'

      if params[:id]
        @user = User.find(params[:id])
        @user.update_without_password(user_params)
      else
        @user = User.new(user_params)
      end

      if @user.save
        order = Order.create(
            user_id: @user.id,
            value: @user.value_plan,
            installment: 6,
            note: "Aquisição do plano #{@user.label_plan}",
            plan: @user.plan
        )
        order.create_checklist(current_org)

        Bill.create(
          user: order.user,
          order: order,
          value: order.relative_value(Date.today),
          commission: WHITELABEL_COMMISSION_PLAN,
          due: Date.today + 1.month,
          note: "Plano mensal #{order.plan} de #{l(Date.today, format: :short)} até #{l(Date.today.end_of_month, format: :short)}"
        )

        sign_in(@user, :bypass => true)
        @user.default_limit

        email = Mails::UserMailer.new_plan_to_client(@user).deliver
        @user.notifications.new.save_email(email)
        @user.save

        Mails::UserMailer.new_plan_to_clictem(@user).deliver

        redirect_to site_minha_conta_url(p: true)
      else
        render action: 'contratar'
      end
    end
  end



  # Alteração do plano
  def altera_plano
    UserUpdatePlanWorker.perform_async(current_user.id, params[:user_plan], params[:banner_position], params[:banner_note] || '')
  end




  def ilegais
    set_page('produtos-e-servicos-ilegais')
  end




  def sitemap
    @departments = Manager::Ad::Department.all

    users_id = Manager::Ad::Product.published.pluck(:user_id).uniq
    @users = User.where(id: users_id).sellers

    respond_to do |format|
     format.xml
    end
  end




  def acesso

     # Se o acesso veio através de uma geração de cupom
    if params.has_key?(:prodCP)
      cookies[:prodCP] = { value: params[:prodCP], expires: 10.minutes.from_now }
    end

     # Se o acesso veio através de um comentário
     if params[:prodCM]
      cookies[:prodCM] = { value: params[:prodCM], expires: 10.minutes.from_now }
    end

    if signed_in?
      redirect_to site_minha_conta_url(subdomain: false)
    end
  end

private


  def set_page(slug)
    Rails.cache.fetch("pages_info_#{slug}", :expires_in => 7.days) do
      @page = Manager::Page.find(slug)
    end

    if @page
      @page_title = @page.title
      @page_description = @page.description

      @page_og = {
        title: @page_title,
        description: @page_description,
        type: 'website',
        url: request.original_url,
        image: view_context.image_url('padrao-fb.png')
      }
    end

    rescue ActiveRecord::RecordNotFound
      Manager::Page.create(title: slug)
  end



  def user_params
    params.require(:user).permit(:name, :last_name, :email, :kind, :cpf, :birthday, :genre, :cellphone, :business_name, :cnpj, :website, :phone, :resp_name, :resp_email, :resp2_name, :resp2_email, :resp3_name, :resp3_email, :resp_cpf, :resp_birthday, :address_zipcode, :address_street, :address_number, :address_district, :address_compl, :city_id, :state_id, :password, :password_confirmation, :skip_validates, :want_email, :want_sms, :plan, :seller, :department_id, :associated_code)
  end

end
