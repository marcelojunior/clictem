module Site
  class ProductsController < ApplicationController
    skip_before_action :verify_authenticity_token, :only => [:show_user_info]
    before_action :is_service

    # Lista todos os produtos (departamento, categorias, subcategorias)
    def index
      if DepartmentsConstraint.matches?(request)
        @department = Manager::Ad::Department.find(request.subdomains(0).first)
      else
        @department = Manager::Ad::Department.find(params[:department])
      end

      # SEO
      @page_title = "#{@department.name}"
      @page_description = @department.description
      # @page_keywords = @department.keywords

      @page_og = {
        title: @page_title,
        description: @page_description,
        type: 'website',
        url: request.original_url,
        image: view_context.image_url('padrao-fb.png')
      }

      @categories = @department.categories
      unless request.xhr?
        if @department.posts.first
          @post = @department.posts.first
        end

        # Anunciantes
        @associados = @department.users.associate

        # Empresas destaque
        @user_premium = @department.users_premium

        # Produtos destaque
        @products_highlight = []

        # Calcula o número de produtos a serem exibidos
        if @user_premium.size > 0
          limit_per_user = (6 / @user_premium.size).round
          limit_per_user = 1 if limit_per_user == 0
        end

        @user_premium.each do |user|
          user.products.published.order('RANDOM()').limit(limit_per_user).each do |product|
            @products_highlight << product
          end
        end
        @products_highlight.shuffle!
      else
        @user_premium = []
      end


      if params[:category] and @categories
        @category = @categories.find(params[:category])

        if @is_service
          @products = @category.products.services.published.ordered.includes([:category, user: [:city]]).page(params[:page])
        else
          @products = @category.products.published.ordered.includes([:category, user: [:city]]).page(params[:page])
        end

        @page_title = "#{@category.name}"
        @page_description = @category.description
        hlp_site_breadcrumb_add(@department.name, department_path(@department.slug))
      else
        if @is_service
          @products = @department.products.services.published.ordered.includes([:category, user: [:city]]).page(params[:page])
        else
          @products = @department.products.published.ordered.includes([:category, user: [:city]]).page(params[:page])
        end
      end
    end



    # Lista de ofertas
    def offers
      # SEO
      @page_title = "Ofertas"
      @page_description = "Somente as melhores ofertas"

      @page_og = {
        title: @page_title,
        description: @page_description,
        type: 'website',
        url: request.original_url,
        image: view_context.image_url('padrao-fb.png')
      }

      hlp_site_breadcrumb_add("Ofertas", "/ofertas")

      unless params[:category]
          @products = Manager::Ad::Product.all_offers.includes([:user, :department, :brand, :category]).published.page(params[:page])
        else
          @products = Manager::Ad::Product.all_offers.published.where(category_id: params[:category]).page(params[:page])
        end

      define_relateds(@products)
    end



    # Lista de produtos com cupom
    def cupoms
      # SEO
      @page_title = "Cupons"
      @page_description = "Emita seu cupom e aproveite as melhores ofertas da cidade."

      @page_og = {
        title: @page_title,
        description: @page_description,
        type: 'website',
        url: request.original_url,
        image: view_context.image_url('padrao-fb.png')
      }

      hlp_site_breadcrumb_add("Cupons", "/cupons")

      unless params[:category]
          @products = Manager::Ad::Product.all_cupoms.includes([:user, :department, :brand, :category]).published.page(params[:page])
        else
          @products = Manager::Ad::Product.all_cupoms.published.where(category_id: params[:category]).page(params[:page])
        end

      define_relateds(@products)
    end



    # Método para mostrar apenas os dados de um único produto
    def show
      @product = Manager::Ad::Product.includes([:category, :department, user: [:city]])
        .friendly.published.find(params[:product])

      if @product

        redirects(@product)

        @department  = @product.department
        @category    = @product.category
        @seller      = @product.user
        @prodCP      = params.has_key?(:prodCP) ? true : false
        @user_custom = @seller

        if @product.published?

          description = get_description(@product)

          seo(@product, @seller, description)

          # Buscar produtos relacionados
          @rel_prods = @department.products.published.includes([:category, user: [:city]])
            .where.not(id: @product.id)
            .order('RANDOM()').limit(8)

          hlp_site_breadcrumb_add(@department.name, department_url(@department.slug,subdomain: false))
          hlp_site_breadcrumb_add(@category.name, category_url(@department.slug, @category.slug,subdomain: false))
        end
      else
        redirect_to product_not_found_path
      end


      rescue ActiveRecord::RecordNotFound
        redirect_to product_not_found_path
    end


    # Busca de anúncios, obviamente.
    def search
      if params[:search]
        @search  = params[:search]
        unless params[:category]
          @products = Manager::Ad::Product
                .includes([:user, :department, :brand, :category])
                .only_sellers
                .where(published: true)
                .search(@search)
                .page(params[:page])
        else
          @products = Manager::Ad::Product.where(published: true)
                .only_sellers
                .where(category_id: params[:category])
                .search(@search)
                .page(params[:page])
        end

        @products = @products.map{|pr| pr.user.cancel_date == nil ? pr : nil }.compact

        # Anunciantes
        @advertisers = User.where('city_id is not null')
                          .where("plan != 'free' or associated_code <> ''").where(roles_mask: nil)
                          .includes([:city, :state]).order(plan: :desc).search(@search)

        # Recupera todas as categorias e marcas
        define_relateds(@products)

      else
        @search       = ''
        @products     = []
        @categories   = []
        @departments  = []
        @advertisers  = []
        @related      = []
      end

      # @sub_categories = @products.pluck(:sub_category_id).uniq


      @page_title   = "Busca: '#{@search}'"
      @page_description = "Busca por anúncios de '#{@search}'"
    end

    # Envia interesse de compra para o anunciante

    def show_user_phone
      @user = User.where(id: params[:user_id]).select(:id, :phone, :phone2, :cellphone).first

      @product = Manager::Ad::Product.find(params[:product_id]) if params[:product_id]

      click    = Manager::Ad::InfoClick.new(seller_id: @user.id)

      if @product
        click.product_id = @product.id
      end

      if current_user
        click.user = current_user
      end

      click.save
      
      render json: @user

    rescue ActiveRecord::RecordNotFound
      render json: { error: "not_found" }, status: :not_found

    end

    # Exibe os dados do anunciante
    def show_user_info
      unless request.env['HTTP_FROM'] =~ /googlebot/
        @user    = User.find(params[:user])

        @product = Manager::Ad::Product.find(params[:product])

        if @product
          click    = Manager::Ad::InfoClick.new(product_id: @product.id)
          if current_user
            click.user = current_user
          end
          click.save
        end

        respond_to do |format|
          format.js
          format.html { redirect_to ad_url(@product.department_slug, @product.category_slug, @product.slug,subdomain: false) }
        end

      else
        render text: 'Clictem'
      end

    end

    private

    # Monta a descrição
    def get_description(product)
      description = ActionController::Base.helpers.strip_tags(product.description.gsub(/\n/, ""))

      if product.has_interest?
        description = product.interest_title
      elsif product.on_request? or product.offer_price.to_f == 0.0
        value = ActionController::Base.helpers.number_to_currency(product.offer_price)
        description = "#{description}"
      else
          if product.price > product.offer_price
              value = "De #{ActionController::Base.helpers.number_to_currency(product.price)} por #{ActionController::Base.helpers.number_to_currency(product.offer_price)}"
              description = "#{value} - #{description}"
          else
              value = "#{ActionController::Base.helpers.number_to_currency(product.offer_price)}"
              description = "#{value} - #{description}"
          end
      end

      description
    end

    # Seo da página de produtos
    def seo(product, seller, description)
      if seller.plan == 'free'
        @page_title = product.name
      else
        @page_title = "#{product.name} | #{seller.name}"
      end
      @page_description = description

      @page_og = {
        title: product.name,
        description:  description,
        type: 'website',
        url: ad_url(product.department_slug, product.category_slug, product.slug,subdomain: false),
        image: "#{product.main_image.file_url(:main) if product.main_image and product.main_image.file?}",
      }
    end

    # Redirecionamentos para produtos
    def redirects(product)
      # Caso a url já tenha sido alterada (acessando com a URL antiga)
      # redireciona (301) para a url mais atual
      if params[:product] != product.slug
        return redirect_to ad_url(product.department_slug, product.category_slug, product.slug, subdomain: false), status: :moved_permanently
      end

      # Redirecionar site se vier com subdominio
      if UsersConstraint.matches?(request)
        return redirect_to ad_url(product.user_subdomain, product.category_slug, product.slug, subdomain: false), status: :moved_permanently
      end

      # Redirecionar para subdominio atual
      if params[:department] != product.user.subdomain
        return redirect_to ad_url(product.user.subdomain, product.category_slug, product.slug, subdomain: false), status: :moved_permanently
      end
    end

    def interest_params
      params.require(:manager_ad_interest).permit!
    end


    def is_service
      if params[:servicos]
        @is_service = true
      else
        @is_service = false
      end
    end


    # Departamentos, marcas e anunciantes relacionados aos produtos encontrados
    def define_relateds(products)
      @categories     = []
      @departments    = []
      @brands         = []
      @related        = [] #Anunciantes encontrados nos produtos
      products.each do |product|
        @categories     << product.category_id
        @departments    << product.department_id
        @brands         << product.brand_id if product.brand
        @related        << product.user_id
      end
      @departments    = Manager::Ad::Department.where(id: @departments.uniq).includes(:categories)
      @related        = User.where(plan: 'total', id: @related.uniq)
      @brands         = @brands.uniq
    end

  end
end
