class Site::BlogController < ApplicationController

  def authors
    @authors = Blog::Author.all.order('RANDOM()')
  end

  # Lista de posts de um blogueiro
  def index
    @author = Blog::Author.find_by_slug(params[:slug_blog])

    if @author
      @posts = @author.posts.page(params[:page])
      hlp_site_breadcrumb_add("Clictem Blog", blog_path)

      @page_title = @author.title
      @page_description = @author.bio_strip

      @page_og = {
        title: @page_title,
        description:  @page_description,
        type: 'website',
        url: blog_author_url(@author.slug, subdomain: false),
        image: "#{@author.image_url}"
      }

    else
      redirect_to '/404'
    end
  end

  # Postagem
  def show
    @post = Blog::Post.find_by_slug(params[:slug_post])
    if @post
      @department = @post.department
      hlp_site_breadcrumb_add(@post.author.title, blog_author_path(@post.author.slug))

      @page_title = @post.title
      @page_description = @post.text_strip[0..200]

      @page_og = {
        title: @post.title,
        description:  @page_description,
        type: 'website',
        url: blog_post_url(@post.slug, subdomain: false),
        image: "#{@post.image_fb_url}"
      }

      # Buscar produtos relacionados
      # @rel_prods = @department.products.published.includes([:category, user: [:city]]).order('RANDOM()').limit(3)

      @rel_posts = Blog::Post.where(author: @post.author).where.not(id: @post.id).order('RANDOM()').limit(3)

    else
      redirect_to '/404'
    end
  end

end
