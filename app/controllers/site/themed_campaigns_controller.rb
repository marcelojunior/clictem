class Site::ThemedCampaignsController < ApplicationController

    def index
        @themed_campaign = Manager::Ad::ThemedCampaign.where(subdomain: request.subdomains(0).first, active: true).first

        # Campanha não encontrada
        unless @themed_campaign
            return redirect_to error_404_url(subdomain: false)
        end

        # Se ainda não está ativa, redireciona para página de participação
        if !@themed_campaign.is_valid?
            return render 'new'
        end

        # Se foi selecionada uma empresa
        if params[:seller]
            @seller   = User.first_by_subdomain(params[:seller])
            @products = Manager::Ad::Product.published.where(id: @themed_campaign.campaigns.pluck(:product_id), user_id: @seller.id).page(params[:page])
        else
            @products = Manager::Ad::Product.published.where(id: @themed_campaign.campaigns.pluck(:product_id)).page(params[:page])
        end

        @sellers  = @themed_campaign.users

        @page_title       = @themed_campaign.title
        @page_description = @themed_campaign.description
        @page_keywords    = @themed_campaign.title

        @page_og = {
          title: @themed_campaign.title,
          description:  @themed_campaign.description,
          type: 'website',
          url: themed_campaign_url(subdomain: @themed_campaign.subdomain),
          image: @themed_campaign.img_3_url
        }

    end



    def new
        @themed_campaign = Manager::Ad::ThemedCampaign.where(subdomain: request.subdomains(0).first, active: true).first
    end


    # Cria um lead na base de dados do Clictem
    def create
        user = User.find 165
        campaign = Manager::Ad::ThemedCampaign.find(params[:customer][:campaign_id])
        @customer = Customer.create_lead(user, params[:customer][:email], {funnel: 2,
            groups: campaign.title,
            name: params[:customer][:name],
            phone: params[:customer][:phone],
            cellphone: params[:customer][:cellphone],
            company: params[:customer][:company],

        })

        Mails::UserMailer.themed_campaign(@customer, campaign).deliver
    end

end
