class Site::ContratarController < ApplicationController

    before_action :verify_user, except: [:planos]

    # 1. Contratar plano
    def planos
      session.delete(:banners)
      session.delete(:plan)
      session.delete(:depts)

      @user = current_user || User.new(kind: 'pj', city_id: @current_org.city_id)

      if request.method == 'POST' and params[:user][:plan] != 'free'
          session[:plan] = params[:user][:plan]
          redirect_to contratar_banners_path
      end
    end


    # 2. Contratar Banners
    def banners
        @banners = BannerType.all
        @departaments = Manager::Ad::Department.on_menu

        if request.method == 'POST'
            session[:banners] = params[:banners]
            session[:depts] = params[:depts]
            redirect_to contratar_dados_path
        end
    end


    # 3. Dados
    def dados
        @user = current_user || User.new(kind: 'pj', city_id: @current_org.city_id)
        @user.plan = session[:plan]
        @banners = []
        if session[:banners]
            session[:banners].each do |banner_id|
                @banners << BannerType.find(banner_id)
            end
        end

      carrega_banners


        if request.method == 'POST'
            if !finaliza_contrato
              render action: 'dados'
            end
        end

    end



    private


    def carrega_banners
      @departaments = Hash.new
      @banners.each do |banner|
        next unless session[:depts]
        next unless session[:depts][banner.id.to_s]
        @departaments[banner.id] = []
        session[:depts][banner.id.to_s].each do |dept|
          @departaments[banner.id] << Manager::Ad::Department.find(dept)
        end
      end
    end


    def finaliza_contrato
      if params[:id]
        @user = User.find(params[:id])
        @user.update_without_password(user_params)
      else
        @user = User.new(user_params)
      end

      if @user.save
        @user.create_order_plan(current_org)

        carrega_banners
        if @banners.size > 0
          @banners.each do |banner|
            valor_banner = banner.value.to_f
            descricao = banner.name

            if banner.department?
              valor_banner = banner.value.to_f * @departaments[banner.id].size
              descricao += " (#{ @departaments[banner.id].map{|dept| dept.name}.join(" + ") })"
            end

            order = Order.create(
                user_id: @user.id,
                value: valor_banner,
                installment: 12,
                note: descricao,
                kind: 'Banner',
                banner_type: banner
            )
            order.create_checklist(current_org)
          end
        end

        sign_in(@user, :bypass => true)
        @user.default_limit

        if Rails.env.production?
          email = Mails::UserMailer.new_plan_to_client(@user).deliver
          @user.notifications.new.save_email(email)
          @user.save
        end

        redirect_to site_minha_conta_url(p: true)
      else
        return false
      end
    end


    def verify_user
        unless session[:plan]
            flash[:error] = "Por favor, primeiro selecione seu plano."
            redirect_to contratar_planos_path
        else
          @plan = session[:plan]
        end
    end

    def user_params
      params.require(:user).permit(:name, :last_name, :email, :kind, :cpf, :birthday, :genre, :cellphone, :business_name, :cnpj, :website, :phone, :resp_name, :resp_email, :resp2_name, :resp2_email, :resp3_name, :resp3_email, :resp_cpf, :resp_birthday, :address_zipcode, :address_street, :address_number, :address_district, :address_compl, :city_id, :state_id, :password, :password_confirmation, :skip_validates, :want_email, :want_sms, :plan, :seller, :department_id, :associated_code)
    end


    def monta_proposta_banner
      @baners
    end


end
