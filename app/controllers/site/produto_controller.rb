#encoding: utf-8
class Site::ProdutoController < ApplicationController
  before_action :authenticate_user!

  def index
  end

  # Confirmação de dados do usuário
  def anuncio_1
    hlp_site_breadcrumb_add(current_user.name, site_minha_conta_url(subdomain: false))

    if params[:product]
      @product = Manager::Ad::Product.find(params[:product])
    end

    if request.method == 'GET'
      @user = current_user
    elsif request.method == 'POST'
      @user = current_user
      if @user.update_without_password(user_params)
        if @product
          redirect_to site_produtos_path(:anuncio_2, product: @product.id)
        else
          redirect_to site_produtos_path(:anuncio_2)
        end
      else
        render action: 'anuncio_1'
      end
    end
  end



  # Cadastro do anúncio
  def anuncio_2
    hlp_site_breadcrumb_add('Minha Conta', site_minha_conta_url(subdomain: false))

    # Se os dados do usuário não estiverem completos, volta para o passo 1
    if !current_user.valid?
      redirect_to site_produtos_path(:anuncio_1), notice: 'Por favor, complete seus dados.'
    else
      if request.method == 'GET'
        anuncio_2_get
      elsif request.method == 'POST'
        anuncio_2_post
      end

    end
  end


  # Upload de imagem
  def upload
    ProductWorker.perform_async(params[:product], params[:field], params[:url])
    cookies[:prodvalid] = { value: params[:product], expires: Time.zone.now + 5.minutes }
    render text: 'Ok'
  end


  # Publicar produto
  # def anuncio_5
  #   unless params[:product]
  #     redirect_to site_produtos_path(:anuncio_2)
  #   else
  #     @product = Manager::Ad::Product.find(params[:product])
  #
  #     if @product.is_valid? or cookies[:prodvalid] == params[:product]
  #       flash[:notice] = "O anúncio <strong>#{@product.name}</strong> foi <strong>publicado</strong>!"
  #
  #       if ((@product.created_at - Time.zone.now) / 60).round <= 10
  #         if current_user.products.size == 1 and cookies["socialprod#{@product.id}"] == nil
  #           cookies["socialprod#{@product.id}"] = @product.id
  #           UserEmailWorker.perform_async(current_user.id, 'social_share')
  #         end
  #       end
  #
  #       redirect_to site_minha_conta_url(subdomain: false)
  #     else
  #       redirect_to site_produtos_path(:anuncio_4, product: @product.id)
  #     end
  #   end
  #
  # end


  # Excluir produto
  def destroy
    @product = current_user.products.find(params[:id])
    @product.destroy
    Rails.cache.clear

    if request.xhr?
      redirect_to site_minha_conta_url(subdomain: false)
    end
  end



  # Retorna todos os interesses de um produto
  def list_interests
    @product   = Manager::Ad::Product.find(params[:product])
    @interests = @product.interests
  end


  # Retorna todos os comentários do produto
  def list_comments
    @product  = Manager::Ad::Product.find(params[:product])
    @comments = @product.comments.where(question_id: nil).order('id DESC')
  end


  # Excluir imagem
  def remove_image
    @product = Manager::Ad::Product.find(params[:product])
    @image   = params[:image]
    @product.send("remove_#{@image}!")
    @product.save
  end



  # Publicar ou não anúncio
  def publish
    @product = current_user.products.find(params[:product])
    @product.update_attributes!(product_params)
    Rails.cache.clear
  end



  # Criar sessions com Base64 das imagens
  def save_session_images
    Manager::Ad::ImageTemp.create!(token: params[:token], base64: params[:base64])
    render json: true
  end


  # Gera um arquivo XLS com todos os cupons emitidos em um anúncio
  def download_cupons
    produto = current_user.products.where(id: params[:id]).first

    nome_xls = I18n.l(Date.today, format: '%m-%Y')
    nome_xls = "#{nome_xls}-#{produto.slug}"

    @cupoms = produto.cupoms

    respond_to do |format|
        format.html
        format.xls do
          response.headers['Content-Disposition'] = 'attachment; filename="' + nome_xls + '.xls"'
        end
    end
  end

  private

  # Carrega a tela de edição e cadastro do anúncio
  def anuncio_2_get
    if params[:product]
      @product = Manager::Ad::Product.find(params[:product])
      @brands  = @product.department.brands

      if @product.offers.size == 0
        @product.offers.build
      end
    else
      @product             = Manager::Ad::Product.new(user_id: current_user.id)
      @product.price       = 0.0
      @product.offer_price = 0.0
      @product.user        = current_user
      @product.on_request  = false
      @product.is_old      = false
      @product.offer_start = Date.today
      @product.offer_end   = Date.today + 60.days
      @product.interest_title = "Quer saber mais sobre este anúncio exclusivo?"
      @product.interest_text = "Então clique em ESTOU INTERESSADO e fique ligado em nossos anúncios exclusivos."
      if current_user.expired_limit
        @product.published = false
      else
        @product.published = true
      end

      last_prod = current_user.products.last
      if last_prod
        @product.department = last_prod.department
      end

      @product.offers.build
    end
  end

  # Post das informações do anúncio
  def anuncio_2_post
    if params[:product]
      @product = Manager::Ad::Product.find(params[:product])
      @product.update_attributes(product_params)
      @product.department_id = @product.category.department_id

      if @product.save
        @product.create_images
        @product.update_offer
        Rails.cache.clear

        if params[:new_ad]
          redirect_to site_produtos_path(:anuncio_2)
        else
          redirect_to site_anuncios_url(subdomain: false)
        end

      else
        render action: 'anuncio_2'
      end

    else
      @product = Manager::Ad::Product.new(product_params)
      @product.user_id = current_user.id

      if @product.save
        @product.create_images
        @product.department_id = @product.category.department_id
        @product.save
        Rails.cache.clear

        @product.update_offer

        if params[:new_ad]
          redirect_to site_produtos_path(:anuncio_2)
        else
          redirect_to site_anuncios_url(subdomain: false)
        end

      else
        gera_mensagens
        render action: 'anuncio_2'
      end
    end
  end

  # Gera as mensagens de errors
  def gera_mensagens
    flash[:alert] = ''

    if @product.errors[:category].present?
      flash[:alert] += '<i class="fa fa-exclamation-circle"></i> Você deve selecionar uma <strong>categoria</strong> para prosseguir com o cadastro. <br>'
    end

    if @product.errors[:offer_price].present?
      flash[:alert] += '<i class="fa fa-exclamation-circle"></i> O preço de oferta deves ser maior que o campo <strong>Preço</strong>. <br>'
    end

    if @product.errors[:description].present?
      flash[:alert] += '<i class="fa fa-exclamation-circle"></i> Você precisa preencher o campo <strong>descrição</strong>. <br>'
    end

    if @product.errors[:images].present?
      flash[:alert] += '<i class="fa fa-exclamation-circle"></i> Você precisa selecionar ao menos uma <strong>imagem</strong>. <br>'
    end

    flash[:alert] = nil if flash[:alert].blank?
  end

  # Permissão dos parâmetros de usuário
  def user_params
    params.require(:user).permit!
  end

  # Permissão dos parâmetros de produto
  def product_params
    params.require(:manager_ad_product).permit!
  end
end
