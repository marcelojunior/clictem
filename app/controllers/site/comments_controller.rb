class Site::CommentsController < ApplicationController
	skip_before_filter :verify_authenticity_token, :only => [:index, :new, :create]

	def index
		@product  = Manager::Ad::Product.find(params[:product_id])
		@comments = @product.comments.where(question_id: nil, deleted: false).order('id DESC')
	end



	def new
		@comment = Comment.new(product_id: params[:product_id], question_id: params[:question_id], seller_id: params[:seller_id])
	end



	def create
		@comment = Comment.new(interest_params)
		@comment.save

		if @comment.is_anwser?
				email = Mails::ProductMailer.anwser(@comment).deliver
		else
				email = Mails::ProductMailer.comment(@comment).deliver
				@comment.seller.notifications.new.save_email(email)


			Customer.create_lead(@comment.seller, @comment.user.email, {funnel: 1,
            	name: @comment.user.fullname,
            	phone: @comment.user.phone,
            	cellphone: @comment.user.cellphone,
            	genre: @comment.user.genre
            })
		end

	end


	def destroy
		@product = Manager::Ad::Product.find(params[:product_id])
		@user    = @product.user
		if current_user == @user
			@comment = Comment.find(params[:id])
			if @comment.deleted == false
				@comment.deleted = true
			else
				@comment.deleted = false
			end
			@comment.save
		end
	end


	private

	def interest_params
	  params.require(:comment).permit!
	end
end
