class Site::UsuariosController < ApplicationController
  # require 'barby'
  # require 'barby/barcode/code_128'
  # require 'barby/outputter/html_outputter'

  before_action :authenticate_user!, except: [:produtos, :contratar]

  def criar_conta
  end



  def lista_desejos
  end



  # Página principal do usuário
  def anuncios
    @products = current_user.products.includes(:department, :category).order(id: :desc)
  end



  # Edição de dados do usuário
  def meus_dados
    if request.method == 'GET'
      @user = current_user
    elsif request.method == 'POST'
      @user = current_user

      if request.format == 'application/json'
        field = params[:field].to_sym
        @user.send("#{field}=", File.open(params[:user][field].tempfile.path))
        @user.save!
        render json: {files: [@user.to_jq_upload(field)]}, status: :created
      else
        if @user.update_without_password(user_params)
          redirect_to site_minha_conta_url(subdomain: false)
        else

          render action: 'meus_dados'
        end
      end

    end
  end



  # Troca de senha
  def trocar_senha
    if request.method == 'GET'
      @user = current_user
    elsif request.method == 'POST'
      @user = current_user
      @user.password = params[:user][:password]
      @user.password_confirmation = params[:user][:password_confirmation]
      @user.skip_validates = params[:user][:skip_validates]


      if @user.save
        sign_in(@user, :bypass => true)
        redirect_to site_minha_conta_url(subdomain: false)
      else
        render action: 'trocar_senha'
      end

    end
  end



  # Lista de produtos do usuário
  def produtos
    if UsersConstraint.matches?(request)
      subdomain = request.subdomains(0).first
    else
      subdomain = params[:user]
    end


    @seller      = User.first_by_subdomain(subdomain)
    @user_custom = @seller

    if @seller

      if @seller.seller == false and @seller.associated_code == nil
        return redirect_to error_404_path
      end

      # Redireciona para o subdominio
      if @seller.subdomain != subdomain
        return redirect_to user_produtos_url(subdomain: @seller.subdomain), status: :moved_permanently
      end

      if params[:store]

        # Verifica se é um ID ou SUBDOMAIN
        if (params[:store] =~ /^-?\d+(\.\d*)?$/).nil?
          user = User.first_by_subdomain(params[:store])
        else
          user = User.find_by_id(params[:store])
        end

        @user_name = user.name

        @products = Manager::Ad::Product.published.where(user_id: user.id).page(params[:page])

      else
        users = User.where(mall_id: @seller.id).pluck(:id) #Usuários que fazem parte deste centro comercial
        users << @seller.id
        @products = Manager::Ad::Product.published.where('manager_ad_products.user_id in (?) or manager_ad_products.id in (?)', users, @seller.campaigns.pluck(:product_id)).page(params[:page])
      end

      fullname = @seller.pj? ? @seller.name : @seller.name + " " + @seller.last_name.to_s

      if @seller.description.present?
        description = @seller.description
      else
        description = "As melhores Ofertas da empresa #{fullname} estão aqui em #{@seller.city_name} - #{@seller.state_symbol}"

        description = "Página de Anúncios de #{fullname} em #{@seller.city_name} - #{@seller.state_symbol}" if @seller.pf?
      end

      @page_title       = "#{fullname} - #{@seller.city_name} - #{@seller.state_symbol}"
      @page_description = description
      @page_keywords    = @seller.keywords

      @page_og = {
        title: "#{fullname} - #{@seller.city_name} - #{@seller.state_symbol}",
        description:  description,
        type: 'website',
        url: user_produtos_url(subdomain: @seller.subdomain),
        image: "#{@seller.logo_url}"
      }
    else
      redirect_to error_404_url(subdomain: false)
    end
  end



  # Cupons impressos pelo usuário
  def meus_cupons
    @cupons = current_user.my_cupoms
  end



  # Gerar cupom de desconto
  def gen_cupom
    user = current_user
    prod = Manager::Ad::Product.find(params[:product])
    @cupom = current_user.my_cupoms.create(
                  seller_id: prod.user_id,
                  product_id: prod.id,
                  due: prod.offer_end,
                  price: prod.price,
                  offer_price: prod.offer_price,
                  offer_id: prod.current_offer_id
              )
    prod.update_cupom
    CupomEmailWorker.perform_async(@cupom.code, current_org.domain)

    Customer.create_lead(prod.user, user.email, {funnel: 1,
      name: user.fullname,
      phone: user.phone,
      cellphone: user.cellphone,
      genre: user.genre
    })

    respond_to do |format|
      format.js
      format.html{ redirect_to ad_url(prod.user.subdomain, prod.category_slug, prod.slug,subdomain: false, prodCP: true) }
    end
  end


  # Imprimir cupom
  def print_cupom
    @cupom = Manager::Ad::Cupom.find_by_code(params[:code])
    # @barcode = Barby::Code128B.new(@cupom.code)
    # @barcode_for_html = Barby::HtmlOutputter.new(@barcode)
    render layout: 'print'
  end


  # Enviar cupons por e-mail
  def send_cupom
    @cupom = Manager::Ad::Cupom.find_by_code(params[:code])
    CupomEmailWorker.perform_async(params[:code], current_org.domain)
  end



  # Cupons emitidos
  def cupons_clientes
    if params[:search]
      @cupons = current_user.cupoms.search(params[:search])
    else
      @cupons = current_user.cupoms.valid.order(created_at: :desc)
    end
  end



  # Marcar cupom como utilizado
  def used_cupom
    @cupom = Manager::Ad::Cupom.find_by_code(params[:code])
    @cupom.used = Date.today
    @cupom.save
  end


  # Resumo
  def resumo
    date1 = Date.today.beginning_of_month
    date2 = Date.today.end_of_month
    @count_ads = current_user.products.published.count
    @cupons_current = current_user.cupoms.where(created_at: (date1..date2)).count
    @cupons_last    = current_user.cupoms.where(created_at: ((date1 - 1.month)..(date2 - 1.month))).count
    @most_views = Visit.ads_most_views(current_user, date1, date2)

    @visitors = current_user.visits.where(created_at: ((date1 - 4.months)..date2)).group_by_month(:created_at, format: '%B').count
    @ver_telefone = Manager::Ad::InfoClick.where(seller_id: current_user.id, created_at: ((date1 - 4.months).beginning_of_day..date2.end_of_day)).group_by_month(:created_at, format: '%B').count

    @comments = current_user.comments.only_comments.order(created_at: :desc).limit(10)
  end



  # Informações sobre o plano
  def meuplano
    @count_ads = current_user.products.published.count
  end



  # Notificações
  def notificacoes
    @notifications = current_user.notifications.page(params[:page])
  end


  # Notificação
  def notificacao
    @notification = current_user.notifications.find(params[:id])
    @notification.view = true
    @notification.save
  end


private

  def user_params
    params.require(:user).permit(:name, :last_name, :email, :kind, :cpf, :birthday, :genre, :cellphone, :business_name, :cnpj, :website, :phone, :resp_name, :resp_email, :resp_cpf, :resp_birthday, :address_zipcode, :address_street, :address_number, :address_district, :address_compl, :city_id, :state_id, :password, :password_confirmation, :skip_validates, :want_email, :want_sms)
  end

end
