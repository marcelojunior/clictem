#encoding: utf-8
class Offers::CampaignsController < ApplicationController
  before_action :set_offer_campaign, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :verify_pj

  def index
    @campaigns = current_user.offer_campaigns
  end



  def new
    @campaign = Manager::Ad::OfferCampaign.new(date_start: Date.today, date_end: Date.today + 20.days)
  end



  def edit

  end



  def create
    @campaign = Manager::Ad::OfferCampaign.new(offer_campaign_params)
    @campaign.user_id = current_user.id
    if @campaign.save
      @campaign.update_offers
      redirect_to offers_campaigns_path, notice: "Campanha <strong>#{@campaign.title}</strong> criada com sucesso."
    else
      render action: 'new'
    end
  end



  def update
    if @campaign.update(offer_campaign_params)
      @campaign.update_offers
      redirect_to offers_campaigns_path, notice: "Campanha <strong>#{@campaign.title}</strong> atualizada."
    else
      render action: 'edit'
    end
  end


  def destroy
    @campaign.destroy
    redirect_to offers_campaigns_path, notice: "Campanha <strong>#{@campaign.title}</strong> excluída."
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_offer_campaign
      @campaign = current_user.offer_campaigns.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def offer_campaign_params
      params.require(:manager_ad_offer_campaign).permit(:title, :date_start, :date_end, :wday0, :wday1, :wday2, :wday3, :wday4, :wday5, :wday6, :has_cupom, :repeat, offers_attributes: [:id, :product_id, :price, :cupom_note, :has_cupom_limit, :cupom_limit, :has_cupom, :_destroy])
    end

end
