class Sms::BuysController < ApplicationController
  before_action :set_sms_buy, only: [:show, :edit, :update, :destroy]
  before_action :verify_pj

  # GET /sms/buys
  # GET /sms/buys.json
  def index
    @sms_buys = current_user.sms_buys.order(id: :desc).all
    @sms_buy  = Sms::Buy.new(qtd: 150, value: 30)
    @user = current_user
  end

  # GET /sms/buys/1
  # GET /sms/buys/1.json
  def show
  end

  # GET /sms/buys/new
  def new
    @sms_buy = Sms::Buy.new
    @sms_buy.schedule_date = Date.today
    @sms_buy.schedule_time = Time.zone.now + 30.minutes
  end

  # GET /sms/buys/1/edit
  def edit
  end

  # POST /sms/buys
  # POST /sms/buys.json
  def create
    @sms_buy = Sms::Buy.new(sms_buy_params)
    if @sms_buy.qtd >= 150
      @sms_buy.user_id = current_user.id
      @sms_buy.save!

      Mails::SmsMailer.buy(@sms_buy).deliver
    end
  end

  # PATCH/PUT /sms/buys/1
  # PATCH/PUT /sms/buys/1.json
  def update
    @sms_buy.update(sms_buy_params)
    redirect_to sms_buys_path, notice: 'Campanha atualizada'
  end



  # Atualiza dados do usuários
  def update_user
    @user = current_user
    @user.update_without_password(user_params)
    redirect_to sms_buys_path
  end

  # DELETE /sms/buys/1
  # DELETE /sms/buys/1.json
  def destroy
    @sms_buy.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sms_buy
      @sms_buy = current_user.sms_buys.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sms_buy_params
      params.require(:sms_buy).permit(:qtd, :value)
    end


    def user_params
      params.require(:user).permit(:name, :last_name, :email, :kind, :cpf, :birthday, :genre, :cellphone, :business_name, :cnpj, :website, :phone, :resp_name, :resp_email, :resp_cpf, :resp_birthday, :address_zipcode, :address_street, :address_number, :address_district, :address_compl, :city_id, :state_id)
    end
end
