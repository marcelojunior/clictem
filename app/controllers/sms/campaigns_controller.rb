#encoding: utf-8
class Sms::CampaignsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_sms_campaign, only: [:show, :edit, :update, :destroy]
  before_action :verify_pj

  # GET /sms/campaigns
  # GET /sms/campaigns.json
  def index
    @sms_campaigns = current_user.sms_campaigns.order(id: :desc)
    @sms_campaign  = Sms::Campaign.new
  end

  # GET /sms/campaigns/1
  # GET /sms/campaigns/1.json
  def show
    @sms_campaign.responses.update_all(read: true)
  end

  # GET /sms/campaigns/new
  def new
    @sms_campaign = Sms::Campaign.new
    @sms_campaign.schedule_date = Time.zone.now.to_date
    @sms_campaign.schedule_time = Time.zone.now
  end

  # GET /sms/campaigns/1/edit
  def edit
    if @sms_campaign.status != 'avaliando'
      flash[:error] = 'Você não pode mais alterar esta campanha.'
      redirect_to sms_campaigns_path
    end
  end

  # POST /sms/campaigns
  # POST /sms/campaigns.json
  def create
    @sms_campaign = Sms::Campaign.new(sms_campaign_params)
    @sms_campaign.user_id = current_user.id
    if @sms_campaign.save
      @sms_campaign.create_messages
      redirect_to sms_campaigns_path, notice: 'Campanha criada com sucesso.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /sms/campaigns/1
  # PATCH/PUT /sms/campaigns/1.json
  def update
    if @sms_campaign.update(sms_campaign_params)
      @sms_campaign.create_messages
      redirect_to sms_campaigns_path, notice: 'Campanha atualizada.'
    else
      render action: 'edit'
    end
  end

  # DELETE /sms/campaigns/1
  # DELETE /sms/campaigns/1.json
  def destroy
    current_user.save
    # Apaga as mensagens e campanha
    Sms::Message.delete_all(campaign_id: @sms_campaign.id)
    @sms_campaign.destroy
  end



  def birthday
    if request.method == 'POST'
      user = current_user
      if user.update_without_password(user_params)
        redirect_to sms_birthday_path, notice: 'Mensagem de aniversário atualizada'
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sms_campaign
      @sms_campaign = current_user.sms_campaigns.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sms_campaign_params
      params.require(:sms_campaign).permit(:message, :link, :schedule_date, :schedule_time, customer_ids: [], group_ids: [] )
    end

    def user_params
    params.require(:user).permit!
  end
end
