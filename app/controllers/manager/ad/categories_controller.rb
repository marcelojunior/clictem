class Manager::Ad::CategoriesController < Manager::ManagerController
  inherit_resources

  def index
    @departments = Manager::Ad::Department.includes(:categories).all
  end

  def new
    @department = Manager::Ad::Department.find(params[:dept])
    @category = Manager::Ad::Category.new(department: @department)
  end

  def edit
    edit!{@department = @category.department}
  end

  def create
    create!(notice: "Categoria adicionada!") { new_manager_ad_category_path(dept: @category.department_id) }
  end

  def update
    update!(notice: "Categoria atualizada!") { manager_ad_categories_path }
  end

  def permitted_params
    params.permit(manager_ad_category: [:name, :department_id, :parent_id, :on_menu, :keywords, :description, :raw_order])
  end

end
