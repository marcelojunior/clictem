# encoding: utf-8
class Manager::Ad::ProductsController < Manager::ManagerController
  inherit_resources

  def index
    unless params[:search]
      @products = Manager::Ad::Product.includes(:category, :user, :department).order(id: :desc).page(params[:page])
    else
      @products = Manager::Ad::Product.where('LOWER(name) like (?)', "%#{params[:search].downcase}%").includes(:category, :user, :department).page(params[:page])
    end
  end



  def new
    @product = Manager::Ad::Product.new(price: 0.0, offer_price: 0.0)
  end



  def edit
    @product = Manager::Ad::Product.find(params[:id])

    if params[:brand_id]
      @product.brand_id = params[:brand_id]
    end
  end



  def create
    @product = Manager::Ad::Product.new(permitted_params)
    @product.user_id = current_user.id
    if @product.save
      redirect_to manager_ad_products_path, notice: 'Seu anúncio foi cadastrado com sucesso.'
    else
      render action: 'new'
    end
  end




  def update
    @product = Manager::Ad::Product.find(params[:id])
    if @product.update(permitted_params)
      Rails.cache.clear
      redirect_to manager_ad_products_path, notice: 'Seu anúncio foi atualizado!'
    else
      render action: 'edit'
    end
  end




  def permitted_params
    params.require(:manager_ad_product).permit(
      :name,
      :department_id,
      :category_id,
      :image1,
      :image2,
      :image3,
      :image4,
      :image1_cache,
      :image2_cache,
      :image3_cache,
      :image4_cache,
      :remove_image1,
      :remove_image2,
      :remove_image3,
      :remove_image4,
      :code,
      :price,
      :offer_price,
      :on_request,
      :is_old,
      :description,
      :payment_description,
      :views_count,
      :sub_category_id,
      :is_service,
      :brand_id,
      :script
    )
  end
end
