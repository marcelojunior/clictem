class Manager::Ad::SubCategoriesController < Manager::ManagerController
  inherit_resources

  def index
    redirect_to manager_ad_categories_path
  end

  def create
    create!(notice: "Subcategoria adicionada!") { manager_ad_categories_path }
  end

  def update
    update!(notice: "Subcategoria atualizada!") { manager_ad_categories_path }
  end

  def permitted_params
    params.permit(manager_ad_sub_category: [:name, :on_menu, :keywords, :description])
  end
end