class Manager::Ad::DepartmentsController < Manager::ManagerController
  inherit_resources

  def index
    respond_to do |format|
      format.html { @departments = Manager::Ad::Department.all }
      format.json { render json: Manager::Ad::Department.all }
    end
  end

  def show
    respond_to do |format|
      format.html { @department = Manager::Ad::Department.find(params[:id]) }
      format.json { render json: Manager::Ad::Department.find(params[:id]) }
    end
  end

  def create
    create!(notice: "Departamento adicionado!") { new_manager_ad_department_path }
  end

  def update
    update!(notice: "Departamento atualizado!") { manager_ad_departments_path }
  end

  def permitted_params
    params.permit(manager_ad_department: [:name, :keywords, :slug, :raw_order, :on_menu, :description, :is_home, brands_attributes: [:id, :name, :_destroy]])
  end

end
