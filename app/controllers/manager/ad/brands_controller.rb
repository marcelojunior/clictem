class Manager::Ad::BrandsController < Manager::ManagerController
  inherit_resources

  def index
    @brands = Manager::Ad::Brand.all
  end

  def new
    @brand = Manager::Ad::Brand.new
    @brand.name = params[:name] if params[:name]
    @brand.departments << Manager::Ad::Department.find(params[:dept]) if params[:dept]
  end


  def create
    create!(notice: "Marca adicionada!") {

      if !params[:product_id].blank?
        product = Manager::Ad::Product.find(params[:product_id])
        product.brand_id = @brand.id
        product.save
      end

      new_manager_ad_brand_path
    }
  end

  def update
    update!(notice: "Marca atualizada!") { manager_ad_brands_path }
  end

  def permitted_params
    params.permit(manager_ad_brand: [:name, :keywords, :description, :department_ids => []])
  end
end