class Manager::Blog::AuthorsController < Manager::ManagerController
	inherit_resources

	respond_to :html, :json

	def index
		@authors = ::Blog::Author.all
	end

	def new
		@author = ::Blog::Author.new
	end

  def create
    @author = ::Blog::Author.new(permitted_params)
    if @author.save
      redirect_to manager_blog_authors_path, notice: 'Autor cadastrado com sucesso.'
    else
      render action: 'new'
    end
  end

  def edit
		@author = ::Blog::Author.find(params[:id])
	end

  def update
    @author = ::Blog::Author.find(params[:id])
    if @author.update(permitted_params)
      redirect_to manager_blog_authors_path, notice: 'Autor foi atualizado!'
    else
      render action: 'edit'
    end
  end


  def destroy
    @author = ::Blog::Author.find(params[:id])
    @author.destroy
    redirect_to manager_blog_authors_path, notice: 'Autor foi removido.'
  end


	private


	def permitted_params
		params.require(:blog_author).permit!
	end
end