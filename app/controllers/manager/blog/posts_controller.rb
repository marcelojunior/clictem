class Manager::Blog::PostsController < Manager::ManagerController
	inherit_resources

	respond_to :html, :json

	def index
		@posts = ::Blog::Post.all
	end

	def new
		@post = ::Blog::Post.new
	end

  def create
    @post = ::Blog::Post.new(permitted_params)
    if @post.save
      redirect_to manager_blog_posts_path, notice: 'Post cadastrado com sucesso.'
    else
      render action: 'new'
    end
  end

  def edit
		@post = ::Blog::Post.find(params[:id])
	end

  def update
    @post = ::Blog::Post.find(params[:id])
    if @post.update(permitted_params)
      redirect_to manager_blog_posts_path, notice: 'Post foi atualizado!'
    else
      render action: 'edit'
    end
  end


  def destroy
    @post = ::Blog::Post.find(params[:id])
    @post.destroy
    redirect_to manager_blog_posts_path, notice: 'Post foi removido.'
  end


	private


	def permitted_params
		params.require(:blog_post).permit!
	end
end