class Manager::BannersController < Manager::ManagerController
  inherit_resources

  respond_to :html, :json

  def index
    @banners = Banner.unscoped.all
  end

  def new
    @banner = Banner.new(end_date: Date.today + 6.months)
  end


  def create
    @banner = Banner.new(permitted_params)
    if @banner.save
      redirect_to manager_banners_path, notice: 'Banner cadastrado com sucesso.'
    else
      render action: 'new'
    end
  end

  def update
    @banner = Banner.find(params[:id])
    if @banner.update(permitted_params)
      redirect_to manager_banners_path, notice: 'Banner foi atualizado!'
    else
      render action: 'edit'
    end
  end

private
  def permitted_params
    params.require(:banner).permit!
  end

end
