class Manager::ManagerController < ApplicationController
  layout 'manager'
  before_action :authenticate_user!, :autorize

  def autorize
    authorize! :manage, :all
  end
end
