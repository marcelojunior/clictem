class Manager::ThemedCampaignsController < Manager::ManagerController
  # inherit_resources

  respond_to :html, :json

  def index
    @campaigns = Manager::Ad::ThemedCampaign.all.order(:date_start)
  end



  def show
     @campaigns = Manager::Ad::ThemedCampaign.all
  end



  def new
    @campaign = Manager::Ad::ThemedCampaign.new
  end

  def edit
    @campaign = Manager::Ad::ThemedCampaign.find(params[:id])

    if params[:user].present? and params[:status].present?
      altera_pagamento_campanha(@campaign)
    end
  end


  def create
    @campaign = Manager::Ad::ThemedCampaign.new(permitted_params)
    if @campaign.save
      redirect_to manager_themed_campaigns_path, notice: 'Campanha temática cadastrada com sucesso.'
    else
      render action: 'new'
    end
  end



  def update
    @campaign = Manager::Ad::ThemedCampaign.find(params[:id])
    if @campaign.update(permitted_params)
      redirect_to manager_themed_campaigns_path, notice: 'Campanha temática foi atualizada!'
    else
      render action: 'edit'
    end
  end


  def destroy
    @campaign = Manager::Ad::ThemedCampaign.find(params[:id])
    @campaign.destroy
    redirect_to manager_themed_campaigns_path, notice: 'Campanha temática foi excluída!'
  end


private

  # Altera o status de uma campanha
  def altera_pagamento_campanha(campaign)
    user = User.find(params[:user])
    status = params[:status] == "true"
    user.campanha_altera_pagamento(campaign.id, status)
    render action: 'edit'
  end

  def permitted_params
    # params.require(:manager_ad_themed_campaign).permit(:id, :title, :subdomain,
    # :description, :hidden, :color1, :color2, :color3, :img_bg, :img_3,
    # :img_7, :date_start, :date_end, :active, :remove_img_bg, :img_bg_cache,
    # :img_3_cache, :img_7_cache, :value,
    # campaigns_attributes: [:id, :product_id, :_destroy])
    params.require(:manager_ad_themed_campaign).permit!
  end

end
