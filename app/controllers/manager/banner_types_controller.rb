class Manager::BannerTypesController < Manager::ManagerController
  inherit_resources

  respond_to :html, :json

  def index
    @banner_types = BannerType.unscoped.all
  end

  def new
    @banner_type = BannerType.new
  end


  def create
    @banner_type = BannerType.new(permitted_params)
    if @banner_type.save
      redirect_to manager_banner_types_path, notice: 'Tipo de banner cadastrado com sucesso.'
    else
      render action: 'new'
    end
  end

  def update
    @banner_type = BannerType.find(params[:id])
    if @banner_type.update(permitted_params)
      redirect_to manager_banner_types_path, notice: 'Tipo de banner foi atualizado!'
    else
      render action: 'edit'
    end
  end

private
  def permitted_params
    params.require(:banner_type).permit!
  end

end
