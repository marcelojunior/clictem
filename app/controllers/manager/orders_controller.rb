class Manager::OrdersController < Manager::ManagerController
  inherit_resources

  respond_to :html, :json

  def index
    @orders = Order.all.order(id: :desc)
  end



  def show
     @orders = Order.all.order(id: :desc)
  end



  def new
    @order = Order.new

    if params[:kind] == 'Plano'
        @order.kind = 'Plano'
        current_org.order_plan_checklist.each do |list|
            @order.order_checklists.build(name: list[:name], resp: list[:resp], raw_order: list[:order], done: false)
        end
    else
        @order.kind = 'Banner'
        @order.date_end = Date.today + 6.months
        current_org.order_plan_checklist.each do |list|
            @order.order_checklists.build(name: list[:name], resp: list[:resp], raw_order: list[:order], done: false)
        end
    end



  end

  def edit
    @order = Order.find(params[:id])
  end


  def create
    @order = Order.new(permitted_params)
    if @order.save
      if @order.kind == 'Banner'

        total_months = @order.months
        total_months.times do |i|
            Bill.create(
              user: @order.user,
              order: @order,
              value: @order.value,
              commission: WHITELABEL_COMMISSION_BANNER,
              due: Date.today + (i + 1).months,
              note: "#{@order.banner_position} (Parcela #{i + 1} de #{total_months})"
            )
        end

      else
        Bill.create(
          user: @order.user,
          order: @order,
          value: @order.relative_value(Date.today),
          commission: WHITELABEL_COMMISSION_PLAN,
          due: Date.today + 1.month,
          note: "Plano mensal #{@order.plan} de #{l(Date.today, format: :short)} até #{l(Date.today.end_of_month, format: :short)}"
        )

        user = @order.user
        user.plan = @order.plan
        user.save
      end

      redirect_to manager_orders_path, notice: 'Pedido de compra cadastrada com sucesso.'
    else
      render action: 'new'
    end
  end

  def update
    @order = Order.find(params[:id])
    if @order.update(permitted_params)
      redirect_to manager_orders_path, notice: 'Pedido de compra foi atualizado!'
    else
      render action: 'edit'
    end
  end


private
  def permitted_params
    params.require(:order).permit(:id, :user_id, :value, :installment, :note, :done, :kind, :date_start, :date_end, :banner_position, :plan, order_checklists_attributes: [:id, :order_id, :done, :name, :raw_order])
  end

end
