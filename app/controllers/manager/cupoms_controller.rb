class Manager::CupomsController < Manager::ManagerController
  inherit_resources

  respond_to :html, :json

  def index
    @products = Manager::Ad::Product.where(id: Manager::Ad::Cupom.pluck(:product_id).uniq)

    # @cupoms = Manager::Ad::Cupom.order(:seller_id, :product_id)
  end



  def show
     @cupoms = Manager::Ad::Cupom.where(product_id: params[:id]).order(created_at: :desc)
     @product = @cupoms.first.product
  end



private
  def permitted_params
    params.require(:cupom).permit!
  end

end
