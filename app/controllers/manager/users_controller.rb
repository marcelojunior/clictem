# encoding: utf-8
class Manager::UsersController < Manager::ManagerController

  SQL_BUSCA = "LOWER(name) like (:busca) or LOWER(email) like (:busca)"

  def index
    unless params[:search]
      @users = User.all.order(id: :desc).page(params[:page])
    else
      @users = User.where(SQL_BUSCA, busca: "%#{params[:search].downcase}%").page(params[:page])
    end

    respond_to do |format|
        format.html
        format.xls do
          @users = User.all.order(id: :desc)
          response.headers['Content-Disposition'] = 'attachment; filename="usuarios.xls"'
        end
    end
  end



  def clients
    unless params[:search]
      @users = User.where(cancel_date: nil).where.not(plan: "free").order(contract_end: :asc, last_sign_in_at: :asc, sign_in_count: :desc).page(params[:page])
    else
      @users = User.where(cancel_date: nil).where.not(plan: "free").where(SQL_BUSCA, busca: "%#{params[:search].downcase}%").page(params[:page])
    end

    respond_to do |format|
        format.html
        format.xls do
          @users = User.where.not(plan: "free")
          response.headers['Content-Disposition'] = 'attachment; filename="clientes.xls"'
        end
    end
  end


  def associados
    unless params[:search]
      @users = User.where.not(associated_code: "").order(contract_end: :asc, last_sign_in_at: :asc, sign_in_count: :desc).page(params[:page])
    else
      @users = User.where.not(associated_code: "").where(SQL_BUSCA, busca: "%#{params[:search].downcase}%").page(params[:page])
    end

    respond_to do |format|
        format.html
        format.xls do
          @users = User.where.not(plan: "free", associated_code: "")
          response.headers['Content-Disposition'] = 'attachment; filename="associados.xls"'
        end
    end
  end


  # Clientes em prospeção
  def prospection
    @levels = Hash.new
    User::DEAL_LEVELS.each do |level|
      next if level == 'Cliente'
      @levels[level] = User.where(deal_level: level)
    end
  end



  def new
    @user = User.new
    @user.city_id = current_org.city_id
    @user.contract_end = Date.today + 12.months
    @user.ad_limit = 0
    @user.has_sms = false
  end



  def edit
    @user = User.find params[:id]

    if params[:campaign].present? and params[:status].present?
      altera_pagamento_campanha(@user, params[:campaign])
    end
  end



  def create
    @user = User.new(permitted_params)

    begin
      if @user.save!
        @user.create_order_plan(current_org)
        redirect_to manager_users_path, notice: 'Usuário foi criado!'
      else
        render action: 'new'
      end
    rescue Exception => e

        if e.message =~ /[email,UniqueViolation]/
          @user.errors.add(:email, "Este e-mail já existe")
        end
        render action: 'new'
    end

  end



  def update
    @user = User.find(params[:id])

    if @user.update(permitted_params)
      unless @user.phone =~ /\d*/
        @user.phone = ""
      end

      unless @user.phone2 =~ /\d*/
        @user.phone2 = ""
      end

      unless @user.cellphone =~ /\d*/
        @user.cellphone = ""
      end

      redirect_to edit_manager_user_path(@user), notice: 'Usuário foi atualizado!'
    else
      render action: 'edit'
    end
  end


  def change_password
    if request.method == 'GET'
      @user = User.find params[:id]
    else
      @user = User.find params[:id]
      if @user.update(permitted_params)
        redirect_to manager_users_path, notice: "A senha do usuário #{@user.name} foi alterada."
      else
        render action: 'change_password'
      end
    end
  end


  def destroy
    @user = User.find(params[:id])
    @user.destroy
    Rails.cache.clear
    redirect_to manager_users_path, notice: "O usuário #{@user.name} foi eliminado da base de dados."
  end


private
  # Altera o status de uma campanha
  def altera_pagamento_campanha(user, campaign)
    status = params[:status] == "true"
    user.campanha_altera_pagamento(campaign, status)
    render action: 'edit'
  end

  def permitted_params

    params.require(:user).permit!
    # params.require(:user).fetch(:departments, nil).try(:permit!)
  end

end
