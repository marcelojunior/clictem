class Manager::Sms::ContactsController < Manager::ManagerController
    inherit_resources

    respond_to :html, :json

    def import
        if request.method == 'POST'

              org_file = params[:file].original_filename
              user     = User.find(params[:user])

              uploader = SmsContactsUploader.new
              uploader.store!(params[:file])

              if Rails.env.production?
                  tmp_path = "http:#{uploader.url}"
              else
                  tmp_path = uploader.path
              end

              user.import_xls(tmp_path, org_file)
              # SmsContactsImportWorker.perform_async(tmp_path, org_file,params[:user], current_org.domain)
              flash[:success] = "Clientes importados"
              redirect_to manager_sms_contacts_import_path

        end
    end

end
