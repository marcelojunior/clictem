class Manager::Sms::BuysController < Manager::ManagerController
    inherit_resources

    respond_to :html, :json

    def index
        @buys = Sms::Buy.order(created_at: :desc)
    end

    def edit
        @buy = Sms::Buy.find(params[:id])
    end

    def update
        @buy = Sms::Buy.find(params[:id])
        if @buy.update(permitted_params)
          # Atualiza saldo
          @buy.user.sms_hired_due     = Date.today + 90.days

          @buy.user.save

          redirect_to manager_sms_buys_path, notice: (@buy.active ? 'Compra de SMS liberada' : '')
        else
          render action: 'edit'
        end
    end

    private

    def permitted_params
        params.require(:sms_buy).permit!
    end

end
