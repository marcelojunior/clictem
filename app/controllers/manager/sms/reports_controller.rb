class Manager::Sms::ReportsController < Manager::ManagerController
    respond_to :html, :json

    def index
      @date = Date.today

      if params[:date]
        @date = Date.parse("01/#{params[:date][:month]}/#{params[:date][:year]}") 
      end

      date_start = @date.beginning_of_month
      date_end = @date.end_of_month

      users = Sms::Message.total_clients(date_start, date_end)

      @reports = []
      users.each  do |user|
          report = SmsReportViewModel.new
          report.user = user
          report.quantidade = Sms::Message.total_messages(user, date_start, date_end)
          @reports << report
      end
    end
end


class SmsReportViewModel
  attr_accessor :user, :quantidade
end
