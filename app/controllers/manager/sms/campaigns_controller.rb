class Manager::Sms::CampaignsController < Manager::ManagerController
    inherit_resources

    respond_to :html, :json

    def index
        @campaigns = Sms::Campaign.order(created_at: :desc)
    end

    def edit
        @campaign = Sms::Campaign.find(params[:id])
    end

    def update
        @campaign = Sms::Campaign.find(params[:id])
        if @campaign.update(permitted_params)
            redirect_to manager_sms_campaigns_path, notice: (@campaign.status == 'aguardando' ? 'Campanha liberada' : '')
        else
            render action: 'edit'
        end
    end


    def destroy
        @campaign = Sms::Campaign.find(params[:id])

        Sms::Message.delete_all(campaign_id: @campaign.id)
        @campaign.destroy

        redirect_to manager_sms_campaigns_path, notice: 'Campanha excluída'
    end

    private

    def permitted_params
        params.require(:sms_campaign).permit!
    end

end
