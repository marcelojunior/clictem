class Manager::DashboardController < Manager::ManagerController

  def index

    # Sugestões de marcas
    @products = Manager::Ad::Product.where(brand_id: 0).where.not(brand_sugest: nil).where.not(brand_sugest: '').includes(:department, :user)

  end
end
