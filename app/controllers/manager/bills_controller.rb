class Manager::BillsController < Manager::ManagerController
  before_action :configure_dates
  inherit_resources

  respond_to :html, :json

  def index
    @clientes = User.clients.includes(:themed_campaigns)
    @associados = User.associate.where(plan: 'free')
    @campaigns = Manager::Ad::ThemedCampaign.where(active: true, hidden: false)
          .where('date_end >= ?', Date.today)
    @banner_types = BannerType.all
    @departamentos = Manager::Ad::Department.where('on_menu = :true or is_home = :true', true: true)
    #  @bills = Bill.where(due: (@date_start..@date_end)).order(:user_id)

    nome_xls = I18n.l(@date_start, format: '%m-%Y')
    nome_xls = "faturamento-#{nome_xls}"

    respond_to do |format|
        format.html
        format.xls do
          response.headers['Content-Disposition'] = 'attachment; filename="' + nome_xls + '.xls"'
        end
    end
  end



  def show
  end



  def new
    Bill.where(due: (@date_start..@date_end)).delete_all
    data = @date_start
    Bill.generate_bills(data)
    redirect_to manager_bills_path(date: data.to_s)
  end



  def edit

  end



  def create
  end



  def update
  end


private



  def permitted_params
    params.require(:bill).permit(:id, :done)
  end



  def configure_dates
    if params[:date]
      date = Date.today
      if params[:date].class != String
        date = Date.parse("01/#{params[:date][:month]}/#{params[:date][:year]}")
      else
        date = Date.parse(params[:date])
      end
      @date_start = date.beginning_of_month
      @date_end = date.end_of_month

    else
      if params[:date_start]
        @date_start = Date.parse(params[:date_start])
      else
        @date_start = Date.today.beginning_of_month
      end

      if params[:date_end]
        @date_end = Date.parse(params[:date_end])
      else
        @date_end = Date.today.end_of_month
      end
    end
  end

end
