class Manager::PagesController < Manager::ManagerController
  inherit_resources

  respond_to :html, :json

  def index
    @pages = Manager::Page.all
  end

  def new
    @page = Manager::Page.new
  end


  def create
    @page = Manager::Page.new(permitted_params)
    if @page.save
      redirect_to manager_pages_path, notice: 'Página cadastrada com sucesso.'
    else
      render action: 'new'
    end
  end

  def update
    @page = Manager::Page.find(params[:id])
    if @page.update(permitted_params)
      redirect_to manager_pages_path, notice: 'Página foi atualizada!'
    else
      render action: 'edit'
    end
  end

private
  def permitted_params
    params.require(:manager_page).permit(:title, :keywords, :description)
  end

end
