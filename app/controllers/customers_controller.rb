class CustomersController < ApplicationController
  before_action :set_sms_contact, only: [:show, :edit, :update, :destroy]
  before_action :verify_pj

  # GET /customers
  # GET /customers.json
  def index

    if params[:search].present? or params[:group].present?

      # Busca somente por grupos, ao clicar nos grupos
      if params[:group] and params[:group].present? and params[:group] != 'all'
        @group = params[:group]
        @customers = current_user.customers.by_group(@group).page(params[:page])  
      else
        @customers = current_user.customers
      end

      # Busca por nome
      search = nil
      if params[:search] and params[:search].present?
        @search = params[:search]
        @customers = @customers.search(@search).page(params[:page])
      end

      @customers.order(:name)
    else

      # Se não efetuar nenhum filtro, busca todos os contatos
      @customers = current_user.customers.includes(:groups).order(id: :desc).page(params[:page])

    end

    @customer  = Customer.new
  end

  # GET /customers/1
  # GET /customers/1.json
  def show
  end

  # GET /customers/new
  def new
    @customer = Customer.new
  end

  # GET /customers/1/edit
  def edit
  end

  # POST /customers
  # POST /customers.json
  def create
    @customer = Customer.new(sms_contact_params)
    @customer.owner = current_user
    @customer.save!
  end

  # PATCH/PUT /customers/1
  # PATCH/PUT /customers/1.json
  def update
    @customer.update(sms_contact_params)
  end

  # DELETE /customers/1
  # DELETE /customers/1.json
  def destroy
    @customer.destroy
  end

  # Retorna JSON com o total de contatos cadastrados nos grupos selecionados
  def count
    if params[:group_ids] != 'all'
      group_ids = params[:group_ids].split('-')
    end

    render json: Group.customers(current_user, group_ids, true).pluck(:id)
  end



  # Exportar todos para Excel
  def export_excel
    @customers = current_user.customers

    respond_to do |format|
        format.html
        format.xls do
          response.headers['Content-Disposition'] = 'attachment; filename="' + 'lista-de-clientes' + '.xls"'
        end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sms_contact
      @customer = current_user.customers.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sms_contact_params
      params.require(:customer).permit(:active, :name, :phone, :cellphone, :birthday, :email, group_ids: [])
    end
end
