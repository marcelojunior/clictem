class InboxController < ApplicationController
  include Mandrill::Rails::WebHookProcessor
  authenticate_with_mandrill_keys! 'TfNgK0IwhJBp3KQXgw8ABw'

  def create
    if params['mandrill_events']
      events = JSON.parse(params['mandrill_events'] || '[]')
      events.each do |inbound|
        res = []
        res << "event: #{inbound['event']}"
        res << "msg/ts: #{inbound['msg']['ts']}"
        res << "msg/subject: #{inbound['msg']['subject']}"
        res << "msg/email: #{inbound['msg']['email']}"
        res << "msg/state: #{inbound['msg']['state']}"

        if inbound['msg']['diag']
          res << "msg/error: #{inbound['msg']['diag']}"
        else
          res << "msg/error: ''"
        end

        if inbound['event'] == 'deferral'
          res << "msg/error: #{inbound['msg']['smtp_events'][0]['diag']}"
        else
          res << "msg/error: ''"
        end

        if inbound['event'] == 'click' and inbound['msg']['clicks'] and inbound['msg']['clicks'].size > 0
          res << "msg/click: #{inbound['msg']['clicks'][0]['url']}"
        else
          res << "msg/click: ''"
        end

        Inbox.create!(content: res.join(' | ') )
      end

    end
    render text: 'ok', status: :ok
  end
end
