class ApplicationController < ActionController::Base

  # rescue_from ActionController::RoutingError, with: :render_not_found
  # rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
  # rescue_from StandardError, with: :render_server_error

  # http_basic_authenticate_with name: "clictem", password: "bunitasso" if Rails.env.production?

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  layout  :define_layout
  include Site::BreadcrumbHelper

  before_filter :current_org, :load_schema
  before_filter :configure_permitted_parameters, if: :devise_controller?

  skip_after_filter :intercom_rails_auto_include, if: :is_client?

  def is_client?
    if user_signed_in?
      current_user.is_client?
    else
      false
    end
  end

  def define_layout
    if request.format.html?
      if controller_name == 'dashboard'
        'manager'
      # elsif is_custom
      #   'custom'
      else
        'application'
      end
    else
      'application'
    end
  end


  rescue_from CanCan::AccessDenied do |exception|
    redirect_to new_user_session_path, :alert => exception.message
  end


  def after_sign_in_path_for(resource_or_scope)
    if cookies[:prodCP]
      redirect_cupom
    elsif cookies[:prodCM]
      redirect_comment
    else
      # site_minha_conta_url(subdomain: false)
      super
    end
  end

  # Redireciona para ageração de cupom
  def redirect_cupom
    product = Manager::Ad::Product.find cookies[:prodCP]
    cookies.delete :prodCP
    site_usuarios_url(:gen_cupom, subdomain: false, product: product.id, prodCP: true)
  end

  # Redireciona para a página de produtos, para poder comentar
  def redirect_comment
    product = Manager::Ad::Product.find cookies[:prodCM]
    cookies.delete :prodCM
    ad_url(product.user.subdomain, product.category_slug, product.slug, subdomain: false)
  end

  # def permission_denied
  #   render file: "public/401.html", layout: false, status: :unauthorized
  # end

protected

  def configure_permitted_parameters
    [:sign_up, :account_update].each do |action|
      devise_parameter_sanitizer.for(action) {|u| u.permit(
        :email,
        :name,
        :password,
        :password_confirmation,
        :last_name,
        :business_name,
        :trade_name,
        :cpf,
        :cnpj,
        :ie,
        :genre,
        :birthday,
        :phone,
        :cellphone,
        :want_email,
        :want_sms,
        :address_street,
        :address_number,
        :address_district,
        :address_compl,
        :address_zipcode,
        :city_id,
        :state_id,
        :resp_name,
        :resp_birthday,
        :resp_cpf,
        :resp_email,
        :subdomain,
        :plan,
        :ad_limit,
        :kind,
        :reset_password_token,
        roles: [])}
    end
  end




  # # Verifica se a página é de um usuário custom
  # def is_custom
  #   (controller_name == 'usuarios' and action_name == 'produtos' and @user.plan != 'free') or
  #   (controller_name == 'products' and action_name == 'show' and @user.plan == 'free' and @user.mall.present?) or # Usar layout do centro comercial
  #   (controller_name == 'products' and action_name == 'show' and @user.plan != 'free')
  # end


  # Redireciona caso a extensão seja .php
  # def redirect_php
  #   puts request.original_url
  #   if File.extname(request.original_url) == '.php'
  #     redirect_to "/404"
  #   end
  # end

  # def render_error(e)
  #   respond_to do |f|
  #     f.html{ render :template => "errors/500", :status => 500 }
  #     f.js{ render :partial => "errors/ajax_500", :status => 500 }
  #   end
  # end

  def render_not_found(e)
    render file: "#{Rails.root}/public/404", layout: false, status: 404
  end


   # def render_not_found
   #    render "/404", :status => 404
   # end

   # def render_server_error
   #    render "/500", :status => 500
   # end



   # Redirecionar para minha conta quando cliente for PF
   def verify_pj
      if current_user != nil and current_user.kind == 'pf'
        redirect_to site_minha_conta_path, notice: 'Você não tem acesso para este módulo.'
      end
   end

   # https://github.com/mikeholownych/prophoto/blob/master/app/controllers/application_controller.rb
   private
      def load_schema
        domain = request.host.split('.')[-1 - ActionDispatch::Http::URL.tld_length]

        ActionMailer::Base.default_url_options[:domain] = "www.#{domain}.com.br"
        ActionMailer::Base.default_url_options[:host] = "#{domain}.com.br"

        Apartment::Tenant.switch!(domain) if request.subdomain.present?

        if current_org
          Apartment::Tenant.switch!(current_org.domain)
        else
          render file: "#{Rails.root}/public/404-tenant", layout: false, status: 404
        end
      end

      def current_org
        domain = request.host.split('.')[-1 - ActionDispatch::Http::URL.tld_length]
        @current_org ||= Organization.find_by_domain(domain)
      end
      helper_method :current_org

end
