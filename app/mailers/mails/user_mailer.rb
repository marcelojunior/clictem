class Mails::UserMailer < MailBase
  add_template_helper(Mails::NumberHelper)
  # default from: "#{current_org.fullname} <#{current_org.email}>"

  # Bem vindo
  def welcome(user)
  	@user = user
  	mail(
  		to: "#{@user.name} <#{@user.email}>",
      from: "#{current_org.fullname} <#{current_org.email}>",
  		subject: "[ #{ current_org.fullname } ] #{@user.name}, seja bem vindo!"
  	)
  end

  # E-mail Semanal
  def semanal(user, date, debug = false)
    @user = user
    @sm2_date1 = date - 7.days
    @sm2_date2 = date
    @sm1_date2 = @sm2_date1 - 1.days
    @sm1_date1 = @sm2_date1 - 8.days
    @ads    = Visit.ads_most_views(user, @sm2_date1, @sm2_date2)
    mail(
      to: debug_to(user, debug),
      from: "#{current_org.fullname} <#{current_org.email}>",
      subject: "[ #{ current_org.fullname } ] #{@user.name}, este é o seu relatório de sucesso (#{Date.today.strftime('%d/%m/%Y')})"
    )
  end


  # Como compartilhar em redes sociais
  def social_share(user)
  	@user = user
  	mail(
  		to: "#{@user.name} <#{@user.email}>",
      from: "#{current_org.fullname} <#{current_org.email}>",
  		subject: "[ #{ current_org.fullname } ] #{@user.name}, utilize o Facebook e obtenha melhores resultados"
  	)
  end



  # Produtos com pouca visualização
  def poor_views(user, products)
    @products = products
    @user     = user
    mail(
      to: "#{@user.name} <#{@user.email}>",
      cc: email_cc(@user),
      from: "#{current_org.fullname} <#{current_org.email}>",
      subject: "[ #{ current_org.fullname } ] #{@user.name}, melhore as visualizações do seu anúncio"
    )
  end



  # Pedindo para cadastrar produtos
  def create_ads(user)
    @user = user
    mail(
      to: "#{@user.name} <#{@user.email}>",
      cc: email_cc(@user),
      from: "#{current_org.fullname} <#{current_org.email}>",
      subject: "[ #{ current_org.fullname } ] #{@user.name}, vamos anuncinar no #{current_org.fullname}?"
    )
  end



  # Mais de 7 dias sem acessar
  def user_miss(user)
    @user = user
    mail(
      to: "#{@user.name} <#{@user.email}>",
      cc: email_cc(@user),
      from: "#{current_org.fullname} <#{current_org.email}>",
      subject: "[ #{ current_org.fullname } ] #{@user.name}, estamos sentindo sua falta"
    )
  end



  # Enviar para PJ pedindo para cadastrar mais produtos
  def pj_more_products(user)
    @user = user
    mail(
      to: "#{@user.name} <#{@user.email}>",
      cc: email_cc(@user),
      from: "#{current_org.fullname} <#{current_org.email}>",
      subject: "[ #{ current_org.fullname } ] #{@user.name}, você pode cadastrar mais anúncios"
    )
  end



  # Mais de dois dias sem responder um comentário
  def wait_comment(user, comments = nil)
    @user     = user
    @comments = []
    date      = Date.today - 3.days

    unless comments
      @user.comments.where(created_at: (date.beginning_of_day..date.end_of_day) ).each do |comment|
        if comment.anwsers.count == 0
          @comments << comment
        end
      end
    else
      @comments = comments
    end

    if @comments.size > 0
      mail(
        to: "#{@user.name} <#{@user.email}>",
        cc: email_cc(@user),
        from: "#{current_org.fullname} <#{current_org.email}>",
        subject: "[ #{ current_org.fullname } ] #{@user.name}, existem comentários aguardando resposta"
      )
    end
  end



  # Envia cupom para usuário
  def cupom(cupom)
    @cupom = cupom
    @user  = cupom.user

    mail(
      to: "#{@user.name} <#{@user.email}>",
      from: "#{current_org.fullname} <#{current_org.email}>",
      subject: "[ #{ current_org.fullname } ] #{@user.name}, seu #{ @cupom.due ? 'cupom de desconto' : 'comprovante de interesse' } para #{@cupom.product.name}"
    )
  end


  # Quando um cliente deixa contato na tela de campanha
  def themed_campaign(customer, campaign)
    @customer = customer
    @campaign = campaign

    if Rails.env.production?
      mail(
        to: "#{current_org.mkt_email}, <Sandro> sm@mattiolo.com",
        from: "#{current_org.fullname} <#{current_org.email}>",
        subject: "[ #{ current_org.fullname } ] #{@customer.name} solicitou participação na campanha #{@campaign.title}"
      )
    end
  end


  # Envia ao cliente todos os dados do Clictem
  def new_plan_to_client(user)
    @user = user
    mail(
      to: "#{@user.name} <#{@user.email}>",
      from: "#{current_org.fullname} <#{current_org.email}>",
      subject: "[ #{ current_org.fullname } ] #{@user.name}, informações sobre seu plano"
    )
  end


  # Envia para o financeiro e marketing o contato do cliente.
  def new_plan_to_clictem(user)
    @user = user

    if Rails.env.production?
      mail(
        to: "#{current_org.mkt_email}, #{current_org.fin_email}",
        from: "#{current_org.fullname} <#{current_org.email}>",
        subject: "[ #{ current_org.fullname } ] Novo plano adquirido. #{@user.fullname}"
      )
    end
  end


  # Informação sobre alteração de plano
  def update_plan_to_clictem(user, plan)
    @user = user
    @plan = plan

    if Rails.env.production?
      mail(
        to: "#{current_org.mkt_email}, #{current_org.fin_email}",
        from: "#{current_org.fullname} <#{current_org.email}>",
        subject: "[ #{ current_org.fullname } ] Solicitação de alteração de plano. #{@user.fullname}"
      )
    end
  end

  # E-Mail mkt com lista de produtos. É enviado para todos os usuários
  def mkt_weekly(user, products)
    # Todos os usuários que aceitaram receber E-Mail
    # users = User.where(want_email: true)
    # user = User.find_by_email('marcelo.machado.jr@gmail.com')
    # products = Manager::Ad::Product.client_produtcs
    @user = user
    @products = products
    mail(
      to: "#{user.name} <#{user.email}>",
      from: "#{current_org.fullname} <#{current_org.email}>",
      subject: "[ #{ current_org.fullname } ] #{user.name}, separamos alguns anúnios para você. #{Date.today.strftime('%d/%m/%Y')}"
    )
  end


  # E-Mails para Checklist que ainda estão faltando
  def order_checklist
    @orders = Order.open_orders

    if Rails.env.production?
      mail(
        to: "#{current_org.mkt_email}, #{current_org.fin_email}",
        from: "#{current_org.fullname} <#{current_org.email}>",
        subject: "[ #{ current_org.fullname } ] Pedidos de compra em aberto #{Date.today.strftime('%d/%m/%Y')}"
      )
    else
      mail(
        to: "Tecnologia Clictem <tecnologia@clictem>",
        from: "#{current_org.fullname} <#{current_org.email}>",
        subject: "[ #{ current_org.fullname } ] Pedidos de compra em aberto #{Date.today.strftime('%d/%m/%Y')}"
      )
    end
  end


  # E-Mails com cópia
  def email_cc(user)
    cc = ''

    if user.resp_email != user.email and user.resp_email.present?
      cc += "#{user.resp_name} <#{user.resp_email}>"
    end

    if user.resp2_email != user.email and user.resp2_email.present?
      cc += ",#{user.resp2_name} <#{user.resp2_email}>"
    end

    cc
  end

  # Se o parametro debug estiver marcado como true, o sisitema envia o e-mail para marcelo.mchado.jr@gmail.com
  def debug_to(user, debug = false)
    if debug
      "Marcelo <marcelo.machado.jr@gmail.com>"
    else
      "#{user.name} <#{user.email}>"
    end
  end

end
