class Mails::SmsMailer < MailBase
  # default from: "#{current_org.fullname} <#{current_org.email}>"

  # Nova Campanha
  def new_campaign(campaign)
    @campaign = campaign
    mail(
        to: "Tecnologia Clictem <tecnologia@clictem>",
        from: "#{current_org.fullname} <#{current_org.email}>",
        subject: "[ #{ current_org.fullname } ] Aprovação de uma campanha #{Date.today.strftime('%d/%m/%Y')}"
    )
  end



  # Compra de SMS
  def buy(buy)
      @buy = buy
      mail(
          to: "Tecnologia Clictem <tecnologia@clictem>",
          from: "#{current_org.fullname} <#{current_org.email}>",
          subject: "[ #{ current_org.fullname } ] Compra de créditos SMS #{Date.today.strftime('%d/%m/%Y')}"
      )
  end

end
