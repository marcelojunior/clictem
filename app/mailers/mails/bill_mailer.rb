class Mails::BillMailer < MailBase
  # default from: "#{current_org.fullname} <#{current_org.email}>"

  # Nova Campanha
  def faturamento(bills)
    @data = Date.today - 1.month
    @date_start = @data.beginning_of_month
    @bills = bills
    to = 'Sandro <sm@mattiolo.com.br>, Guilherme <gamattiolo@gmail.com>, '
    to += current_org.fin_email
    mail(
        to: to,
        from: "#{current_org.fullname} <#{current_org.email}>",
        subject: "[ #{ current_org.fullname } ] Faturamento #{@data.strftime('%m/%Y')}"
    )
  end

end
