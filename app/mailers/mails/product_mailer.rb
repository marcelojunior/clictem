class Mails::ProductMailer < MailBase
  # default from: "#{current_org.fullname} <#{current_org.email}>"


  def interest(int)
  	@product = int.product
  	@user    = @product.user
  	@sender  = int
  	mail(
  		to: "#{@user.name} <#{@user.email}>",
      cc: email_cc(@user),
      from: "#{current_org.fullname} <#{current_org.email}>",
  		subject: "[ #{ current_org.fullname } ] #{@user.name}, você recebeu uma nova mensagem em seu anúncio."
  	)
  end


  def comment(comment)
    @product = comment.product
    @user    = comment.user
    @owner   = @product.user
    @comment = comment
    mail(
      to: "#{@owner.name} <#{@owner.email}>",
      cc: email_cc(@owner),
      from: "#{current_org.fullname} <#{current_org.email}>",
      subject: "[ #{ current_org.fullname } ] #{@owner.name}, seu anúncio recebeu um comentário."
    )
  end


  def anwser(anwser)
    @product = anwser.product
    @user    = anwser.user
    @owner   = @product.user
    @anwser  = anwser
    mail(
      to: "#{@user.name} <#{@user.email}>",
      cc: email_cc(@user),
      from: "#{current_org.fullname} <#{current_org.email}>",
      subject: "[ #{ current_org.fullname } ] #{@user.name}, seu comentário foi respondido."
    )
  end


  def report_basic(user, date1, date2)
    @user   = user
    @date1  = date1
    @date2  = date2
    @visits = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
    @ads    = Visit.ads_most_views(user, date1, date2)

    mail(
      to: "#{@user.name} <#{@user.email}>",
      cc: email_cc(@user),
      from: "#{current_org.fullname} <#{current_org.email}>",
      subject: "[ #{ current_org.fullname } ] #{@user.name}, este é o seu relatório de sucesso (#{Date.today.strftime('%d/%m/%Y')})"
    )
  end


  def report_complete(user, date1, date2, custom_email = nil)
    @user   = user
    @date1  = date1
    @date2  = date2
    @visits = Visit.where(owner: user.id, created_at: (date1.beginning_of_day..date2.end_of_day))
    @ads    = Visit.ads_most_views(user, date1, date2)

    unless custom_email
      mail(
        to: "#{@user.name} <#{@user.email}>",
        cc: email_cc(@user),
        from: "#{current_org.fullname} <#{current_org.email}>",
        subject: "[ #{ current_org.fullname } ] #{@user.name}, este é o seu relatório de sucesso (#{Date.today.strftime('%d/%m/%Y')})"
      )
    else
      mail(
        to: custom_email,
        from: "#{current_org.fullname} <#{current_org.email}>",
        subject: "[ #{ current_org.fullname } ] Relatório de sucesso do anunciante #{@user.name}"
      )
    end
  end


  # Relatório enviado logo após o final das campanhas de cupom
  def report_cupoms(user)
    # parse('09/03/2015')
    @user   = user
    @offers = @user.offers.where(has_cupom: true, date_end: Date.parse('09/03/2015'))
    @date1  = @offers.pluck(:date_start).sort.first
    @date2  = Date.parse('09/03/2015')
    @visits = Visit.where(owner: user.id, created_at: (@date1.beginning_of_day..@date2.end_of_day), ad: @offers.pluck(:product_id).uniq)
    @ads    = Visit.views_products(@offers.pluck(:product_id).uniq, @user, @date1, @date2)

    mail(
      to: "#{@user.name} <#{@user.email}>",
      cc: email_cc(@user),
      from: "#{current_org.fullname} <#{current_org.email}>",
      subject: "[ #{ current_org.fullname } ] #{@user.name}, veja como foram suas campanhas de cupom (#{Date.today.strftime('%d/%m/%Y')})"
    )
  end


  # E-Mails com cópia
  def email_cc(user)
    cc = ''

    if user.resp_email != user.email and user.resp_email.present?
      cc += "#{user.resp_name} <#{user.resp_email}>"
    end

    if user.resp2_email != user.email and user.resp2_email.present?
      cc += ",#{user.resp2_name} <#{user.resp2_email}>"
    end

    cc
  end

end
