class MailBase < ActionMailer::Base

    def self.current_org
        @current_org ||= Organization.find_by_domain(Apartment::Tenant.current)
        @url = "#{@current_org.domain}.com.br"

        ActionMailer::Base.default_url_options[:domain] = "www.#{@url}"
        ActionMailer::Base.default_url_options[:host] = @url

        @current_org
    end

    def current_org
        @current_org ||= Organization.find_by_domain(Apartment::Tenant.current)
        @url = "#{@current_org.domain}.com.br"

        ActionMailer::Base.default_url_options[:domain] = "www.#{@url}"
        ActionMailer::Base.default_url_options[:host] = @url

        @current_org
    end

end
