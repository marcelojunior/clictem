module ApplicationHelper

  # Exibir mensanges de notificação no layout
  def hlp_application_flash_message
    flash.map do |key, msg|
        content_tag :div, msg, :id => key, :class => 'callout callout-success'
    end.join.html_safe
  end


  # Botão de submit
  def hlp_application_btn_submit(name)
    "<button type='submit' class='btn btn-primary'><i class='fa fa-floppy-o'></i> SALVAR <strong>#{name.upcase}</strong></button>".html_safe
  end


  # Exibir mensagens
  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym]
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} alert-dismissible fade in", role: 'alert') do
              concat content_tag(:button, '×', class: "close", data: { dismiss: 'alert' }, 'aria-label' => 'Close')
              concat raw(message)
            end)
    end
    nil
  end



  def hlp_has_banner? (banner)
    (banner and Date.today <= banner[:expire])
  end

  # TODO: refatorar esses métodos abaixo, criar tipo um restrict_banner? (mais semantico)
  def hlp_not_my_account?
    (
      controller_name != 'page' and
      action_name != 'minha_conta' and
      action_name != 'anuncio_1' and
      action_name != 'anuncio_2' and
      action_name != 'anuncio_3' and
      action_name != 'anuncio_4' and
      action_name != 'anuncio_5' and
      action_name != 'meus_dados' and
      controller.class.name.split("::").first != "Sms" and
      controller.class.name.split("::").first != "Offers" and
      (controller_name != 'usuarios' and action_name != 'produtos')
    )
  end

  def hlp_not_sessions?
    (controller_name != 'registrations' and controller_name != 'sessions')
  end

  def hlp_is_sessions?
    !hlp_not_sessions?
  end

  def hlp_is_my_account?
    (request.path =~ /minha-conta/) and user_signed_in?
  end



  # Retornar banners de menu válido
  def hlp_banner_menu
    banner_type = BannerType.find_by_name('Banner 7')

    banners = Banner.where(banner_type_id: banner_type)
                    .where('end_date >= ?', Date.today)
                    .includes(:department)
                    .order('RANDOM()')

    a_banners = []
    deps      = []
    achou_menu = false # Marca ao achar um banner de departamento "Mais Departamentos"

    banners.each do |banner|
      next if banner.department == nil
      next if achou_menu == true and banner.department.on_menu == false
      if !deps.include?(banner.department_id)
        a_banners << banner
        deps << banner.department_id
      end

      if banner.department.on_menu == false
        achou_menu = true
      end

    end

    a_banners
  end


  # Banners exibidos no meio da página
  def hlp_banner_page(banner_position, department = nil)
    banner_type = BannerType.find_by_name(banner_position)

    if banner_type
      if department
        banner = Banner.where(banner_type_id: banner_type.id, department_id: department).where('end_date >= ?', Date.today).order('RANDOM()').first
      else
        banner = Banner.where(banner_type_id: banner_type.id).where('end_date >= ?', Date.today).order('RANDOM()').first
      end
    end

    if banner
      render('/layouts/partials/banner_page', banner: banner)
    else
      ''
    end
  end

  # Helpers para utilizar o Devise com formulários Ajax
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end


end
