module Site::ProdutosHelper

  # Seleção da primeira etapa - Dados do usuário.
  def hlp_site_produtos_anuncio_1
    action_name == 'anuncio_1' ? 'active' : ''
  end



  # Seleção da segunda etapa - Categorias.
  def hlp_site_produtos_anuncio_2
    classes = ''
    classes += 'active ' if action_name == 'anuncio_2'
    classes += 'disabled ' if !current_user.valid?
    classes
  end

  # Exibir label de status
  def hlp_site_produtos_published(product)
    if product.published?
      content_tag(:span, 'Publicado', class: 'label label-success')
    else
      content_tag(:span, 'Não publicado', class: 'label label-danger')
    end
  end


  # Seleção da terceira etapa - Informações sobre o anúncio.
  def hlp_site_produtos_anuncio_3
    classes = ''
    classes += 'active ' if action_name == 'anuncio_3'

    unless @product
      classes += 'disabled '
    end

    if @product and @product.new_record?
      classes += 'disabled '
    end

    classes
  end



  # Seleção da quarta etapa - Imagens.
  def hlp_site_produtos_anuncio_4
    classes = ''
    classes += 'active ' if action_name == 'anuncio_4'

    unless @product
      classes += 'disabled '
    end

    if @product
      if @product.new_record? or @product.name.blank?
        classes += 'disabled '
      end
    end

    classes
  end



  # Seleção da quinta etapa - Publicar.
  def hlp_site_produtos_anuncio_5
    classes = ''
    classes += 'active ' if action_name == 'anuncio_5'

    unless @product
      classes += 'disabled '
    end

    if @product
      if @product.new_record? or @product.name.blank?
        classes += 'disabled '
      end
    end

    classes
  end



  # Exibir label de produto vendido
  def hlp_site_produtos_sold(product)
    if !product.sold?
      content_tag(:span, 'Disponível', class: 'label label-info')
    elsif product.published?
      content_tag(:span, 'Vendido', class: 'label label-default')
    end
  end


  # Exibir informações sobre a oferta
  def hlp_site_produtos_offer(product)
    if product.is_offer?
      if product.offer_valid?
        content_tag(:span, 'Em Oferta', class: 'label label-success')
      elsif product.published?
        content_tag(:span, 'Oferta Expirou', class: 'label label-danger')
      end
    else
      content_tag(:span, 'Preço Normal', class: 'label label-info')
    end
  end


  # Exibir telefone incompleto do anunciante
  def hlp_site_produts_phone(user)
    if !user.phone.blank?
      "<i class='fa fa-phone'></i> #{ user.phone.truncate(12) }".html_safe
    else
      "<i class='fa fa-mobile'></i> #{ user.cellphone.truncate(12) }".html_safe
    end
  end


  # Se não for um usuário free, será exibido o nome do anunciante no link.
  def hlp_site_produtos_url(product)
    ad_url(product.user.subdomain, product.category_slug, product.slug, subdomain: false)
  end



  # Dados estatísticos do Facebook
  def hlp_produtos_facebook(product, field)
    doc = Nokogiri::HTML(open("http://api.facebook.com/restserver.php?method=links.getStats&urls=#{hlp_site_produtos_url(product)}"))
    doc.at_xpath("//link_stat/#{field}").content
  end
end
