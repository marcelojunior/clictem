module Site::ActiveHelper
  def hlp_active_menu(path)
    'active' if request.url == path
  end
end
