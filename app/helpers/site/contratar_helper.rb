module Site::ContratarHelper

    def hlp_selected_banner?(id)
        if session[:banners]
            session[:banners].include?(id.to_s)
        else
            false
        end
    end


    def hlp_selected_department?(bannerId, deptId)
       if session[:depts] and session[:depts][bannerId.to_s]
            session[:depts][bannerId.to_s].include?(deptId.to_s)
        else
            false
        end
    end

end
