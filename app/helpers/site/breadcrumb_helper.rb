module Site::BreadcrumbHelper
  # Adiciona as páginas ao breadcrumb. Não é necessário adicionar a home
  def hlp_site_breadcrumb_add(title, url)
    unless @bc_pages
      @bc_pages = []
    end

    @bc_pages << { :title => title, :url => url }
  end

  # Renderiza o breadcrumb. Passar o título da página atual como parâmetro
  def hlp_site_breadcrumb_render(active)
    content_tag(:div, class: 'row') do
      content_tag(:div, class: 'breadcrumbDiv col-md-12') do
        content_tag(:ul, class: 'breadcrumb', :'xmlns:v'=>"http://rdf.data-vocabulary.org/#") do

          concat(content_tag(:li, typeof:"v:Breadcrumb") do # Home
            concat(content_tag(:span, 'Você está em: &nbsp;'.html_safe, class: 'breadcrumb-text color1'))
            if @user and @user.plan != 'free'
              concat(content_tag(:a, @user.name, href: user_produtos_url(subdomain: @user.subdomain), title: 'Página do anunciante', rel:'v:url', property:'v:title', class: 'color2'))
            else
              concat(content_tag(:a, 'Página Inicial', href: root_url(subdomain: false), title: 'Página Inicial', rel:'v:url', property:'v:title', class: 'color2'))
            end
          end)

          if @bc_pages
            @bc_pages.each do |page|
              concat(content_tag(:li, typeof:"v:Breadcrumb") do
                concat(content_tag(:a, page[:title], href: page[:url], title: page[:title], rel:'v:url', property:'v:title', class: 'color2'))
              end)
            end
          end

          concat(content_tag(:li, active, class: 'active color3')) # Texto da página ativa
        end
      end
    end
  end
end
