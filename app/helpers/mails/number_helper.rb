module Mails::NumberHelper

  # Calcula a diferença em % entre dois números
  def hlp_diference_percentage(number1, number2)
    number1 = 1.0 if number1 == 0 and number2 > 0
    resultado = (number2.to_f - number1.to_f) / number1.to_f * 100
    resultado.nan? ? 0.0 : resultado
  end

  # Gera um SPAN com a melhoria ou perda de percentual
  def hlp_number_percentage_progress(number1, number2)
    diferenca = hlp_diference_percentage(number1, number2)

    arrow = up_arrow = "&#8673;"
    down_arrow = "&#8675;"
    pause = "&#10074;"

    color = up_color = "#039614"
    down_color = "#c40909"
    pause_color = "#6b6b6b"

    if diferenca < 0.0
      arrow = down_arrow
      color = down_color
    elsif diferenca == 0.0
      arrow = pause
      color = pause_color
    end

    diferenca = sprintf( "%0.02f", diferenca)  

    "<span style='color: #{color};'>#{diferenca}% #{arrow}</span>".html_safe

  end
end
