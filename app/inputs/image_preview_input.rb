class ImagePreviewInput < SimpleForm::Inputs::FileInput
  def input(wrapper_options)
    # :preview_version is a custom attribute from :input_html hash, so you can pick custom sizes
    version = input_html_options.delete(:preview_version)
    input_html_classes << "form-control"
    out = '' # the output string we're going to build
    # check if there's an uploaded file (eg: edit mode or form not saved)
    class_name = object.class.to_s.downcase.gsub('::', '-')
    if object.send("#{attribute_name}?")
      # append preview image to output
      out << "<div class='clearfix image-preview' style='text-align: center; margin-bottom: 10px;'>"
      out << template.image_tag(object.send(attribute_name).tap {|o| break o.send(version) if version}.send('url'), class: 'col-md-12')
      out << "</div>"
      out << "<div class='clearfix' style='text-align: center; margin-bottom: 10px;'>"
      out << "<label>#{ @builder.check_box("remove_#{attribute_name}").html_safe } Remover imagem</label>"
      out << "</div>"
      out << '<small>Para alterar a imagem, selecione abaixo</small>'
    else
      out << "<div class='clearfix image-preview' style='text-align: center; margin-bottom: 10px;'></div>"
    end
    # allow multiple submissions without losing the tmp version
    out << @builder.hidden_field("#{attribute_name}_cache").html_safe
    # append file input. it will work accordingly with your simple_form wrappers
    out << @builder.file_field(attribute_name, input_html_options)
    out << "<div class='progress #{attribute_name} hide'>"
    out << "<div class='progress-bar' role='progressbar' aria-valuemin='0' aria-valuemax='100' style='width: 0%;'>"
    out << "</div></div>"
    out.html_safe
  end
end
