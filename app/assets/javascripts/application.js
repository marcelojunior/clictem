//### DEPENDENCIAS
//= require jquery/dist/jquery.min
//= require jquery_ujs
//= require ./components/bootstrap-sprockets
//= require site/navegg/script

//= require jquery-infinite-scroll/jquery.infinitescroll.min
//= require idangerous-swiper/dist/idangerous.swiper-2.1.min
//= require jquery-cycle2/build/jquery.cycle2
//= require site/theme/jquery.easing.1.3
// require site/theme/helper-plugins/jquery.mousewheel.min
//= require site/theme/owl.carousel.min
// require elevatezoom/jquery.elevatezoom
//= require fancybox/source/jquery.fancybox.pack
//= require fancybox/source/helpers/jquery.fancybox-thumbs.js

// ### PAGINAS
//= require site/pages/main

// ### PRODUTOS
//= require site/products/main


(function (window, document, $, undefined) {
  "use strict";

  var inProduction = 'clictem.com' === window.location.hostname;
  var inStage = 'anuncios.link' === window.location.hostname;
  var inDevMode = ('?development' === window.location.search);
  var inProductionAndNotDevMode = inProduction && !inDevMode;

  if (inProductionAndNotDevMode || inStage) {
    var console = {};

    console.assert = console.clear = console.constructor = console.count = console.debug = console.dir = console.dirxml = console.error = console.group = console.groupCollapsed = console.groupEnd = console.info = console.log = console.markTimeline = console.profile = console.profileEnd = console.table = console.time = console.timeEnd = console.timeStamp = console.timeline = console.timelineEnd = console.trace = console.warn = function () { return false; };
  }

})(window, document, window.jQuery);
