$(function(){

  // Marcar as etapas no rodapé
  function markSteps(){
      if( $('.step-info').size() > 0 ){

         $(".bs-wizard-step").removeClass('complete');
         $(".bs-wizard-step").addClass('disabled');

         var step = $('.step-info:visible').attr('data-step');
         step = parseInt(step);

         var i = 1;
         do {
             $(".bs-wizard-step.step-" + i).removeClass('disabled');
             $(".bs-wizard-step.step-" + i).addClass('complete');
             i++;
         }
         while (i <= step);

      };
  }
  markSteps();

  $(".btn-formulario").click(function(){
    window.scrollTo(0,0)
    if( $(".contratar-formulario").hasClass('hide') ){
      $(".contratar-formulario").removeClass('hide');
      $(".contratar-aceite").addClass('hide');
    }else{
      $(".contratar-formulario").addClass('hide');
      $(".contratar-aceite").removeClass('hide');
    }

    markSteps();
  });


  $(".btn-aceite").click(function(){
    window.scrollTo(0,0)
    if( $(".contratar-aceite").hasClass('hide') ){
      $(".contratar-resumo").addClass('hide');
      $(".contratar-aceite").removeClass('hide');
    }else{
      $(".contratar-aceite").addClass('hide');
      $(".contratar-resumo").removeClass('hide');
    }

    markSteps();
  });


  // Banners -------------------------
  function toggleBanner(bannerId){
    var klass = '.banner-' + bannerId;
    var checkbox = $(klass).find('.check-banner');

    if( $(checkbox).is(':checked') ){
      unselectBanner(bannerId);
    }else{
      selectBanner(bannerId);
    }
  }

  function selectBanner(bannerId){
    var klass = '.banner-' + bannerId;
    var checkbox = $(klass).find('.check-banner');
    $(checkbox).prop("checked",true);
    enableContinuar();
  }

  function unselectBanner(bannerId){
    var klass = '.banner-' + bannerId;
    var checkbox = $(klass).find('.check-banner');
    $(checkbox).prop("checked",false);
    enableContinuar();
  }

  // Habilita o botão continuar se tiver algum banner selecionado
  function enableContinuar(){
    if( $('.check-banner:checked').size() > 0 ){
      $('.btn-continuar').prop('disabled', false);
    }else{
      $('.btn-continuar').prop('disabled', true);
    }
  }

  $(document.body).on('change', '.check-dept', function(){
    var bannerId = $(this).attr('data-banner');
    if ($('.check-dept:visible:checked').size() > 0){
      selectBanner(bannerId);
    }else{
      unselectBanner(bannerId);
    }
    enableContinuar();
  });

  $(document.body).on('change', '.check-banner', function(){
    if ( this.checked == false ){
      $('.check-dept:visible:checked').prop('checked', false);
    }
    enableContinuar();
  })



  // Verifica se todos os campos obrigatórios estão preenchidos
  var countRequired = 0;
  function allRequiredEmpty(){
    countRequired = 0;
    $('#new_user input.required:visible, form[id^=edit_user] input.required:visible').each(function(){
      if($(this).val() == ''){
        countRequired++
      }
    });
  }

  $(document.body).on('change', '#new_user input, form[id^=edit_user] input', function(){
    botaoContinuar();
  });

  // Se não existir nenhum campo obrigatório em branco, o botão de continuar fica ativo
  function botaoContinuar(){
    allRequiredEmpty();
    setTimeout(function(){
      if (countRequired == 0){
        $('.btn-continuar').removeAttr('disabled');
        $('.btn-continuar').attr('style', '');
      }else{
        $('.btn-continuar').attr('disabled', true);
      }
    }, 500);
  }
  botaoContinuar();

});
