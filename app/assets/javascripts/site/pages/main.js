//= require site/theme/home
//= require site/theme/script
//= require site/pages/usuario/custom
//= require site/pages/contratar

(function (window, document, $, undefined) {
  "use strict";

  // Desabilitar links
  $('a[disabled]').css('pointer-events', 'none');
  $('a[disabled]').css('cursor', 'default');

  $("body").on('click', "a[href=#]", function (e) {e.preventDefault(); });


  /*==========  HABILITAR PLUGIN BOOTSTRAP  ==========*/
  // Executar Popovers
  $('body').popover({
    trigger: "hover",
    container: 'body',
    selector: '[data-toggle="popover"]'
  });


  // bootstrap tooltip
  $('body').tooltip({
    selector: '.has-tooltip'
  });


  // Slide das campanhas da home
  $(".slide-campaign").owlCarousel({
      autoPlay: 3000,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true
  });
  $(".slide-campaign").removeClass('hide');


  // Slide das empresas parceiras da home
  $(".slide-clients").owlCarousel({
      center: true,
      autoPlay: 5000,
      navigation: true,
      items: 7,
      navigationText : ["anterior","próximo"]
  });
  $(".slide-clients").removeClass('hide');


  // Seleciona plano ao clicar em "Selecionar"
  $('.plan-select').click(function(){
    $('#user_plan').val($(this).attr('plan'));
    selectPlanButton($(this).attr('plan'));
  });

  // Deixar marcado opão de seleção de plano
  if($('.plan-select').size() > 0){
    selectPlanButton($('#user_plan').val());
  }

  if($('.selected-plan').size() > 0){
    showPlanColumn($('.selected-plan').attr('data-plan'));
  }


  // Função para selecionar botão do plano
  function selectPlanButton(plan){
    if(plan != 'free'){
      var el = $('.plan-select[plan='+plan+']')
      var klass = $(el).parent()[0].classList[0];
      $('.plan-table th, .plan-table td').removeClass('selected');
      $('.plan-table .'+klass).addClass('selected');
    }
  }

  // Esconde todas as colunas dos planos, deixando visivel somente o plano selecionado
  function showPlanColumn(plan){
    if(plan != 'free'){
      var el = $('.plan-select[plan='+plan+']')
      var klass = $(el).parent()[0].classList[0];
      $('.plan-0, .plan-1, .plan-2, .plan-3').addClass('hide');
      $('.plan-table th, .plan-table td').removeClass('selected');
      $('.plan-table .'+klass).addClass('selected');
      $('.tr-btn').addClass('hide');
      $('.plan-table .'+klass).removeClass('hide');
    }
  }


  // Repete os dados do contato geral
  $('.repeat-resp input').change(function(){
    var parent = $(this).parents('.col-md-4');
    if( $(this).is(':checked') == true ){
      $(parent).find('input[id$=name]').val( $('#user_resp_name').val() );
      $(parent).find('input[id$=email]').val( $('#user_resp_email').val() );
      //$(parent).find('input[id$=cellphone]').val( $('#user_resp_cellphone').val() );
    }else{
      $(parent).find('input[id$=name]').val( '' );
      $(parent).find('input[id$=email]').val( '' );
      //$(parent).find('input[id$=cellphone]').val( '' );
    }
  });





  // Busca ao clicar na lupa. Isso precisa ser trocado por um botão
  $('.header-search .form-group').on('click', function(e) {
      var left = $('.header-search .form-group .form-control-feedback').offset().left - 10;
      if (e.pageX >= left){
        $(this).parents('form').submit();
      }
  });


})(window, document, window.jQuery);
