(function (window, document, $, undefined) {
  "use strict";

  $('.more-departments').on('click', function () {
    $('.page-custom').toggleClass('menu-in');
    $('.user-custom.custom-menu').toggle(80);
  });

  // Slide dos banners
  var pshowcase = $("#user-custom-banners");

  pshowcase.owlCarousel({
    autoPlay : 4000,
    stopOnHover: true,
    navigation: false,
    pagination: false,
    paginationSpeed: 1000,
    goToFirstSpeed: 2000,
    singleItem: true,
    autoHeight: true
  });

})(window, document, window.jQuery);
