(function (window, document, $, undefined) {
  "use strict";

  $("#user_cpf").mask("999.999.999-99");
  $("#user_cnpj").mask("99.999.999/9999-99");
  $("#user_resp_cpf").mask("999.999.999-99");
  $('input[id*=_phone]').mask("(99) 9999-9999");
  $('input[id*=_cellphone]').mask("(99) 99999-9999");
  $("#user_address_zipcode").mask("99999-999");


  // Desabilita opções de etapas
  $('.orderStep li.disabled a').css('pointer-events', 'none');
  $('.orderStep li.disabled a').css('cursor', 'default');


  // Validação do formulário
  $('.form-validate').validate({
    errorClass: 'has-error',
    validClass: "has-success",
    errorElement: "small",
    ignore: ":hidden, .date",
    onfocusout: function (element) { $(element).valid(); },
    success: function (label, element) {
      label.closest('.form-group').addClass('has-success');

      setTimeout(function () {
        label.closest('.form-group').removeClass('has-success');
      }, 3000);
    },
    errorPlacement: function (error, element) {
      error.addClass('help-block').appendTo(element.parent());
    },
    highlight: function (element, errorClass, validClass) {
      $(element).closest(".form-group").addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).closest(".form-group").removeClass(errorClass).addClass(validClass);
    },
    submitHandler: function (form) {
      $('input[id$=phone]').blur();
      form.validate();
      form.submit();
    }
  });


  // Seleção dos campos para pessoa física ou jurídica
  $('body').on('click', "input[name='user[kind]']", function () {
    $('.is_user_kind_pj').toggle();
    $('.is_user_kind_pf').toggle();

    if ($(this).val() === 'pf') {
      $('label[for=user_name]').html('<abbr title="Campo obrigatório">&#10033;</abbr> Primeiro nome');
    } else {
      $('label[for=user_name]').html('<abbr title="Campo obrigatório">&#10033;</abbr> Nome fantasia');
    }

  });


  // Impedir submit do campo de sugestão de marca ao pressionar enter
  $("form[id^='edit_manager']").on("keyup keypress", function (e) {
    var code = e.keyCode || e.which;
    if (code === 13) {
      if (e.target.id === 'manager_ad_product_brand_sugest') {
        $('#brand-modal').modal('hide');
        e.preventDefault();
        return false;
      }
    }
  });


  // Preenche campo de marca quando modal é fechado
  $('#brand-modal').on('hidden.bs.modal', function (e) {
    $("#manager_ad_product_brand_id").append(
      $('<option>', { text: '* ' + $('#manager_ad_product_brand_sugest').val() + ' *'})
    );
    $("#manager_ad_product_brand_id option:last").attr("selected", "selected");
  });



})(window, document, window.jQuery);
