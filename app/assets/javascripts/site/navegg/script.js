(function (window, document, $, undefined) {
  "use strict";

  var nvgId = 1;
  var nvgAsync = false;
  (function() {
  var nvg = document.createElement('script');
  nvg.id="navegg";
  nvg.type = 'text/javascript';
  nvg.async = nvgAsync;
  nvg.src=document.location.protocol+'//tag.navdmp.com/tm'+nvgId+'.js';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(nvg, s);
  })();

  // Salva o acesso na tela de produto ou blog
  function saveVisit() {
    if ($('#visit-data').size() > 0 && typeof window.nvgGetSegment !== "undefined") {
      var productData = {
        "ad": $("#visit-data").data('ad'),
        "post": $("#visit-data").data('post'),
        "department": $("#visit-data").data('department'),
        "category": $("#visit-data").data('category'),
        "owner": $("#visit-data").data('owner'),
        "visitor": $("#visit-data").data('visitor'),
        "age": window.nvgGetSegment('age'),
        "cluster": window.nvgGetSegment('cluster'),
        "interest": window.nvgGetSegment('interest'),
        "product": window.nvgGetSegment('product'),
        "brand": window.nvgGetSegment('brand'),
        "career": window.nvgGetSegment('career'),
        "gender": window.nvgGetSegment('gender'),
        "income": window.nvgGetSegment('income')
      };

      $.ajax({
        url: '/site/visits/new',
        data: productData
      });
    }else if ($('#visit-data').size() > 0){
      var productData = {
        "ad": $("#visit-data").data('ad'),
        "post": $("#visit-data").data('post'),
        "department": $("#visit-data").data('department'),
        "category": $("#visit-data").data('category'),
        "owner": $("#visit-data").data('owner'),
        "visitor": $("#visit-data").data('visitor')
      };

      $.ajax({
        url: '/site/visits/new',
        data: productData
      });
    }
  };

  setTimeout(function () {
    saveVisit();
  }, 5000);


})(window, document, window.jQuery);
