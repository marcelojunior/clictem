(function (window, document, $, undefined) {
  "use strict";

  // Habilitar Zoom
  // $("#main-image-zoom").elevateZoom({
  //   gallery: 'main-image-gallery',
  //   zoomType  : "inner",
  //   cursor: "crosshair",
  //   galleryActiveClass: 'active',
  //   imageCrossfade: true,
  //   loadingIcon: 'http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif'
  // });

  // // pass the images to Fancybox
  // $("#main-image-zoom").on("click", function (e) {
  //   e.preventDefault();
  //   var ez = $('#main-image-zoom').data('elevateZoom');
  //   $.fancybox(ez.getGalleryList());
  // });

  $(".fancybox").fancybox({
    openEffect  : 'none',
    closeEffect : 'none',
    helpers : {
      title : {
        type: 'outside'
      },
      thumbs  : {
        width : 100,
        height  : 100
      }
    }
  });


  // Carrega comentários
  if ($('#visit-data').size() > 0) {
    $.ajax({
      url: '/site/comments/new',
      data: { "product_id": $("#visit-data").data('ad'), "seller_id": $('#seller-id').attr('content') }
    });

    $.ajax({
      url: '/site/comments',
      data: {"product_id": $("#visit-data").data('ad'), "seller_id": $('#seller-id').attr('content')}
    });
  }


  $('.scroll-comment').click(function () {
    $('body,html').animate({
      scrollTop: $('.comments-form').offset().top - 50
    }, 800);
  });


  // Imprime cupom
  $('body').on('click', '.print-cupom',function(){
    setTimeout(function(){window.printCupom.print()}, 500);
  })

})(window, document, window.jQuery);
