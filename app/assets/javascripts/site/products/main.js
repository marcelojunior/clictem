//
//= require site/products/_list
//= require site/products/_show

(function (window, document, $, undefined) {
  "use strict";

  var $show_phone = $(".show-phone");

  $show_phone.on("click", function() {
    var $info = $(".user-custom-info"),
      userId = $info.data("seller"),
      productId = $info.data("product");

    showPhone(userId, productId);
  });


  // Pegar o Telefone (json) E Mostrar telefone
  function showPhone(sellerId, productId) {
    var url = "/show-user-phone/" + sellerId;

    if (typeof productId !== undefined ) {
      url += "/" + productId;
    }

    $.get(url).success(function(result) {
      fillPhone(result.phone, result.phone2, result.cellphone);
    });
  }

  // Preencher Telefone
  function fillPhone(phoneNumber, phone2Number, mobileNumber){
    var $phone = $("[data-info-phone]");
    var $phone2 = $("[data-info-phone2]");
    var $mobile = $("[data-info-mobile]");

    // if (phoneNumber.length == 0 && mobileNumber.length > 0)
    // {
    //   setTimeout(function (){ $phone.text(mobileNumber) }, 500);
    // }
    // else 
    if (phoneNumber.length > 0)
    {
      setTimeout(function (){ $phone.text(phoneNumber); $(".show-phone1").removeClass("hide"); }, 500);
    }

    if(phone2Number.length > 0){
      setTimeout(function (){ $phone2.text(phone2Number); $(".show-phone2").removeClass("hide"); }, 500);
    }

    if(mobileNumber.length > 0){
      setTimeout(function (){ $mobile.text(mobileNumber); $(".show-mobile").removeClass("hide"); }, 500);
    }

    // Esconder "mostrar telefone"
    $show_phone.hide();
    $(".show-phone-message, .data-mobile .fa-mobile").removeClass("hide");
  }

})(window, document, window.jQuery);
