(function (window, document, $, undefined) {
  "use strict";

  function load_after_load() {

    if ($(".item.list-view").size() > 0) {
      $(".item").addClass("list-view");
      $(this).data("active", true);
      $(".grid-view").data("active", false);
    }
  }

  $(function (){
    $(".categoryProduct").infinitescroll({
      navSelector: ".categoryFooter",
      nextSelector: ".categoryFooter a[rel=next]",
      itemSelector: ".categoryProduct .item",
      animate: false,
      bufferPx: 1000,
      debug: false,
      loadingImg: null,
      loading: {
        finishedMsg: "<span class='text-center'>Não existem mais anúncios para carregar.</span>",
        msgText: "<span class='text-center'>Estamos procurando mais alguns anúncios para você...</span>"
      }
    }, function () { load_after_load(); });
  });

})(window, document, window.jQuery);
