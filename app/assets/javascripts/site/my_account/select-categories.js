(function (window, document, $, undefined) {
  "use strict";

  // Habilitar categorias
  function showCategories(category) {
    $('.category-tree[department=' + category.department_id + '] a[id=' + category.id + ']').addClass('selected');
    $('.department-tree a#' + category.department_id).addClass('selected');

    $('.category-tree[department=' + category.department_id + ']').show();
    $('.category-tree[category=' + category.id + ']').show();
  }

  if ($('.categories-tree').size() > 0) {
    var $container  = $('.categories-tree'),
      category_id = $container.data('category'),
      category    = null,
      department  = $container.data('department');

    $container.html('<span class="text-muted">Carregando categorias. Aguarde...</span>');

    // Monta o select
    $.getJSON("/api/departments", function (data) {

      // Departamentos
      var departments = [];

      // https://github.com/adobe/brackets/wiki/Brackets-Coding-Conventions
      data.forEach(function (val, key) {
        departments.push("<li><a href='javascript:void(0)' id='" + val.id + "' class='select-department'><i class='" + val.icon + "'></i> " + val.name + " <i class='fa fa-arrow-circle-right'></i></a></li>");
      });

      $container.html('');
      $("<ul/>", {
        "class": "col-md-6 department-tree",
        html: departments.join("")
      }).appendTo(".categories-tree");


      // Categorias
      data.forEach(function (dept, key) {
        var categories = [];

        categories.push("<div class='clearfix'><div class='col-md-12'><h3 class='block-title-2'>Selecione uma categoria <i class='fa fa-arrow-circle-down'></i></h3></div></div>");

        $.each(dept.categories, function (key, val) {
          var checked = '';

          if (category_id === val.id) {
            checked  = 'checked';
            category = val;
          }

          categories.push("<li><label><input type='radio' id='manager_ad_product_category_id_" + val.id + "' name='manager_ad_product[category_id]' value='" + val.id + "' " + checked + "/> <span>" + val.name + "</span></label></li>");
        });

        $("<ul/>", {
          "class": "col-md-6 category-tree",
          department: dept.id,
          html: categories.join("")
        }).appendTo(".categories-tree");

        if (category) {
          showCategories(category);
        }

      });

    });

    // Seleção de departamentos
    $('body').on('click', '.department-tree a', function () {
      $('.category-tree[department]').hide();
      $('.category-tree[category]').hide();
      $('.categories-tree a').removeClass('selected');
      $(this).addClass('selected');
      $('.category-tree[department=' + $(this).attr('id') + ']').show();
    });

    // Seleção de categorias
    $('body').on('click', '.category-tree[department] a', function () {
      $('.category-tree[category]').hide();
      $('.category-tree[department] a').removeClass('selected');
      $(this).addClass('selected');
      $('.category-tree[category=' + $(this).attr('id') + ']').show();
    });

    // Salvar o departamento ao enviar o formulário
    $('form.simple_form').submit(function () {
      $('#manager_ad_product_department_id').val($('.department-tree a.selected').attr('id'));
      // $('#manager_ad_product_category_id').val($('.category-tree[department] a.selected').attr('id'));
    });

    // Alterar marcas
    $('body').on('click', '.select-department', function () {

      $.ajax({
        url: "/site/brands?department=" + $(this).attr('id'),
        beforeSend: function () {
          $('#manager_ad_product_brand_id').html('<option val="">Aguarde...</option>');
        },
        error: function () {
          $('#manager_ad_product_brand_id').html('<option val="">Tente novamente</option>');
        },
        success: function (brands) {
          var items = [];
          items.push('<option val=""></option>')
          $.each(brands, function () {
            items.push('<option value="' + this.id + '">' + this.name + '</option>');
          });
          $('#manager_ad_product_brand_id').html(items.join(''));
        }
      });
      $('#manager_ad_product_brand_id').attr('disabled', null);
    });

  }


})(window, document, window.jQuery);
