(function (window, document, $, undefined) {
  "use strict";

  // Busca de clientes
  $('body').on('change', 'select#group', function(){
    $(this).parents('form').submit();
  });

  // Ao clicar em um grupo
  $('body').on('change', '.new_customer .customer_groups input', function(){
    var ids = [];
    $('.customer_groups input:checked').each(function(){
      ids.push( $(this).val() );
    });
    $.ajax({url:'/minha-conta/clientes?utf8=✓&groups='+ids, dataType: 'script'});
  })

})(window, document, window.jQuery);
