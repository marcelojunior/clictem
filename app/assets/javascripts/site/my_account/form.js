(function (window, document, $, undefined) {
  "use strict";

  // Permitir busca case-insensitive no JQuery
  // NEW selector
  jQuery.expr[':'].Contains = function(a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
  };

  // OVERWRITES old selecor
  jQuery.expr[':'].contains = function(a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
  };


  /*==========  MASCARAS  ==========*/
  $("#user_cpf").mask("999.999.999-99");
  $("#user_cnpj").mask("99.999.999/9999-99");
  $("#user_resp_cpf").mask("999.999.999-99");
  $('input[id*=_phone]').mask("(99) 9999-9999");
  $('input[id*=_cellphone]').mask("(99) 99999-9999");
  $("#user_address_zipcode").mask("99999-999");

  /*==========  VALIDACAO  ==========*/
  // Validação do formulário
  $('.form-validate').validate({
    errorClass: 'has-error',
    validClass: "has-success",
    errorElement: "small",
    ignore: ":hidden, .date",
    onfocusout: function (element) { $(element).valid(); },
    success: function (label, element) {
      label.closest('.form-group').addClass('has-success');

      setTimeout(function () {
        label.closest('.form-group').removeClass('has-success');
      }, 3000);
    },
    errorPlacement: function (error, element) {
      error.addClass('help-block').appendTo(element.parent());
    },
    highlight: function (element, errorClass, validClass) {
      $(element).closest(".form-group").addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).closest(".form-group").removeClass(errorClass).addClass(validClass);
    },
    submitHandler: function (form) {
      $('input[id$=phone]').blur();
      form.validate();
      form.submit();
    }
  });

  /*==========  AÇÕES DO USUARIO  ==========*/
  // Seleção dos campos para pessoa física ou jurídica
  // $('body').on('click', "input[name='user[kind]']", function(){
  $("input[name='user[kind]']").change(function () {

    if ($(this).val() === 'pf') {
      $('label[for=user_name]').html('<abbr title="Campo obrigatório">&#10033;</abbr> Primeiro nome');
      $('.is_user_kind_pj').addClass('hide');
      $('.is_user_kind_pf').removeClass('hide');
    } else {
      $('label[for=user_name]').html('<abbr title="Campo obrigatório">&#10033;</abbr> Nome fantasia');
      $('.is_user_kind_pj').removeClass('hide');
      $('.is_user_kind_pf').addClass('hide');
    };

  });

  // Seleção dos itens de produto
  $("input[name='manager_ad_product[is_service]']").change(function () {
    var isService = $(this).val(),
        $isOld = $("#manager_ad_product_is_old");

    if (isService === "true") {
      $isOld.parent().hide();
    } else {
      $isOld.parent().show();
    }

  });


  // Ao clicar em Consulte o preço
  $("input[name='manager_ad_product[on_request]']").change(function () {
    var showPrice = $(this).is( ":checked" ),
        $fromOnly = $("#manager_ad_product_from_only"),
        $price = $("#manager_ad_product_price");

    if (!showPrice) {
      $fromOnly.parent().show();
      $price.prop('disabled', false);
    } else {
      $fromOnly.parent().hide();
      $price.prop('disabled', true);
    }

  });



  // Ao clicar em exibir "Estou Interessado"
  $("input[name='manager_ad_product[has_interest]']").change(function () {
    if($(this).is(':checked')){
      $('.manager_ad_product_is_old, .manager_ad_product_on_request, .manager_ad_product_from_only').addClass('hide');
      $('.offers-tab').addClass('hide');
      $('.interest-options').removeClass('hide');
      $('#interest-modal').modal();
      $("#manager_ad_product_price").prop('disabled', true);
    }else{
      $('.manager_ad_product_is_old, .manager_ad_product_on_request, .manager_ad_product_from_only').removeClass('hide');
      $('.offers-tab').removeClass('hide');
      $('.interest-options').addClass('hide');
      $("#manager_ad_product_price").prop('disabled', false);
    }
  });


  // Impedir submit do campo de sugestão de marca ao pressionar enter
  $("form[id^='edit_manager']").on("keyup keypress", function (e) {
    var code = e.keyCode || e.which;
    if (code  === 13) {
      if (e.target.id === 'manager_ad_product_brand_sugest') {
        $('#brand-modal').modal('hide');
        e.preventDefault();
        return false;
      }
    }
  });


  // Preenche campo de marca quando modal é fechado
  $('#brand-modal').on('hidden.bs.modal', function (e) {
    $("#manager_ad_product_brand_id").append($('<option>', {
      text: '* ' + $('#manager_ad_product_brand_sugest').val() + ' *'
    }));
    $("#manager_ad_product_brand_id option:last").attr("selected", "selected");
  });



  // Abrir campos para oferta
  $('.offer-btn').on('click', 'a', function () {
    setTimeout(function(){
      $('.input-group.datepicker').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: 'pt-br'
      });

      $('input[price-format]').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
      });


      $('.offers select').chosen({no_results_text: "Anúncio não encontrado!"});

      $('.offers select').on('change', function(event, params) {
        var $price_element = $(this).parents('.nested-fields').find('span.price');
        $.ajax({
          url: '/api/products/' + params.selected,
          success: function(data){
            $price_element.html(convertToReal(data.price))
          }
        });
      });

      cupomEnable();

    }, 300);
  });


  // Abrir dias da semana em ofertas
  $('.offers').on('click', '.manager_ad_product_offers_repeat input, .manager_ad_offer_campaign_repeat input', function(){
    if ( $(this).val() == 'true' ){
      $(this).parents('.check_boxes_inline').find('ul').removeClass('hide');
    }else{
      $(this).parents('.check_boxes_inline').find('ul').addClass('hide');
    };
  });



  // Exibir pergunta de limite de cupom
  $('.offers').on('click', '.manager_ad_product_offers_has_cupom input', function(){
    if ( $(this).val() == 'true' ){
      $(this).parents('.nested-fields').find('.has_cupom_limit').removeClass('hide');
    }else{
      $(this).parents('.nested-fields').find('.has_cupom_limit').addClass('hide');
    };
  });

  // Exibir número de limite de cupons
  $('.offers').on('click', '.manager_ad_product_offers_has_cupom_limit input', function(){
    if ( $(this).val() == 'true' ){
      $(this).parents('.nested-fields').find('.cupom_limit').removeClass('hide');
    }else{
      $(this).parents('.nested-fields').find('.cupom_limit').addClass('hide');
    };
  });

  // Exibir pergunta de limite de cupom na tela de campanhas.
  $('form').on('click', '.manager_ad_offer_campaign_has_cupom input', function(){
    if ( $(this).val() == 'true' ){
      $('.has_cupom_limit').removeClass('hide');
    }else{
      $('.has_cupom_limit').addClass('hide');
    };
  });

  // Exibir número de limite de cupons na tela de campanhas.
  $('.offers').on('click', '.manager_ad_offer_campaign_offers_has_cupom_limit input', function(){
    if ( $(this).val() == 'true' ){
      $(this).parents('.nested-fields').find('.cupom_limit').removeClass('hide');
    }else{
      $(this).parents('.nested-fields').find('.cupom_limit').addClass('hide');
    };
  });

  // Apagar linha o clicar em excluir
  $('.product-remove').click(function () {
    if (window.confirm('Deseja mesmo excluir este anúncio?') === false) {
      return false;
    } else {
      $(this).parents('tr').fadeOut('slow');
    }
  });



  // Abre modal para ver cupom
  $('.view-cupom').click(function(){
    $('#modalCupom').modal();
  });




  // Marcar anúncio como publicado
  $('.publish-list input').click(function(){
    var form = $(this).parents('form');
    form.submit();
  })




  // Converte float para Reais
  function convertToReal(value){
    var value = value.toFixed(2).replace(/./g, function(c, i, a) {
        return i && c !== "." && ((a.length - i) % 3 === 0) ? '.' + c : c;
    });

    return 'R$ ' + value.replace(/\.([^\.]*)$/,",$1")
  };




  // Libera o limite de cupons na tela de campanhas
  $('.offers').on('click', '.manager_ad_offer_campaign_offers_has_cupom input', function(){
    var parent  = $(this).parents('.nested-fields');
    var balance = parseInt($('meta[name=cupomLimitBalance]').attr('content'));

    if( $(this).val() == 'true' ){
      $('meta[name=cupomLimitBalance]').attr('content', balance - 1);
      $(parent).find('.cupom-configs').removeClass('hide');
    }else{
      $('meta[name=cupomLimitBalance]').attr('content', balance + 1);
      $(parent).find('.cupom-configs').addClass('hide');
    };

    cupomEnable();
  });



  // Liberar ou não opção de cupons
  function cupomEnable(){
    var balance = parseInt($('meta[name=cupomLimitBalance]').attr('content'));
    var cupoms  = $('.manager_ad_offer_campaign_offers_has_cupom input[value=true]:checked').size();

    if(balance == 0){
      $('.manager_ad_offer_campaign_offers_has_cupom input[value=false]:checked').each(function(){
        var cupom_parent = $(this).parents('.nested-fields');
        $(cupom_parent).find('.has-cupom').addClass('hide');
        $(cupom_parent).find('.has-no-cupom').removeClass('hide');
      });
    }else{
      $('.has-cupom').removeClass('hide');
      $('.has-no-cupom').addClass('hide');
    };
  }



  // Datepicker
  $(".input-group.datepicker input").mask("99/99/9999");
  $('.input-group.datepicker').datetimepicker({
    format: 'DD/MM/YYYY',
    locale: 'pt-br'
  });


  // Redimensiona imagem Base64
  function resizedataURL(datas) {
    var img = document.createElement('img');
    img.src = datas;

    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');

    var original_width = img.width;
    var original_height = img.height;

    var new_width = (original_width * 500) / original_height;

    canvas.width = new_width;
    canvas.height = 500;

    ctx.drawImage(img, 0, 0, new_width, 500);

    var dataURI = canvas.toDataURL('image/jpeg', 0.5);

    return dataURI;
  }


  // Upload de imagens ------
  function addImage(base64){
    var base64 = resizedataURL(base64);

    $('.submit-buttons button').attr('disabled', true);
    $('.btn-add-img').trigger('click');
    $('.img-drag').hide();
    var container = $('.product-image:last');
    var token     = Math.random().toString(36).substr(2) + $('#current-user-id').attr('content');
    $(container).find('input[id$=token]').val(token);
    $(container).find('img').attr('src', base64);
    $(container).find('.wait').css('width', $(container).width());
    $(container).find('.wait').removeClass('hide');

    $.post('/minha-conta/produto/save_session_images', { token: token, base64: base64 })
      .done(function(data){
        $(container).find('.wait').addClass('hide');
        if($('.wait:visible').size() > 0){
          $('.submit-buttons button').attr('disabled', true);
        }else{
          $('.submit-buttons button').attr('disabled', false);
        }
    });
  };

  // Iimagens com drag on drop
  $('html').fileDragAndDrop(function(fileCollection){
    $.each(fileCollection, function(){
      addImage(this.data);
    });
  });

  // Imagens carregando via input
  function readImage(file) {

      var reader = new FileReader();
      var image  = new Image();

      reader.readAsDataURL(file);
      reader.onload = function(_file) {
          image.src    = _file.target.result;              // url.createObjectURL(file);
          image.onload = function() {
              addImage(image.src);
          };
          image.onerror= function() {
              alert('Tipo de arquivo inválido: '+ file.type +'. Por favor, adicione somente imagens.');
          };
      };
  }
  $(".img-uploader input").change(function (e) {
      if(this.disabled) return alert('File upload not supported!');
      var F = this.files;
      if(F && F[0]) for(var i=0; i<F.length; i++) readImage( F[i] );
  });


  // Busca de produtos
  $('.search-product').keyup(function(){
    var text = $.trim($(this).val());
    if(text == ''){
      $('.product-item').parent().show();
    }else{
      $('.product-item').parent().hide();
      $('.product-item:contains('+ text +')').parent().show();
    }
  });


  // Repete os dados do contato geral
  $('.repeat-resp input').change(function(){
    var parent = $(this).parents('.col-md-4');
    if( $(this).is(':checked') == true ){
      $(parent).find('input[id$=name]').val( $('#user_resp_name').val() );
      $(parent).find('input[id$=email]').val( $('#user_resp_email').val() );
      //$(parent).find('input[id$=cellphone]').val( $('#user_resp_cellphone').val() );
    }else{
      $(parent).find('input[id$=name]').val( '' );
      $(parent).find('input[id$=email]').val( '' );
      //$(parent).find('input[id$=cellphone]').val( '' );
    }
  });



})(window, document, window.jQuery);
