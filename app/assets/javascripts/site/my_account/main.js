//= require site/my_account/form
//= require site/my_account/select-categories
//= require site/my_account/insert-images
//= require site/my_account/customers
//= require_tree ./sms

(function (window, document, $, undefined) {
  "use strict";

  // Campos de valor
  $('input[price-format]').priceFormat({
    prefix: '',
    centsSeparator: ',',
    thousandsSeparator: '.'
  });

  // Desabilita opções de etapas
  $('.orderStep li.disabled a').css('pointer-events', 'none');
  $('.orderStep li.disabled a').css('cursor', 'default');

  // CHOSEN
  $('.chosen-select').chosen({no_results_text: "Cadastro não encontrado!", width: "95%"});

})(window, document, window.jQuery);
