(function (window, document, $, undefined) {
  "use strict";

  if ($('#sms_buy_qtd').size() > 0) {
    var val1 = 0.20, // Até 499
      val2 = 0.18, // De 500 a 999
      val3 = 0.16, // De 1000 a 1999
      val4 = 0.14, // Acima de 1000
      val_total = 30;

    // Verificar limite minimo de compra de SMS
    $('#sms_buy_qtd').keyup(function () {
      var qtd_total = parseInt($('#sms_buy_qtd').val(), 10);

      if (qtd_total < 150) {
        $('.sms-buys-button').attr('disabled', 'disabled');
        $('.sms-buy-alert').removeClass('hide');

      } else {
        $('.sms-buys-button').attr('disabled', null);
        $('.sms-buy-alert').addClass('hide');
      }

      $('#sms-buy-confirm .total').html($('#sms_buy_qtd').val() + ' créditos');

      // Calcula o total a pagar
      if (qtd_total <= 499) {
        val_total = qtd_total * val1;
      }

      if (qtd_total >= 500 && qtd_total <= 999) {
        val_total = qtd_total * val2;
      }

      if (qtd_total >= 1000 && qtd_total <= 1999) {
        val_total = qtd_total * val3;
      }

      if (qtd_total >= 2000) {
        val_total = qtd_total * val4;
      }

      $('.sms-buy-total, #sms-buy-confirm .value').html('R$ ' + val_total.toFixed(2).replace('.', ','));

      $('#sms_buy_value').val(val_total.toFixed(2));
    });
  }



  // Ao pressionar enter, abrir tela de confirmação
  $('#new_sms_buy').find('input').keypress(function (e) {

    // Enter key = keycode 13
    if (e.which === 13) {
      $('#sms-buy-confirm').modal();
      return false;
    }
  });

})(window, document, window.jQuery);
