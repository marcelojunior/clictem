
(function (window, document, $, undefined) {
  "use strict";

  // Exibe quantidade de contatos ao selecionar o grupo
  $('.sms_campaign_groups :checkbox').change(function () {
    countContacts();
  });

  // Exibe a quantidade de contatos ao adicionar individualmente
  $('#sms_campaign_customer_ids').on('change', function(){
    countContacts();
  });

  countContacts();


  // Seleção de contatos indivualmente
  $('#sms_campaign_customer_ids').select2({
    allowClear: true
  })


  // Contagem de caracteres restantes
  $("#sms_campaign_message").keyup(function () {
    var limit = parseInt($(this).attr('maxlength'), 10),
      text  = $(this).val(),
      chars = text.length,
      count = limit - chars,
      new_text;

    $('#message-count').html('<strong>' + count + '</strong> caracteres restantes.');

    if (chars > limit) {
      new_text = text.substr(0, limit);

      $(this).val(new_text);
    }
  });




  // Contagem de contatos selecionados
  function countContacts() {
    var ids = [],
      total = $('meta[name="sms:credit"]').attr('content');

    $.each($('.sms_campaign_groups :checkbox:checked'), function () {
      ids.push($(this).attr('value'));
    });

    if (ids.length > 0) {
      $.ajax({
        url: '/minha-conta/customers-group-count?group_ids=' + ids.join('-'),
        success: function (data) {
          // Ids dos clientes selecionados indivudualemente
          var custom = $.map($('#sms_campaign_customer_ids option:selected'), function(el){
                            return parseInt($(el).val());
                        });
          var count  = $.unique($.merge(data, custom)).length;

          if (count === 0) {
            $('.campaigns-count-groups').html('<span class="text-danger">Nenhum contato selecionado</span>');
            $('.campaign-button-save').attr('disabled', 'disabled');
          } else if (count === 1) {
            $('.campaigns-count-groups').html('<span class="text-success"><strong>' + count + '</strong> contato selecionado</span>');
            $('.campaign-button-save').attr('disabled', null);
            $('.campaigns-count-alert').html(count + ' contato');
          } else if (count > 1) {
            $('.campaigns-count-groups').html('<span class="text-success"><strong>' + count + '</strong> contatos selecionados</span>');
            $('.campaign-button-save').attr('disabled', null);
            $('.campaigns-count-alert').html(count + ' contatos');
          }

          if (count > total) {
            $('.campaigns-alert').removeClass('hide');
          } else {
            $('.campaigns-alert').addClass('hide');
          }
        }
      });

    } else {

      $('.campaigns-count-groups').html('<span class="text-danger">Nenhum contato selecionado</span>');
      $('.campaign-button-save').attr('disabled', 'disabled');
      // $('#credit-msg').hide('slow');
    }
  }


})(window, document, window.jQuery);



