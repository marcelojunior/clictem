(function (window, document, $, undefined) {
  "use strict";


  // Contagem de caracteres restantes
  $("#user_sms_birthday_message").keyup(function () {
    var limit = parseInt($(this).attr('maxlength'), 10),
      text  = $(this).val(),
      chars = text.length,
      count = limit - chars,
      new_text;

    $('#message-count').html('<strong>' + count + '</strong> caracteres restantes.');

    if (chars > limit) {
      new_text = text.substr(0, limit);
      $(this).val(new_text);
    }
  });

})(window, document, window.jQuery);
