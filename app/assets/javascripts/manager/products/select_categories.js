/**
*
* Todos os comportamentos da seleção de categorias estão neste arquivo.
*
**/


if($('.categories-tree').size() > 0){
    $container  = $('.categories-tree');
    category_id = $container.data('category');
    category    = null
    department  = $container.data('department');

    $container.html('<span class="text-muted">Carregando categorias. Aguarde...</span>')

    // Monta o select
    $.getJSON( "/manager/ad/departments.json", function( data ) {

        // Departamentos
        departments = [];
        $.each( data, function( key, val ) {
            departments.push( "<li><a href='javascript:void(0)' id='" + val.id + "'>" + val.name + "</a></li>" );
        } )

        $container.html('');
        $( "<ul/>", {
            "class": "col-md-6 department-tree",
            html: departments.join( "" )
        }).appendTo( ".categories-tree" );


        // Categorias
        $.each( data, function( key, dept ) {
            categories = [];

            $.each( dept.categories, function( key, val ) {
                checked = ''
                if(category_id == val.id){
                    checked  = 'checked'
                    category = val
                }
                categories.push( "<li><label><input type='radio' id='manager_ad_product_category_id_"+val.id+"' name='manager_ad_product[category_id]' value='"+val.id+"' "+checked+"/> <span>"+val.name+"</span></label></li>" );
                // categories.push( "<li><a href='javascript:void(0)' id='" + val.id + "'>" + val.name + "</a></li>" );
            } )

            $( "<ul/>", {
                "class": "col-md-6 category-tree",
                department: dept.id,
                html: categories.join( "" )
            }).appendTo( ".categories-tree" );

            if(category){
              showCategories(category)
            }

        } )


        // Subcategorias
        // $.each( data, function( key, dept ) {

        //     $.each( dept.categories, function( key, cats ) {
        //         sub_categories = [];
        //         $.each( cats.sub_categories, function( key, val ) {
        //             checked = ''
        //             if(sub_category_id == val.id){
        //                 checked = 'checked'
        //                 sub_category = val
        //             }
        //             sub_categories.push( "<li><label><input type='radio' id='manager_ad_product_sub_category_id_"+val.id+"' name='manager_ad_product[sub_category_id]' value='"+val.id+"' "+checked+"/> "+val.name+"</label></li>" );

        //         } )

        //         $( "<ul/>", {
        //             "class": "col-md-4 category-tree",
        //             category: cats.id,
        //             html: sub_categories.join( "" )
        //         }).appendTo( ".categories-tree" );

        //         if(sub_category){
        //             showCategories(sub_category)
        //         }

        //     } )

        // } )
    })


    // Habilitar categorias
    function showCategories(category){

        $('.category-tree[department='+category.department_id+'] a[id='+category.id+']').addClass('selected');
        $('.department-tree a#'+category.department_id).addClass('selected');

        $('.category-tree[department='+category.department_id+']').show();
        $('.category-tree[category='+category.id+']').show();
    }


    // Seleção de departamentos
    $(document).on('click', '.department-tree a', function() {
        $('.category-tree[department]').hide();
        $('.category-tree[category]').hide();
        $('.categories-tree a').removeClass('selected');
        $(this).addClass('selected');
        $('.category-tree[department='+$(this).attr('id')+']').show();
        selectBrands($(this).attr('id'))
    })

    // Seleção de categorias
    $(document).on('click', '.category-tree[department] a', function() {
        $('.category-tree[category]').hide();
        $('.category-tree[department] a').removeClass('selected');
        $(this).addClass('selected');
        $('.category-tree[category='+$(this).attr('id')+']').show();
    })



    // Salvar o departamento ao enviar o formulário
    $('form.simple_form').submit(function(){
        $('#manager_ad_product_department_id').val($('.department-tree a.selected').attr('id'));
        // $('#manager_ad_product_category_id').val($('.category-tree[department] a.selected').attr('id'));
    })

}