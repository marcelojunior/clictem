/**
*
* Todos os comportamentos da seleção de marcas
*
**/

function selectBrands(department_id){
  department_id = typeof department_id !== 'undefined' ? department_id : null;
  brands        = null;
  $select       = $('div.brand_id');
  brand_id      = $select.data('brand');
  if(department_id == null){
    department_id = $select.data('department');
  }

  if(department_id == ''){
    $select.html('<span class="text-muted">Por favor, selecione uma categoria.</span>')
  }else{
    $.getJSON( "/manager/ad/departments/"+department_id+".json", function( department ) {
      brands_options = [];
      brands_options.push( "<option value=''></option>" );
      $.each( department.brands, function( key, val ) {
          selected = '';
          if(brand_id == val.id){ selected = 'selected' }
          brands_options.push( "<option value='"+val.id+"' "+selected+">"+val.name+"</option>" );
      } );

      $select.html('');
      $( "<select/>", {
          "class": "form-control",
          id: 'manager_ad_product_brand_id',
          name: 'manager_ad_product[brand_id]',
          html: brands_options.join( "" )
      }).appendTo( "div.brand_id" );
    } );
  }

}


if($('div.brand_id').size() > 0){
  selectBrands();
}

