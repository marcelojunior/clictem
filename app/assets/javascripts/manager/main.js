// # Vendor
//= require jquery/dist/jquery.min
//= require angular/angular.min
// require angular-route/angular-route.min
//= require angular-resource/angular-resource
//= require angular-animate/angular-animate.min
//= require underscore/underscore
//= require Jquery-Price-Format/jquery.price_format.min
//= require jquery-maskedinputs/dist/jquery.maskedinput.min
//
// require redactor-rails
// require redactor-rails/langs/pt_br
//
// # Rails
//= require jquery_ujs
//= require angular-rails-templates
//
// # Templates
//= require_tree ../templates
//
// # ui
//= require angular-bootstrap/ui-bootstrap-tpls.min
// require jquery-spinner/dist/jquery.spinner.min
// require seiyria-bootstrap-slider/dist/bootstrap-slider.min
// require jquery-steps/build/jquery.steps.min
// require toastr/toastr.min
// require bootstrap-file-input/bootstrap.file-input
// require jquery.slimscroll/jquery.slimscroll.min
// require holderjs/holder
// require raphael/raphael-min
// require morris.js/morris
// require ./theme/vendors/responsive-tables
// require ./theme/vendors/jquery.sparkline.min
// require flot/jquery.flot
// require flot/jquery.flot.resize
// require flot/jquery.flot.pie
// require flot/jquery.flot.stack
// require flot.tooltip/js/jquery.flot.tooltip.min
// require flot/jquery.flot.time
// require gauge.js/dist/gauge.min
// require jquery.easy-pie-chart/dist/angular.easypiechart.min
// require angular-wizard/dist/angular-wizard.min
// require textAngular/dist/textAngular-sanitize.min
// require textAngular/dist/textAngular.min
// require ./theme/vendors/skycons

// # App
//= require ./theme/app
//= require ./theme/shared/main
//= require ./theme/shared/directives
// require ./theme/UI/UICtrl
// require ./theme/UI/UIDirective
// require ./theme/UI/UIService
// require ./theme/Form/FormDirective
// require ./theme/Form/FormCtrl
// require ./theme/Form/FormValidation
// require ./theme/Table/TableCtrl
// require ./theme/Task/Task
// require ./theme/Chart/ChartCtrl
// require ./theme/Chart/ChartDirective

// # CLICTEM
//= require ./pages/pages.controllers

// Mestre detalhe para o Simple Form
//= require cocoon

// Produtos
//= require ./products/select_categories
//= require ./products/select_brands

// Usuários
//= require ./users/dados


// Plugins
// require ../plugins/jquery.maskedinput
// require ../plugins/jquery.price
// require ../plugins/geral
