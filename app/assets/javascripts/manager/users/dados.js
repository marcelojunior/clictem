(function (window, document, $, undefined) {
    "use strict";

    $(document).ready(function(){
        $("#user_cpf").mask("999.999.999-99");
        $("#user_cnpj").mask("99.999.999/9999-99");
        $("#user_resp_cpf").mask("999.999.999-99");
        $('input[id*=_phone]').mask("(99) 9999-9999");
        $('input[id*=_cellphone]').mask("(99) 99999-9999");
        $("#user_address_zipcode").mask("99999-999");

        // Seleção dos campos para pessoa física ou jurídica
        $("form").on('change', "input[name='user[kind]']", function(){
          if($(this).val() === 'pf'){

            // $('.user_last_name').parent().removeClass('hide');
            $('.user_name label').html('<abbr title="Campo obrigatório">✱</abbr> Nome');
            $('.user_last_name').parent().removeClass('hide');
            $('.user_cpf').parent().removeClass('hide');
            $('.user_birthday').parent().removeClass('hide');
            $('.user_genre').parent().removeClass('hide');

            $('.user_business_name').parent().addClass('hide');
            $('.user_cnpj').parent().addClass('hide');
            // $('.user_ie').addClass('hide');
            $('.resp-dados').addClass('hide');
            $('.user_website').parent().addClass('hide');

          }else{

            // $('.user_last_name').parent().addClass('hide');
            $('.user_name label').html('<abbr title="Campo obrigatório">✱</abbr> Nome Fantasia');
            $('.user_last_name').parent().addClass('hide');
            $('.user_cpf').parent().addClass('hide');
            $('.user_birthday').parent().addClass('hide');
            $('.user_genre').parent().addClass('hide');

            $('.user_business_name').parent().removeClass('hide');
            $('.user_cnpj').parent().removeClass('hide');
            // $('.user_ie').removeClass('hide');
            $('.resp-dados').removeClass('hide');
            $('.user_website').parent().removeClass('hide');

          }
        });

        // Desabilita opções de etapas
        $('.orderStep li.disabled a').css('pointer-events', 'none');
        $('.orderStep li.disabled a').css('cursor', 'default');


        // Decimal
        setTimeout(function(){
            $('.decimal').priceFormat({
                prefix: '',
                centsSeparator: ',',
                thousandsSeparator: '.'
            });
        }, 500);


        // Popula os dados de compartilhamento no Facebook, na aba de destatísticas.
        setTimeout(function(){
            $('.ad-metrics').each(function(){
                var el  = $(this);
                var url = $(el).data('url');
                var share = 0;
                var like  = 0;
                var link = 'http://api.facebook.com/restserver.php?method=links.getStats&urls='+url+'&format=json'
                $.getJSON(link).done(function(data){
                    console.log(data[0]);
                    share = data[0].share_count;
                    like  = data[0].like_count;
                    $(el).find('.share-count').html(share);
                    $(el).find('.like-count').html(like);
                })
            });
        }, 500);


        // Repete os dados do contato geral
        $('form').on('change', '.repeat-resp input',function(){
          console.log('teste')
          var parent = $(this).parents('.col-md-4');
          if( $(this).is(':checked') == true ){
            $(parent).find('input[id$=name]').val( $('#user_resp_name').val() );
            $(parent).find('input[id$=email]').val( $('#user_resp_email').val() );
            //$(parent).find('input[id$=cellphone]').val( $('#user_resp_cellphone').val() );
          }else{
            $(parent).find('input[id$=name]').val( '' );
            $(parent).find('input[id$=email]').val( '' );
            //$(parent).find('input[id$=cellphone]').val( '' );
          }
        });
    });

})(window, document, window.jQuery);
