'use strict';

angular.module('app', [
  # Templates
  'templates'

  # Angular modules
  # 'ngRoute' #-- Desativado
  'ngAnimate'
  'ngResource'

  # 3rd Party Modules
  'ui.bootstrap'
  # 'easypiechart'
  # 'mgo-angular-wizard'
  # 'textAngular'

  # Custom modules
  # 'app.ui.ctrls'
  # 'app.ui.directives'
  # 'app.ui.services'
  'app.controllers'
  'app.directives'
  # 'app.form.validation'
  # 'app.ui.form.ctrls'
  # 'app.ui.form.directives'
  # 'app.tables'
  # 'app.task'
  # 'app.localization'
  # 'app.chart.ctrls'
  # 'app.chart.directives'
  'app.pages.controllers'
])

.config([ '$httpProvider', ($httpProvider) ->
  authToken = $("meta[name=\"csrf-token\"]").attr("content")
  $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = authToken
])


# .config([
  # '$routeProvider'
  # '$locationProvider'
  # ($routeProvider, $locationProvider) ->
    # $locationProvider.html5Mode(true) if(window.history && window.history.pushState)
#         $locationProvider.html5Mode(false).hashPrefix('!');

    # $routeProvider
#             .when(
#                 '/'
#                 redirectTo: '/dashboard'
#             )

      # Pages
      # .when(
      #   '/manager/pages'
      #   templateUrl: 'pages/index.html'
      # )

#             .when(
#                 '/dashboard'
#                 templateUrl: 'dashboard.html'
#             )

#             # CRUDS
#             .when(
#                   '/products'
#                   templateUrl: 'products/index.html'
#             )
#             .when(
#                   '/products/new'
#                   templateUrl: 'products/form.html'
#             )
#             .when(
#                   '/banners'
#                   templateUrl: 'banners/index.html'
#             )
#             .when(
#                   '/banners/new'
#                   templateUrl: 'banners/form.html'
#             )
#             .when(
#                   '/brands'
#                   templateUrl: 'brands/index.html'
#             )
#             .when(
#                   '/brands/new'
#                   templateUrl: 'brands/form.html'
#             )
#             .when(
#                   '/departaments'
#                   templateUrl: 'departaments/index.html'
#             )
#             .when(
#                   '/departaments/new'
#                   templateUrl: 'departaments/form.html'
#             )
#             .when(
#                   '/pages'
#                   templateUrl: 'pages/index.html'
#             )
#             .when(
#                   '/pages/new'
#                   templateUrl: 'pages/form.html'
#             )
#             .when(
#                   '/users'
#                   templateUrl: 'users/index.html'
#             )
#             .when(
#                   '/users/new'
#                   templateUrl: 'users/form.html'
#             )

#             # Erros
#             .when(
#                 '/404'
#                 templateUrl: '404.html'
#             )
#             .otherwise(
#                 redirectTo: '/404'
#             )
# ])


# .when(
#     '/pages/500'
#     templateUrl: 'pages/500.html'
# )
# .when(
#     '/pages/blank'
#     templateUrl: 'pages/blank.html'
# )
# .when(
#     '/pages/invoice'
#     templateUrl: 'pages/invoice.html'
# )

# Tasks
# .when(
#     '/tasks'
#     templateUrl: 'tasks/tasks.html'
# )


# UI Kit
# .when(
#     '/ui/typography'
#     templateUrl: 'ui/typography.html'
# )
# .when(
#     '/ui/buttons'
#     templateUrl: 'ui/buttons.html'
# )
# .when(
#     '/ui/icons'
#     templateUrl: 'ui/icons.html'
# )
# .when(
#     '/ui/grids'
#     templateUrl: 'ui/grids.html'
# )
# .when(
#     '/ui/widgets'
#     templateUrl: 'ui/widgets.html'
# )
# .when(
#     '/ui/components'
#     templateUrl: 'ui/components.html'
# )
# .when(
#     '/ui/timeline'
#     templateUrl: 'ui/timeline.html'
# )

# Forms
# .when(
#     '/forms/elements'
#     templateUrl: 'forms/elements.html'
# )
# .when(
#     '/forms/layouts'
#     templateUrl: 'forms/layouts.html'
# )
# .when(
#     '/forms/validation'
#     templateUrl: 'forms/validation.html'
# )
# .when(
#     '/forms/wizard'
#     templateUrl: 'forms/wizard.html'
# )

# Tables
# .when(
#     '/tables/static'
#     templateUrl: 'tables/static.html'
# )
# .when(
#     '/tables/responsive'
#     templateUrl: 'tables/responsive.html'
# )
# .when(
#     '/tables/dynamic'
#     templateUrl: 'tables/dynamic.html'
# )

# Charts
# .when(
#     '/charts/others'
#     templateUrl: 'charts/charts.html'
# )
# .when(
#     '/charts/morris'
#     templateUrl: 'charts/morris.html'
# )
# .when(
#     '/charts/flot'
#     templateUrl: 'charts/flot.html'
# )

# Mail
#  .when(
#     '/mail/inbox'
#     templateUrl: 'mail/inbox.html'
# )
#  .when(
#     '/mail/compose'
#     templateUrl: 'mail/compose.html'
# )
#  .when(
#     '/mail/single'
#     templateUrl: 'mail/single.html'
# )

# Pages
# .when(
#     '/pages/features'
#     templateUrl: 'pages/features.html'
# )
# .when(
#     '/pages/signin'
#     templateUrl: 'pages/signin.html'
# )
# .when(
#     '/pages/signup'
#     templateUrl: 'pages/signup.html'
# )
# .when(
#     '/pages/lock-screen'
#     templateUrl: 'pages/lock-screen.html'
# )
# .when(
#     '/pages/profile'
#     templateUrl: 'pages/profile.html'
# )


