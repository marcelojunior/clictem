'use strict'

angular.module('app.pages.controllers', [])

.factory('Page', [
    '$resource'
    ($resource) ->
        return $resource('/api/pages.json')
])

.controller('PagesCtrl',[
    '$scope', '$http', '$location'
    ($scope, $http, $location) ->
        $scope.template_url = 'pages/index.html'

        $scope.newItem = ->
            # $scope.template_url = 'pages/new.html'
            console.log('novo item')
            window.location.url = '/manager/pages/new'

        # $scope.update = (id) ->
        #     $scope.template_url = 'pages/form.html'
        #     $scope.page_id = id
])

.controller('PagesIndexCtrl',[
    '$scope', '$http', 'Page'
    ($scope, $http, Page) ->

        # Usando $resource com factory
        $scope.pages = Page.query()

        # usando $http.get
        # $scope.pages = []
        # $http.get('/api/pages.json').success( (data) ->
        #     $scope.pages = data
        # )
])

.controller('PagesFormCtrl',[
    '$scope', '$http'
    ($scope, $http) ->
        # $scope.pages = []

        # $http.get('/api/pages.json').success( (data) ->
        #     $scope.pages = data
        # )
])