//
//= require s3_direct_upload
//= require cocoon

/*==========  Forms  ==========*/
//= require jquery-maskedinputs/dist/jquery.maskedinput.min
//= require Jquery-Price-Format/jquery.price_format.min
// require redactor-rails
// require redactor-rails/langs/pt_br
//= require jquery-validation/dist/jquery.validate
//= require jquery-validation/src/localization/messages_pt_BR
//= require select2/select2.min
//= require select2/select2_locale_pt-BR

/*==========  Datepicker  ==========*/
//= require moment
//= require bootstrap-datetimepicker
//= require pickers

// You may include any languages (optional)
//= require moment/pt-br


/*==========  Plugins  ==========*/
//= require ./plugins/jQuery.FileDrop.min

/*==========  CHOSEN  ==========*/
//= require chosen_v1.3.0/chosen.jquery.min.js

/*==========  MINHA CONTA  ==========*/
//= require site/my_account/main

// $.ajaxSetup({
//   headers: {
//     'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
//   }
// });

//
//(function (window, document, $, undefined) {
//  "use strict";
//
//})(window, document, window.jQuery);
