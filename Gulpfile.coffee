
pkg = require "./package.json"
yargs = require "yargs"
exec = require "exec"
fs = require "fs"
gulp = require "gulp"
bump = require "gulp-bump"
gsync = require "gulp-sync"
sync = gsync(gulp).sync
version = yargs.argv.type or "patch"


# Verifica a versão atual
getPackageJson = () ->
    return JSON.parse(fs.readFileSync('./package.json', 'utf8'));

# Falha se tiver commit pendentes
gulp.task "fail-if-dirty", (cb) ->
    exec 'git diff-index HEAD --', (err, output, code) ->
        return cb(err) if err
        return cb("Possui COMMITs pendentes!") if output
        cb()

# Falha se não estiver no master
gulp.task "fail-if-not-master", (cb) ->
    exec 'git symbolic-ref -q HEAD' , (err, output, code) ->
        return cb(err) if err
        return cb("Não está no MASTER") if !/refs\/heads\/master/.test output
        cb()

# Cria a tag com a versão atual
gulp.task "git-tag", (cb) ->
    message = "v" + getPackageJson().version
    exec 'git tag ' + message, cb

# Add alterações
gulp.task "git-add", (cb) ->
    exec 'git add package.json bower.json public/assets/manifest-*.json', cb

# Commit da versão atual
gulp.task "git-commit", [ "git-add"], (cb) ->
    message = "Deploy v" + getPackageJson().version
    exec 'git commit -m "' + message + '"', cb

# Pull remote changes
gulp.task "git-pull", (cb) ->
    exec 'git pull origin master', (err, output, code) ->
        return cb(err + output) if code isnt 0
        cb()

# Push commits e tags para o origin
gulp.task "git-push", [ "git-tag" ], (cb) ->
    exec [
        'git push origin master',
        'git push --tags'
    ].join(" && "), (err, output, code) ->
        return cb(err + output) if code isnt 0
        cb()

# Push commits para o heroku
gulp.task "git-push-heroku", [ "git-commit" ], (cb) ->
    exec 'git push heroku master', (err, output, code) ->
        return cb(err + output) if code isnt 0
        cb()

# Push commits para o heroku
gulp.task "git-push-beta", [ "git-commit" ], (cb) ->
    exec 'git push beta master', (err, output, code) ->
        return cb(err + output) if code isnt 0
        cb()

# Altera a versão atual (para deploy): 0.0.1
gulp.task "version", () ->
    gulp.src([ "package.json", "bower.json" ])
    .pipe(bump({ type: version }))
    .pipe(gulp.dest("."))

# Altera a versão atual (para Sprint da semana): 0.1.0
gulp.task "version:week", (cb) ->
    gulp.src([ "package.json", "bower.json" ])
    .pipe(bump({ type: 'minor' }))
    .pipe(gulp.dest("."))

# Altera a versão atual (para Release do mês): 1.0.0
gulp.task "version:month", (cb) ->
    gulp.src([ "package.json", "bower.json" ])
    .pipe(bump({ type: 'major' }))
    .pipe(gulp.dest("."))

gulp.task "version:beta", (cb) ->
    exec 'gulp version --type=prerelease', cb

# Gerando o Precompile
gulp.task "precompile", (cb) ->
    exec 'rake assets:precompile RAILS_ENV=production --trace', cb

# Limpando os Assets
gulp.task "assets-clean", (cb) ->
    exec [
        "rm -r public/assets",
        "git checkout -- public/assets/manifest-\*"
    ].join(" && "), (err, output, code) ->
        return cb(err + output) if code isnt 0
        cb()

# Publica para o ELB
gulp.task "tag", sync([
    "version"
    "git-commit"
    "git-tag"
], "Adiciona Tag pre-deploy")


# Publica para o BETA
gulp.task "beta", sync([
    [
        "fail-if-not-master"
        # "fail-if-dirty"
    ]
    "version:beta"
    "git-commit"
    "git-push"
    "git-push-beta"
], "Deploy no Beta")

# Publica para o Heroku com novos assets
# Ainda naão funciona
# gulp.task "heroku-assets", sync([
#     [
#         "fail-if-not-master"
#         "fail-if-dirty"
#     ]
#     "precompile"
#     "version"
#     "git-commit"
#     "git-push"
#     "git-push-heroku"
#     "assets-clean"
# ], "Atualizando Heroku com novos Assets")
