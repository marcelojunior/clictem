class AddThemedCampaignIdToManagerAdCampaign < ActiveRecord::Migration
  def change
    add_column :manager_ad_campaigns, :themed_campaign_id, :integer
  end
end
