class AddResp2ToUsers < ActiveRecord::Migration
  def change
    add_column :users, :resp2_name, :string
    add_column :users, :resp2_email, :string
  end
end
