class CreateManagerAdImageTemps < ActiveRecord::Migration
  def change
    create_table :manager_ad_image_temps do |t|
      t.string :token
      t.text :base64

      t.timestamps
    end
  end
end
