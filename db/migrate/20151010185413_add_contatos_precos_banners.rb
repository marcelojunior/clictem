class AddContatosPrecosBanners < ActiveRecord::Migration
  def change
    add_column :users, :resp_cellphone, :string
    add_column :users, :resp2_cellphone, :string
    add_column :users, :resp3_cellphone, :string

    add_column :manager_ad_departments, :banner_value, :decimal, precision: 12, scale: 2, default: 0.0

    # Manager::Ad::Department.update_all(banner_value: 0.0)
  end
end
