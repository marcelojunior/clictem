class AddKindToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :kind, :string, default: 'Plano'
  end
end
