class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :user, index: true
      t.text :text
      t.references :product, index: true
      t.integer :question_id

      t.timestamps
    end
  end
end
