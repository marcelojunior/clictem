class CreateBlogPosts < ActiveRecord::Migration
  def change
    create_table :blog_posts do |t|
      t.integer :department_id
      t.string :title
      t.text :text
      t.string :tags
      t.string :image1
      t.string :image_fb
      t.string :slug

      t.timestamps
    end
  end
end
