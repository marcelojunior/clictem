class AddCurrentOfferToProducts < ActiveRecord::Migration
  def change
    add_column :manager_ad_products, :current_offer_id, :integer
    add_column :manager_ad_cupoms, :offer_id, :integer
  end
end
