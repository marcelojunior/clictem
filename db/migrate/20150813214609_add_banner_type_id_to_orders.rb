class AddBannerTypeIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :banner_type_id, :integer
  end
end
