class AddBgImageAndBgRepeatToUsers < ActiveRecord::Migration
  def change
    add_column :users, :bg_image, :string
    add_column :users, :bg_repeat, :boolean, default: false
    # User.update_all(bg_repeat: false)
  end
end
