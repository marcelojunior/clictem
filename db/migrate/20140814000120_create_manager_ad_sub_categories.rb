class CreateManagerAdSubCategories < ActiveRecord::Migration
  def change
    create_table :manager_ad_sub_categories do |t|
      t.string :name
      t.string :name_url
      t.belongs_to :category, index: true

      t.timestamps
    end
  end
end
