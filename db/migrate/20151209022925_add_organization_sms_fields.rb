class AddOrganizationSmsFields < ActiveRecord::Migration
  def change
    add_column :organizations, :commission_plan, :float
    add_column :organizations, :commission_banner, :float
    add_column :organizations, :commission_sms, :float

    add_column :organizations, :sms_unit_value, :float

    Organization.update_all(
        commission_plan: 10.0,
        commission_banner: 50.0,
        commission_sms: 0.0,
        sms_unit_value: 0.14
    )
  end
end
