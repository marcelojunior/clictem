class AddAdDeletedToVisits < ActiveRecord::Migration
  def change
    add_column :visits, :ad_deleted, :boolean, default: false

    # ids = Manager::Ad::Product.all.pluck(:id)
    # Visit.update_all(ad_deleted: true)
    # Visit.where(ad: ids).update_all(ad_deleted: false)
  end
end
