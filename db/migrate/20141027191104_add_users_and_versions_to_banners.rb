class AddUsersAndVersionsToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :image_topo, :string
    add_column :banners, :image_menu, :string
    add_column :banners, :image_user, :string
    add_column :banners, :user_id, :integer
  end
end
