class HabtmUserThemedCampaign < ActiveRecord::Migration
  def change
    create_table :manager_ad_themed_campaigns_users, id: false do |t|
      t.references :user
      t.references :themed_campaign
      t.boolean :pago, default: false
    end


  end
end
