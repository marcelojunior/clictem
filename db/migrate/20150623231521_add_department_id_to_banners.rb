class AddDepartmentIdToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :department_id, :integer
  end
end
