class AddOfferCampaignLimitToUsers < ActiveRecord::Migration
  def change
    add_column :users, :offer_campaign_limit, :integer, default: 0

  end
end
