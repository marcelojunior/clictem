class AddCampaignIdToManagerAdOffer < ActiveRecord::Migration
  def change
    add_column :manager_ad_offers, :offer_campaign_id, :integer
  end
end
