class RenameSmsGroups < ActiveRecord::Migration
  def change
    rename_table :sms_groups, :groups
  end
end
