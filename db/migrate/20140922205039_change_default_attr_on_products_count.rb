class ChangeDefaultAttrOnProductsCount < ActiveRecord::Migration
  def change
    change_column :manager_ad_departments, :products_count, :integer, default: 0, null: false
    change_column :manager_ad_categories, :products_count, :integer, default: 0, null: false
    change_column :manager_ad_sub_categories, :products_count, :integer, default: 0, null: false
  end
end
