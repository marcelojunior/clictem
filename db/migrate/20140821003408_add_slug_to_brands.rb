class AddSlugToBrands < ActiveRecord::Migration
  def change
    add_column :manager_ad_brands, :slug, :string

    remove_column :manager_ad_brands, :name_url

    add_index :manager_ad_brands, :slug, unique: true
  end
end
