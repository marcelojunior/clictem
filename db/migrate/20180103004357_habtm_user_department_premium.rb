class HabtmUserDepartmentPremium < ActiveRecord::Migration
  def change
    create_table :departments_users_premium, id: false do |t|
      t.references :user
      t.references :department
    end
  end
end
