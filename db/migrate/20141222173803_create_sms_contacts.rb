class CreateSmsContacts < ActiveRecord::Migration
  def change
    create_table :sms_contacts do |t|
      t.references :user, index: true
      t.integer :owner
      t.string :name
      t.string :phone
      t.string :email
      t.string :cellphone
      t.date :birthday

      t.timestamps
    end
  end
end
