class AddValueToSmsBuys < ActiveRecord::Migration
  def change
    add_column :sms_buys, :value, :decimal, precision: 12, scale: 2, default: 0.0
    # Sms::Buy.update_all(value: 0.0)
  end
end
