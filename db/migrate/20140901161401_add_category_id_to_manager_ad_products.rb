class AddCategoryIdToManagerAdProducts < ActiveRecord::Migration
  def change
    add_column :manager_ad_products, :category_id, :integer
  end
end
