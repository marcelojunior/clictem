class CreateBannerTypes < ActiveRecord::Migration
  def change
    create_table :banner_types do |t|
      t.string :name
      t.decimal :value, precision: 12, scale: 2, default: 0.0
      t.string :size
      t.boolean :department
      t.boolean :geral
      t.boolean :home
      t.string :placeholder

      t.timestamps
    end
  end
end
