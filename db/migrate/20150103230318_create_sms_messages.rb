class CreateSmsMessages < ActiveRecord::Migration
  def change
    create_table :sms_messages do |t|
      t.references :campaign, index: true
      t.references :user, index: true
      t.references :contact, index: true
      t.string :cellphone
      t.text :text
      t.string :status, default: 'aguardando'
      t.text :balance_origin, default: 'franchise'

      t.timestamps
    end

    add_column :sms_campaigns, :status, :string, default: 'avaliando'
  end
end
