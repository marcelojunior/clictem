class CreateSmsBuys < ActiveRecord::Migration
  def change
    create_table :sms_buys do |t|
      t.references :user, index: true
      t.integer :qtd
      t.boolean :active, default: false

      t.timestamps
    end
  end
end
