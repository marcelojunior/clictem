class AddBrandToProducts < ActiveRecord::Migration
  def change
    add_column :manager_ad_products, :brand_id, :integer, index: true
  end
end
