class AddInterestToProduct < ActiveRecord::Migration
  def change
    add_column :manager_ad_products, :has_interest, :boolean, default: false
    add_column :manager_ad_products, :interest_title, :string
    add_column :manager_ad_products, :interest_text, :text
  end
end
