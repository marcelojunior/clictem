class AddKeywordsAndDescriptionToManagerAdCategory < ActiveRecord::Migration
  def change
    add_column :manager_ad_departments, :keywords, :string
    add_column :manager_ad_departments, :description, :string

    add_column :manager_ad_categories, :keywords, :string
    add_column :manager_ad_categories, :description, :string

    add_column :manager_ad_sub_categories, :keywords, :string
    add_column :manager_ad_sub_categories, :description, :string
  end
end
