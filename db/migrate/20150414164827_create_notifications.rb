class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.string :subject
      t.text :body
      t.boolean :view, default: false
      t.string :to

      t.timestamps
    end
  end
end
