#encoding: utf-8
class CreateAdDepartments < ActiveRecord::Migration
  def change
    create_table :manager_ad_departments do |t|
      t.string :name
      t.string :name_url

      t.timestamps
    end
  end
end
