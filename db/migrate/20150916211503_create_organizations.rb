class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :fullname
      t.string :color1
      t.string :color2
      t.string :color3
      t.string :facebook
      t.string :phone
      t.string :email
      t.string :email_comercial
      t.string :domain
      t.boolean :active
      t.string :business_name
      t.string :cnpj
      t.string :address_street
      t.string :address_number
      t.string :address_additional
      t.string :address_neighborhood
      t.references :city, index: true
      t.references :state, index: true
      t.string :zip_code
      t.string :title_site
      t.string :ti_email
      t.string :mkt_email
      t.string :fin_email
      t.text :order_plan_checklist
      t.text :order_banner_checklist
      t.text :google_maps
      t.text :about

      t.timestamps
    end
  end
end
