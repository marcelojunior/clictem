class AddBirthdayToUsers < ActiveRecord::Migration
  def change
    add_column :users, :sms_birthday, :boolean, default: false
    add_column :users, :sms_birthday_message, :text

    # User.update_all(sms_birthday: false)
  end
end
