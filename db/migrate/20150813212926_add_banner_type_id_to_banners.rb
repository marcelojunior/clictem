class AddBannerTypeIdToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :banner_type_id, :integer
  end
end
