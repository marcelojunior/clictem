class AddMissingIndexes < ActiveRecord::Migration
  def change

    add_column :manager_ad_products, :state_id, :integer
    add_column :manager_ad_products, :city_id, :integer

    add_index :manager_ad_brands_departments, :department_id
    add_index :manager_ad_brands_departments, :brand_id
    add_index :redactor_assets, :user_id
    add_index :blog_posts, :department_id
    add_index :blog_posts, :author_id
    add_index :users, :city_id
    add_index :users, :state_id
    add_index :users, :department_id
    add_index :users, :mall_id
    add_index :manager_ad_products, :category_id
    add_index :manager_ad_products, :user_id
    add_index :manager_ad_products, :brand_id
    add_index :manager_ad_products, :state_id
    add_index :manager_ad_products, :city_id
    add_index :banners, :user_id
    add_index :comments, :question_id
  end
end
