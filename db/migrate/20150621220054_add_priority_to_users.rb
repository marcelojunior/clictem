class AddPriorityToUsers < ActiveRecord::Migration
  def change
    add_column :users, :priority, :integer, default: 0

    # User.where(plan: 'total').update_all(priority: 3)
    # User.where(plan: 'top').update_all(priority: 2)
    # User.where(plan: 'basico').update_all(priority: 1)
    # User.where(plan: 'free').update_all(priority: 0)
  end
end
