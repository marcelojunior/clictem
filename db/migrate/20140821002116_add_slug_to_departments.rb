class AddSlugToDepartments < ActiveRecord::Migration
  def change
    add_column :manager_ad_departments, :slug, :string

    remove_column :manager_ad_departments, :name_url

    add_index :manager_ad_departments, :slug, unique: true
  end
end
