class AddResp3ToUsers < ActiveRecord::Migration
  def change
    add_column :users, :resp3_name, :string
    add_column :users, :resp3_email, :string
  end
end
