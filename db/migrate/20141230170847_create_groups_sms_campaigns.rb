class CreateGroupsSmsCampaigns < ActiveRecord::Migration
  def change
    create_table :sms_campaigns_groups, :id => false do |t|
      t.integer :group_id
      t.integer :campaign_id
    end

    add_index :sms_campaigns_groups, [:group_id, :campaign_id],
      name: "sms_campaigns_groupsindex",
      unique: true
  end
end
