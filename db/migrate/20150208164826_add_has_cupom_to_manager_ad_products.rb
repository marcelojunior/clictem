class AddHasCupomToManagerAdProducts < ActiveRecord::Migration
  def change
    add_column :manager_ad_products, :has_cupom, :boolean, default: false
  end
end
