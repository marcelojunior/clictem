class AddSellerIdToComments < ActiveRecord::Migration
  def change
    add_column :comments, :seller_id, :integer

    Comment.all.each do |comment|
        next unless comment.product
        prod = comment.product
        user = prod.user
        comment.seller_id = user.id
        comment.save!
    end
  end
end
