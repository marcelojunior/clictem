class CreateManagerAdProducts < ActiveRecord::Migration
  def change
    create_table :manager_ad_products do |t|
      t.string :name
      t.belongs_to :department, index: true
      t.belongs_to :sub_category, index: true
      t.string :image1
      t.string :image2
      t.string :image3
      t.string :image4
      t.string :code
      t.decimal :price, precision: 12, scale: 2, default: 0.0
      t.decimal :offer_price, precision: 12, scale: 2, default: 0.0
      t.boolean :on_request
      t.boolean :is_old
      t.text :description
      t.string :payment_description
      t.integer :views_count, default: 0

      t.timestamps
    end
  end
end
