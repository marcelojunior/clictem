class RenameSmsContacts < ActiveRecord::Migration
  def change
    rename_table :sms_contacts, :customers
  end
end
