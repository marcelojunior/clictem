class AddFunnelToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :funnel, :boolean, default: false
    add_column :groups, :level, :integer, default: 0

    # Group.update_all(funnel: false)
    # User.all.each do |user|
    #     user.groups.create!(name: 'Contato', funnel: true, level: 1) if user.groups.where(level: 1).blank?
    #     user.groups.create!(name: 'Oportunidade', funnel: true, level: 2) if user.groups.where(level: 2).blank?
    #     user.groups.create!(name: 'Cliente', funnel: true, level: 3) if user.groups.where(level: 3).blank?
    # end

  end
end
