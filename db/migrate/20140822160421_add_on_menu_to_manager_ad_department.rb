class AddOnMenuToManagerAdDepartment < ActiveRecord::Migration
  def change
    add_column :manager_ad_departments, :on_menu, :boolean, default: false
    add_column :manager_ad_departments, :raw_order, :integer
  end
end
