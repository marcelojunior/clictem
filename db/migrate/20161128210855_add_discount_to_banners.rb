class AddDiscountToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :discount, :decimal, precision: 12, scale: 2, default: 0.0
    add_column :banners, :discount_type, :integer, default: 0

    Banner.update_all(
        discount: 0.0,
        discount_type: 0
    )
  end
end
