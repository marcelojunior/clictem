class AddContratToUsers < ActiveRecord::Migration
  def change
    add_column :users, :contract_start, :date
    add_column :users, :contract_end, :date
    add_column :users, :contract_value, :decimal, precision: 12, scale: 2, default: 0.0
    add_column :users, :satisfaction, :boolean, default: true

    # User.update_all(contract_value: 0.0, satisfaction: true)
  end
end
