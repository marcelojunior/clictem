class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :user
      t.float :value, default: 0.0
      t.integer :installment
      t.text :note
      t.boolean :done, default: false

      t.timestamps
    end
  end
end
