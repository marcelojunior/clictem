class AddServiceAndUserToProducts < ActiveRecord::Migration
  def change
    add_column :manager_ad_products, :is_service, :boolean, default: false
    add_column :manager_ad_products, :user_id, :integer, index: true
  end
end
