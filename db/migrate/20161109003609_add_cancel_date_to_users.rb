class AddCancelDateToUsers < ActiveRecord::Migration
  def change
    add_column :users, :cancel_date, :date
  end
end
