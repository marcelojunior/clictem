class CreateManagerAdBrands < ActiveRecord::Migration
  def change
    create_table :manager_ad_brands do |t|
      t.string :name
      t.string :name_url
      t.belongs_to :department, index: true

      t.timestamps
    end
  end
end
