class AddPublishedToManagerAdProducts < ActiveRecord::Migration
  def change
    add_column :manager_ad_products, :published, :boolean, default: false
  end
end
