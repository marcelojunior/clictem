class AddCellphoneDefaults < ActiveRecord::Migration
  def change
    change_column :users, :resp_cellphone, :string, default: ''
    change_column :users, :resp2_cellphone, :string, default: ''
    change_column :users, :resp3_cellphone, :string, default: ''

    # User.where(resp_cellphone: nil).update_all(resp_cellphone: '')
    # User.where(resp2_cellphone: nil).update_all(resp_cellphone: '')
    # User.where(resp3_cellphone: nil).update_all(resp_cellphone: '')
  end
end
