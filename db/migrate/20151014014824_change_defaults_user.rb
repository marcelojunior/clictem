class ChangeDefaultsUser < ActiveRecord::Migration
  def change
    change_column(:users, :has_cupoms, :boolean, default: true)
    change_column(:users, :has_sms, :boolean, default: true)
  end
end
