class ChangeFormatUsedCupom < ActiveRecord::Migration
  def change
    remove_column :manager_ad_cupoms, :used
    add_column :manager_ad_cupoms, :used, :date
  end
end
