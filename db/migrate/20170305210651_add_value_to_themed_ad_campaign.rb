class AddValueToThemedAdCampaign < ActiveRecord::Migration
  def change
    add_column :manager_ad_themed_campaigns, :value, :decimal, precision: 12, scale: 2, default: 0.0
    Manager::Ad::ThemedCampaign.update_all(value: 0.0)
  end
end
