class AddFromOnlyToManagerAdProduct < ActiveRecord::Migration
  def change
    add_column :manager_ad_products, :from_only, :boolean, default: false

    # Manager::Ad::Product.update_all(from_only: false)
  end
end
