class AddPremiumToUsers < ActiveRecord::Migration
  def change
    add_column :users, :premium, :boolean, default: false

    Apartment::Tenant.each do |org|
        Apartment::Tenant.switch!(org)
        User.where(plan: 'total').update_all(premium: true)
        User.where(plan: 'limitado').update_all(plan: 'basico')
        User.where(plan: 'iniciante').update_all(plan: 'basico')
        User.where(plan: 'total').update_all(plan: 'top')

        User.where(plan: ['basico', 'top']).each do |user|
          user.premium = true
          user.save
        end
    end

  end
end
