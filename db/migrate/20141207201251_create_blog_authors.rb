class CreateBlogAuthors < ActiveRecord::Migration
  def change
    create_table :blog_authors do |t|
      t.string :name
      t.text :bio
      t.string :image

      t.timestamps
    end

    add_column :blog_posts, :author_id, :integer
  end
end
