class CreateSmsCampaigns < ActiveRecord::Migration
  def change
    create_table :sms_campaigns do |t|
      t.references :user
      t.text :message
      t.string :link
      t.integer :contacts_count
      t.date :schedule_date
      t.time :schedule_time

      t.timestamps
    end

    add_column :users, :sms_franchise_balance, :integer, default: 0
    add_column :users, :sms_franchise_total, :integer, default: 0
    add_column :users, :sms_hired_balance, :integer, default: 0
    add_column :users, :sms_hired_due, :date
    # User.update_all(sms_franchise_balance: 0, sms_hired_balance: 0, sms_franchise_total: 0, sms_hired_due: Date.today)
  end
end
