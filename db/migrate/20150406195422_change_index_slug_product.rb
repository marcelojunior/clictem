class ChangeIndexSlugProduct < ActiveRecord::Migration
  def change
    remove_index :manager_ad_products, :slug

    add_index :manager_ad_products, [:slug, :id], unique: true
  end
end
