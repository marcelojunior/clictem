class CreateManagerAdCupoms < ActiveRecord::Migration
  def change
    create_table :manager_ad_cupoms do |t|
      t.references :user, index: true
      t.references :product, index: true
      t.integer :seller_id
      t.string :code
      t.date :due
      t.boolean :used, default: false
      t.decimal :price, precision: 12, scale: 2, default: 0.0
      t.decimal :offer_price, precision: 12, scale: 2, default: 0.0

      t.timestamps
    end
  end
end
