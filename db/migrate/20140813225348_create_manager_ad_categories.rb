class CreateManagerAdCategories < ActiveRecord::Migration
  def change
    create_table :manager_ad_categories do |t|
      t.string :name
      t.string :name_url
      t.belongs_to :department, index: true

      t.timestamps
    end
  end
end
