class AddPeriodAndKindToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :date_start, :date
    add_column :orders, :date_end, :date
    add_column :orders, :banner_position, :string
  end
end
