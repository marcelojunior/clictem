class AddSubtitleToBlogAuthors < ActiveRecord::Migration
  def change
    add_column :blog_authors, :subtitle, :string
  end
end
