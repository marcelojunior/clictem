class AddHasSmsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :has_sms, :boolean, default: false

    # User.update_all(has_sms: false)
  end
end
