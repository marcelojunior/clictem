class RenameHabtmSmsCampaignsGroups < ActiveRecord::Migration
  def change
    remove_index :sms_campaigns_groups, name: 'sms_campaigns_groupsindex'
    rename_table :sms_campaigns_groups, :groups_sms_campaigns

    add_index :groups_sms_campaigns, [:campaign_id, :group_id],
      name: "groups_sms_campaigns_index",
      unique: true
  end
end
