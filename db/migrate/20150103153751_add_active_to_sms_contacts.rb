class AddActiveToSmsContacts < ActiveRecord::Migration
  def change
    add_column :sms_contacts, :active, :boolean, default: true

    # Sms::Contact.update_all(active: true)
  end
end
