class CreateVisits < ActiveRecord::Migration
  def change
    create_table :visits do |t|
      t.integer :ad
      t.references :category, index: true
      t.references :department, index: true
      t.integer :owner
      t.references :banner, index: true
      t.integer :visitor, index: true
      t.string :cluster
      t.text :product
      t.string :age
      t.text :interest
      t.text :brand
      t.text :career
      t.string :gender
      t.string :income
      t.string :ip

      t.timestamps
    end
  end
end