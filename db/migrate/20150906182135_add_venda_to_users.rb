class AddVendaToUsers < ActiveRecord::Migration
  def change
    add_column :users, :deal_level, :string, default: 'Stand By'
    add_column :users, :deal_note, :text
    add_column :users, :deal_suggest_plan_note, :text
    add_column :users, :deal_suggest_plan_value, :decimal, precision: 12, scale: 2, default: 0.0
    add_column :users, :deal_suggest_banner_note, :text
    add_column :users, :deal_suggest_banner_value, :decimal, precision: 12, scale: 2, default: 0.0



    # User.update_all(deal_level: 'Cliente')
  end
end
