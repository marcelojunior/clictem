class AddOnMenuToManagerAdCategories < ActiveRecord::Migration
  def change
    add_column :manager_ad_categories, :on_menu, :boolean, default: false
    add_column :manager_ad_categories, :raw_order, :integer
    add_column :manager_ad_sub_categories, :on_menu, :boolean, default: false
    add_column :manager_ad_sub_categories, :raw_order, :integer
  end
end
