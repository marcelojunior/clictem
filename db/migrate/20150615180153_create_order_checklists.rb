class CreateOrderChecklists < ActiveRecord::Migration
  def change
    create_table :order_checklists do |t|
      t.references :order, index: true
      t.string :name
      t.boolean :done, default: false
      t.string :resp

      t.timestamps
    end
  end
end
