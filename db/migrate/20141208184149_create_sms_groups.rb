class CreateSmsGroups < ActiveRecord::Migration
  def change
    create_table :sms_groups do |t|
      t.string :name
      t.references :user, index: true

      t.timestamps
    end
  end
end
