class CreateSmsContactsSmsGroups < ActiveRecord::Migration
  def change
    create_table :sms_contacts_groups, :id => false do |t|
        t.integer :contact_id
        t.integer :group_id
    end

    add_index :sms_contacts_groups, [:contact_id, :group_id],
      name: "sms_contacts_groups_index",
      unique: true
  end
end
