class AddFbAppIdAndFbAppSecretInOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :fb_app_id, :string
    add_column :organizations, :fb_app_secret, :string
  end
end
