class ChangePaymentDescriptionOnProductTable < ActiveRecord::Migration
  def change
    change_column :manager_ad_products, :payment_description, :text
  end
end
