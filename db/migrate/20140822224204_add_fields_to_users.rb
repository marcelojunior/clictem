class AddFieldsToUsers < ActiveRecord::Migration
  def change
    # add_column :users, :name, :string, null: false
    add_column :users, :last_name, :string
    add_column :users, :business_name, :string
    add_column :users, :trade_name, :string
    add_column :users, :cpf, :string
    add_column :users, :cnpj, :string
    add_column :users, :ie, :string
    add_column :users, :genre, :string
    add_column :users, :birthday, :date
    add_column :users, :phone, :string
    add_column :users, :cellphone, :string
    add_column :users, :want_email, :boolean, default: true
    add_column :users, :want_sms, :boolean, default: true
    add_column :users, :address_street, :string
    add_column :users, :address_number, :string
    add_column :users, :address_district, :string
    add_column :users, :address_compl, :string
    add_column :users, :address_zipcode, :string
    add_column :users, :city_id, :integer
    add_column :users, :state_id, :integer
    add_column :users, :resp_name, :string
    add_column :users, :resp_birthday, :date
    add_column :users, :resp_cpf, :string
    add_column :users, :resp_email, :string
    add_column :users, :subdomain, :string, unique: true
    add_column :users, :plan, :string, default: 'free'
    add_column :users, :ad_limit, :integer, default: 25
  end
end