class AddTitleToBlogAuthors < ActiveRecord::Migration
  def change
    add_column :blog_authors, :title, :string
  end
end
