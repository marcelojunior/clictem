class ChangeColumnValueOrders < ActiveRecord::Migration
  def change
    change_column :orders, :value, :decimal, precision: 12, scale: 2, default: 0.0
  end
end
