class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :name
      t.datetime :start_date
      t.datetime :end_date
      t.string :client_name
      t.string :url
      t.string :contract

      t.timestamps
    end
  end
end
