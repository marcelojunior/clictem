class AddOfferToProducts < ActiveRecord::Migration
  def change
  	# rename_column :manager_ad_products, :value_new, :offer_price
  	# rename_column :manager_ad_products, :value_old, :price
  	rename_column :manager_ad_products, :published_until, :offer_start

  	add_column :manager_ad_products, :offer_end, :date
    add_column :manager_ad_products, :is_offer, :boolean, default: false

   #  Manager::Ad::Product.where('offer_price < price').update_all(is_offer: true)
  	# Manager::Ad::Product.update_all(offer_start: Date.today, offer_end: Date.today + 60.days)

  	# Manager::Ad::Product.all.each do |prod|
  	# 	value_old = prod.price
  	# 	value_new = prod.offer_price

  	# 	prod.price = value_new
  	# 	prod.offer_price = value_old

  	# 	prod.save
  	# end
  end
end



# Manager::Ad::Product.all.each do |prod|
#   price = prod.price
#   offer = prod.offer_price

#   if offer > 0 and price == 0
#     prod.price = offer
#     prod.offer_price = 0
#   elsif offer > price
#     prod.price = offer
#     prod.offer_price = price
#   end

#   if prod.on_request
#     prod.offer_price = 0
#   end

#   prod.save
# end

# Manager::Ad::Product.where('offer_price < price').update_all(is_offer: true)
# Manager::Ad::Product.where('offer_price >= price or offer_price <= 0').update_all(is_offer: false)





