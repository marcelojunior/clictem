class AddScriptToUsersAndProducts < ActiveRecord::Migration
  def change
    add_column :users, :script, :text
    add_column :manager_ad_products, :script, :text
  end
end
