class AddIconToDepartment < ActiveRecord::Migration
  def change
    add_column :manager_ad_departments, :icon, :string

    # Manager::Ad::Department.where(slug:'casa-e-escritorio').update_all(icon: 'icons-casa')
    # Manager::Ad::Department.where(slug:'tv-celulares-e-tecnologia').update_all(icon: 'icons-info')
    # Manager::Ad::Department.where(slug:'moda-e-beleza').update_all(icon: 'icons-moda')
    # Manager::Ad::Department.where(slug:'esporte-e-saude').update_all(icon: 'icons-esporte')
    # Manager::Ad::Department.where(slug:'veiculos').update_all(icon: 'icons-veiculos')
    # Manager::Ad::Department.where(slug:'imoveis').update_all(icon: 'icons-imoveis')
    # Manager::Ad::Department.where(slug:'bebes-e-brinquedos').update_all(icon: 'icons-bebe')
    # Manager::Ad::Department.where(slug:'alimentos-e-bebidas').update_all(icon: 'icons-alimentos')
    # Manager::Ad::Department.where(slug:'todos-os-departamentos').update_all(icon: 'icons-seta')
  end
end
