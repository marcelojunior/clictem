class CreateManagerAdInterests < ActiveRecord::Migration
  def change
    create_table :manager_ad_interests do |t|
      t.belongs_to :product, index: true
      t.belongs_to :user, index: true
      t.string :name
      t.string :email
      t.string :phone
      t.string :cellphone
      t.text :note

      t.timestamps
    end
  end
end
