class CreateThemedCampaigns < ActiveRecord::Migration
  def change
    create_table :manager_ad_themed_campaigns do |t|
      t.string :title
      t.string :subdomain
      t.text :description
      t.string :color1, default: '#f17420'
      t.string :color2, default: '#16556a'
      t.string :color3, default: '#898b8d'
      t.string :img_bg
      t.string :img_1
      t.string :img_2
      t.string :img_3
      t.string :img_4
      t.string :img_5
      t.string :img_6
      t.date :date_start
      t.date :date_end
      t.boolean :active, default: false

      t.timestamps
    end
  end
end
