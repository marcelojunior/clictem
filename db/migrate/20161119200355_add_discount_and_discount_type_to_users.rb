class AddDiscountAndDiscountTypeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :discount, :decimal, precision: 12, scale: 2, default: 0.0
    add_column :users, :discount_type, :integer, default: 0

    add_column :bills, :discount, :decimal, precision: 12, scale: 2, default: 0.0
    add_column :bills, :discount_type, :integer, default: 0

    User.update_all(
        discount: 0.0,
        discount_type: 0
    )

    Bill.update_all(
        discount: 0.0,
        discount_type: 0
    )
  end
end
