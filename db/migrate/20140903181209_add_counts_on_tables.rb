class AddCountsOnTables < ActiveRecord::Migration
  def change
    add_column :manager_ad_departments, :products_count, :integer
    add_column :manager_ad_categories, :products_count, :integer
    add_column :manager_ad_sub_categories, :products_count, :integer
  end
end
