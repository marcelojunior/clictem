class CreatePages < ActiveRecord::Migration
  def change
    # drop_table :pages

    create_table :pages do |t|
      t.string :title
      t.string :description
      t.string :keywords
      t.text :content

      t.timestamps
    end
  end
end
