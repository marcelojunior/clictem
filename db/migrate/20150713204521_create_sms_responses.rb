class CreateSmsResponses < ActiveRecord::Migration
  def change
    create_table :sms_responses do |t|
      t.references :message
      t.references :customer
      t.text :text
      t.string :uniqid
      t.references :user
      t.references :campaign
      t.boolean :read, default: false

      t.timestamps
    end
  end
end
