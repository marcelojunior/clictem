class AddNewLinkToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :news_link, :string
  end
end
