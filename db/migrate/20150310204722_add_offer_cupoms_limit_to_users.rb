class AddOfferCupomsLimitToUsers < ActiveRecord::Migration
  def change
    add_column :users, :offer_cupoms_limit, :integer, default: 0
  end
end
