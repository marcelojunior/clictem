class AddColorsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :color1, :string
    add_column :users, :color2, :string
    add_column :users, :color3, :string

    # User.update_all(
    # 	color1: '#f17420',
    # 	color2: '#16556a',
    # 	color3: '#898b8d'
    # )
  end
end
