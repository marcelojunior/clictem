class CreateManagerAdImages < ActiveRecord::Migration
  def change
    # drop_table :manager_ad_images
    create_table :manager_ad_images do |t|
      t.references :product, index: true
      t.string :file
      t.text :base64
      t.boolean :highlight, default: false

      t.timestamps
    end
  end
end
