class AddSlugToCategories < ActiveRecord::Migration
  def change
    add_column :manager_ad_categories, :slug, :string

    remove_column :manager_ad_categories, :name_url

    add_index :manager_ad_categories, :slug, unique: true
  end
end
