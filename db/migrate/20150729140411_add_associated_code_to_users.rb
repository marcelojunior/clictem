class AddAssociatedCodeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :associated_code, :string
  end
end
