class AddObsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :contract_monthly, :decimal, precision: 12, scale: 2, default: 0.0
    add_column :users, :contract_period, :integer, default: 12
    add_column :users, :note, :text
  end
end
