class AddGenreToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :genre, :string
  end
end
