class AddHiddenToManagerAdThemedCampaign < ActiveRecord::Migration
  def change
    add_column :manager_ad_themed_campaigns, :hidden, :boolean, default: false

    Manager::Ad::ThemedCampaign.update_all(hidden: false)
  end
end
