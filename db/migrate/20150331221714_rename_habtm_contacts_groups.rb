class RenameHabtmContactsGroups < ActiveRecord::Migration
  def change
    remove_index :sms_contacts_groups, name: 'sms_contacts_groups_index'
    rename_table :sms_contacts_groups, :customers_groups
    rename_column :customers_groups, :contact_id, :customer_id

    add_index :customers_groups, [:customer_id, :group_id],
      name: "customers_groups_index",
      unique: true
  end
end
