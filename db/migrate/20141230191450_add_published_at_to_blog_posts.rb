class AddPublishedAtToBlogPosts < ActiveRecord::Migration
  def up
    add_column :blog_posts, :published_at, :datetime

    Blog::Post.all.each do |p|
      p.published_at = p.created_at
      p.save
    end
  end

  def down
    remove_column :blog_posts, :published_at
  end
end
