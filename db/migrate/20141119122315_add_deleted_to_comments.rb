class AddDeletedToComments < ActiveRecord::Migration
  def change
    add_column :comments, :deleted, :boolean, default: false
    # Comment.update_all(deleted: false)
  end
end
