class AddWaitingToManagerAdImages < ActiveRecord::Migration
  def change
    add_column :manager_ad_images, :waiting, :boolean, default: true

    # Manager::Ad::Image.update_all(waiting: true)
  end
end
