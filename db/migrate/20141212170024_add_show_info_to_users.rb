class AddShowInfoToUsers < ActiveRecord::Migration
  def change
    add_column :users, :show_info, :boolean, default: true

    # User.update_all(show_info: true)
  end
end
