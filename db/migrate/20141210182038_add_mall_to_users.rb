class AddMallToUsers < ActiveRecord::Migration
  def change
    add_column :users, :mall_id, :integer
    add_column :users, :is_mall, :boolean, default: false
  end
end
