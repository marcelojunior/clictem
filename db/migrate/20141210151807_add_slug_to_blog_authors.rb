class AddSlugToBlogAuthors < ActiveRecord::Migration
  def change
    add_column :blog_authors, :slug, :string
  end
end
