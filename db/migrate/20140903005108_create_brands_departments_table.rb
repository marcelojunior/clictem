class CreateBrandsDepartmentsTable < ActiveRecord::Migration
  def change
    create_table :manager_ad_brands_departments, id: false do |t|
      t.references :brand
      t.references :department
    end

    # add_index :manager_ad_brands_departments, [:brand_id, :department_id]
    # add_index :manager_ad_brands_departments, :department_id

  end
end
