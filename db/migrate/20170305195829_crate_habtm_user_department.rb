class CrateHabtmUserDepartment < ActiveRecord::Migration
  def change
    create_table :manager_ad_departments_users, id: false do |t|
      t.references :user
      t.references :department
    end

    Apartment::Tenant.each do |org|
        Apartment::Tenant.switch!(org)
        User.where.not(department_id: nil).each do |user|
          user.departments << Manager::Ad::Department.find(user.department_id)
          user.save
        end
    end

  end
end
