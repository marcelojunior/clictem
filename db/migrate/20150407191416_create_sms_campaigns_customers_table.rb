class CreateSmsCampaignsCustomersTable < ActiveRecord::Migration
  def change
    create_table :customers_sms_campaigns, id: false do |t|
        t.integer :campaign_id
        t.integer :customer_id
    end

    add_index :customers_sms_campaigns, [:campaign_id, :customer_id]
    add_index :customers_sms_campaigns, :customer_id
  end
end
