class AddLimitOfCupoms < ActiveRecord::Migration
  def change
    add_column :manager_ad_products, :cupom_limit, :integer, default: 0
    add_column :manager_ad_products, :cupom_generated, :integer, default: 0
    add_column :manager_ad_products, :has_cupom_limit, :boolean, default: false
    add_column :manager_ad_offers, :cupom_limit, :integer, default: 0
    add_column :manager_ad_offers, :cupom_generated, :integer, default: 0
    add_column :manager_ad_offers, :has_cupom_limit, :boolean, default: false

    # Manager::Ad::Product.update_all(cupom_limit: 0, cupom_generated: 0, has_cupom_limit: false)
    # Manager::Ad::Offer.update_all(cupom_limit: 0, cupom_generated: 0, has_cupom_limit: false)
  end
end
