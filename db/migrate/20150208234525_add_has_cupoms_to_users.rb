class AddHasCupomsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :has_cupoms, :boolean, default: false
    # User.update_all(has_cupoms: false)
  end
end
