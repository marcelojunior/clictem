class AddDescriptionAndKeywordsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :description, :text
    add_column :users, :keywords, :text
  end
end
