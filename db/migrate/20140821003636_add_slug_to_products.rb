class AddSlugToProducts < ActiveRecord::Migration
  def change
    add_column :manager_ad_products, :slug, :string

    add_index :manager_ad_products, :slug, unique: true
  end
end
