class CreateManagerAdOffers < ActiveRecord::Migration
  def change
    create_table :manager_ad_offers do |t|
      t.references :product, index: true
      t.references :user, index: true
      t.date :date_start
      t.date :date_end
      t.boolean :wday0, default: true
      t.boolean :wday1, default: true
      t.boolean :wday2, default: true
      t.boolean :wday3, default: true
      t.boolean :wday4, default: true
      t.boolean :wday5, default: true
      t.boolean :wday6, default: true
      t.boolean :has_cupom, default: false
      t.boolean :repeat, default: false
      t.decimal :price, precision: 12, scale: 2, default: 0.0

      t.timestamps
    end
  end
end
