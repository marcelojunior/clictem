class CreateManagerAdInfoClicks < ActiveRecord::Migration
  def change
    create_table :manager_ad_info_clicks do |t|
      t.references :product, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
