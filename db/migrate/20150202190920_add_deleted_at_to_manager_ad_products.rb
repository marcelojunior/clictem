class AddDeletedAtToManagerAdProducts < ActiveRecord::Migration
  def change
    add_column :manager_ad_products, :deleted_at, :datetime
    add_index :manager_ad_products, :deleted_at
    # Manager::Ad::Product.update_all(is_old: false)
  end
end
