class AddCupomNoteToManagerAdOffers < ActiveRecord::Migration
  def change
    add_column :manager_ad_offers, :cupom_note, :string
    add_column :manager_ad_products, :cupom_note, :string
  end
end
