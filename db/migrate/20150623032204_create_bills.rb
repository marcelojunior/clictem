class CreateBills < ActiveRecord::Migration
  def change
    create_table :bills do |t|
      t.references :user, index: true
      t.decimal :value, precision: 12, scale: 2, default: 0.0
      t.references :order, index: true
      t.references :sms_buy, index: true
      t.decimal :commission, precision: 12, scale: 2, default: 0.0
      t.date :due
      t.boolean :paid, :boolean, default: true
      t.string :note

      t.timestamps
    end
  end
end
