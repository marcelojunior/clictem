class AddKeywordsAndDescriptionToManagerAdBrand < ActiveRecord::Migration
  def change
    add_column :manager_ad_brands, :keywords, :string
    add_column :manager_ad_brands, :description, :string
  end
end
