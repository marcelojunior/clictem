# ADD Departamentos
# Apartment::Tenant.switch!('localhost')

casa = Manager::Ad::Department.find_or_create_by(name: 'Casa e Escritório', on_menu: true, raw_order: 1)
  eletro = casa.categories.find_or_create_by name: "Eletrodomésticos", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Fogões e Cooktops", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Fornos", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Freezer", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Lavadoras de Roupas", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Microondas", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Refrigeradores", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Acessórios", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Adega de Vinhos", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Aquecedores", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Climatizadores e Ar-condicionado", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Coifas", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Cooler", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Depuradores de Ar", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Lava-Louça", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Secadora de Roupas", on_menu: true
    # eletro.sub_categories.find_or_create_by name: "Ventiladores e Circuladores", on_menu: true
  moveis = casa.categories.find_or_create_by name: "Móveis", on_menu: true
    # moveis.sub_categories.find_or_create_by name: "Quarto", on_menu: true
    # moveis.sub_categories.find_or_create_by name: "Cozinha", on_menu: true
    # moveis.sub_categories.find_or_create_by name: "Sala de Estar", on_menu: true
    # moveis.sub_categories.find_or_create_by name: "Decoração", on_menu: true
    # moveis.sub_categories.find_or_create_by name: "Móveis de Escritório", on_menu: true
    # moveis.sub_categories.find_or_create_by name: "Colchões", on_menu: true
    # moveis.sub_categories.find_or_create_by name: "Sala de Jantar", on_menu: true
    # moveis.sub_categories.find_or_create_by name: "Bar", on_menu: true
    # moveis.sub_categories.find_or_create_by name: "Banheiro", on_menu: true
    # moveis.sub_categories.find_or_create_by name: "Área de Serviço", on_menu: true
    # moveis.sub_categories.find_or_create_by name: "Jardim de Lazer", on_menu: true
  eletroportateis = casa.categories.find_or_create_by name: "Eletroportáteis", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Aspirador de Pó", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Cafeteiras", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Grill e Sanduicheiras", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Processador de Alimentos", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Ventiladores e Circuladores", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Acessórios", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Batedeiras", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Bebedouros e Purificadores", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Churrasqueiras Elétricas", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Espremedores de Frutas", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Ferro de Passar", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Fritadeiras", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Liquidificadores e Acessórios", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Máquinas de Costura", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Mixer", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Panelas Elétricas", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Panificadora", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Processador de Alimentos", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Omeleteira", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Depiladores", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Pranchas - Chapinhas", on_menu: true
    # eletroportateis.sub_categories.find_or_create_by name: "Secadores de Cabelos", on_menu: true
  utilidades = casa.categories.find_or_create_by name: "Utilidades Domésticas", on_menu: true
    # utilidades.sub_categories.find_or_create_by name: "Aparelhos de Jantar", on_menu: true
    # utilidades.sub_categories.find_or_create_by name: "Baixelas e Travessas", on_menu: true
    # utilidades.sub_categories.find_or_create_by name: "Faqueiros", on_menu: true
    # utilidades.sub_categories.find_or_create_by name: "Conjunto de Panelas", on_menu: true
    # utilidades.sub_categories.find_or_create_by name: "Churrasqueiras e Acessórios", on_menu: true
    # utilidades.sub_categories.find_or_create_by name: "Decoração", on_menu: true
  papelaria = casa.categories.find_or_create_by name: "Papelaria", on_menu: true
    # papelaria.sub_categories.find_or_create_by name: "Escritório", on_menu: true
    # papelaria.sub_categories.find_or_create_by name: "Papéis e Pastas", on_menu: true
    # papelaria.sub_categories.find_or_create_by name: "Escrita e Corretivos", on_menu: true
    # papelaria.sub_categories.find_or_create_by name: "Mochila, Estojos e Lancheiras", on_menu: true
    # papelaria.sub_categories.find_or_create_by name: "Calculadoras", on_menu: true
  cama = casa.categories.find_or_create_by name: "Cama, Mesa e Banho", on_menu: true
    # cama.sub_categories.find_or_create_by name: "Jogo de Cama", on_menu: true
    # cama.sub_categories.find_or_create_by name: "Edredom", on_menu: true
    # cama.sub_categories.find_or_create_by name: "Cobertor e Manta", on_menu: true
    # cama.sub_categories.find_or_create_by name: "Colcha e Cobre-leito", on_menu: true
    # cama.sub_categories.find_or_create_by name: "Travesseiro", on_menu: true
  casa.categories.find_or_create_by name: "Serviços", on_menu: true

# --
tecnologia = Manager::Ad::Department.find_or_create_by(name: 'Informática e Tecnologia', on_menu: true, raw_order: 2)
  tv = tecnologia.categories.find_or_create_by name: "TV e Vídeo", on_menu: true
    # tv.sub_categories.find_or_create_by name: "TV LED", on_menu: true
    # tv.sub_categories.find_or_create_by name: "TV 3D", on_menu: true
    # tv.sub_categories.find_or_create_by name: "Smart TV", on_menu: true
    # tv.sub_categories.find_or_create_by name: "Smart TV 3D", on_menu: true
    # tv.sub_categories.find_or_create_by name: "Blu Ray Player", on_menu: true
  info = tecnologia.categories.find_or_create_by name: "Informática", on_menu: true
    # info.sub_categories.find_or_create_by name: "Notebook", on_menu: true
    # info.sub_categories.find_or_create_by name: "Tablets e Ipad", on_menu: true
    # info.sub_categories.find_or_create_by name: "Computadores", on_menu: true
    # info.sub_categories.find_or_create_by name: "Monitores", on_menu: true
    # info.sub_categories.find_or_create_by name: "Impressoras e Acessórios", on_menu: true
  celulares = tecnologia.categories.find_or_create_by name: "Celulares e Telefones", on_menu: true
    # celulares.sub_categories.find_or_create_by name: "Smartphones", on_menu: true
    # celulares.sub_categories.find_or_create_by name: "Celulares", on_menu: true
    # celulares.sub_categories.find_or_create_by name: "Telefonia Fixa", on_menu: true
    # celulares.sub_categories.find_or_create_by name: "Acessórios para Celulares", on_menu: true
  games = tecnologia.categories.find_or_create_by name: "Games", on_menu: true
    # games.sub_categories.find_or_create_by name: "Xbox One", on_menu: true
    # games.sub_categories.find_or_create_by name: "Xbox 360", on_menu: true
    # games.sub_categories.find_or_create_by name: "Playstation 4", on_menu: true
    # games.sub_categories.find_or_create_by name: "Playstation 3", on_menu: true
    # games.sub_categories.find_or_create_by name: "Jogos para PC", on_menu: true
  cine = tecnologia.categories.find_or_create_by name: "Cine e Foto", on_menu: true
    # cine.sub_categories.find_or_create_by name: "Home Theater", on_menu: true
    # cine.sub_categories.find_or_create_by name: "Micro System", on_menu: true
    # cine.sub_categories.find_or_create_by name: "Dock Station", on_menu: true
    # cine.sub_categories.find_or_create_by name: "iPod", on_menu: true
    # cine.sub_categories.find_or_create_by name: "Fones de Ouvido", on_menu: true

# --
moda = Manager::Ad::Department.find_or_create_by(name: 'Moda e Beleza', on_menu: true, raw_order: 3)
  mod = moda.categories.find_or_create_by name: "Moda", on_menu: true
    # mod.sub_categories.find_or_create_by name: "Roupa Feminina", on_menu: true
    # mod.sub_categories.find_or_create_by name: "Roupa Masculina", on_menu: true
    # mod.sub_categories.find_or_create_by name: "Moda Íntima Feminina", on_menu: true
    # mod.sub_categories.find_or_create_by name: "Cuecas", on_menu: true
  calcados = moda.categories.find_or_create_by name: "Calçados", on_menu: true
    # calcados.sub_categories.find_or_create_by name: "Femininos", on_menu: true
    # calcados.sub_categories.find_or_create_by name: "Masculinos", on_menu: true
    # calcados.sub_categories.find_or_create_by name: "Infantis", on_menu: true
  relogios = moda.categories.find_or_create_by name: "Relógios", on_menu: true
    # relogios.sub_categories.find_or_create_by name: "Masculinos", on_menu: true
    # relogios.sub_categories.find_or_create_by name: "Femininos", on_menu: true
    # relogios.sub_categories.find_or_create_by name: "Unissex", on_menu: true
  joias = moda.categories.find_or_create_by name: "Jóias", on_menu: true
    # joias.sub_categories.find_or_create_by name: "Alianças", on_menu: true
    # joias.sub_categories.find_or_create_by name: "Brincos", on_menu: true
    # joias.sub_categories.find_or_create_by name: "Pingentes", on_menu: true
    # joias.sub_categories.find_or_create_by name: "Pulseiras e Braceletes", on_menu: true
  beleza = moda.categories.find_or_create_by name: "Beleza", on_menu: true
    # beleza.sub_categories.find_or_create_by name: "Secadores de Cabelo", on_menu: true
    # beleza.sub_categories.find_or_create_by name: "Pranchas", on_menu: true
    # beleza.sub_categories.find_or_create_by name: "Barbeadores", on_menu: true
  permufaria = moda.categories.find_or_create_by name: "Perfumaria", on_menu: true
    # permufaria.sub_categories.find_or_create_by name: "Perfumes Masculinos", on_menu: true
    # permufaria.sub_categories.find_or_create_by name: "Perfumes Femininos", on_menu: true
    # permufaria.sub_categories.find_or_create_by name: "Perfumes Unissex", on_menu: true

# --
esporte = Manager::Ad::Department.find_or_create_by(name: 'Esporte e Saúde', on_menu: true, raw_order: 4)
  esp = esporte.categories.find_or_create_by name: "Esporte e Lazer", on_menu: true
    # esp.sub_categories.find_or_create_by name: "Bicicletas", on_menu: true
    # esp.sub_categories.find_or_create_by name: "Fitness", on_menu: true
    # esp.sub_categories.find_or_create_by name: "Futebol", on_menu: true
    # esp.sub_categories.find_or_create_by name: "Camping", on_menu: true
  supl = esporte.categories.find_or_create_by name: "Suplementos Alimentares", on_menu: true
    # supl.sub_categories.find_or_create_by name: "Proteínas", on_menu: true
    # supl.sub_categories.find_or_create_by name: "Suplementos Energéticos", on_menu: true
    # supl.sub_categories.find_or_create_by name: "Vitaminas e Fitoterápicos", on_menu: true
  saude = esporte.categories.find_or_create_by name: "Saúde", on_menu: true
    # saude.sub_categories.find_or_create_by name: "Balanças", on_menu: true
    # saude.sub_categories.find_or_create_by name: "Massageadores", on_menu: true
    # saude.sub_categories.find_or_create_by name: "Medidores de Pressão", on_menu: true

# --
veiculo = Manager::Ad::Department.find_or_create_by(name: 'Veículos', on_menu: true, raw_order: 5)
  novos = veiculo.categories.find_or_create_by name: "Novos", on_menu: true
    # novos.sub_categories.find_or_create_by name: "Carros", on_menu: true
    # novos.sub_categories.find_or_create_by name: "Motos", on_menu: true
    # novos.sub_categories.find_or_create_by name: "Caminhões", on_menu: true
  seminovos = veiculo.categories.find_or_create_by name: "Seminovos", on_menu: true
    # seminovos.sub_categories.find_or_create_by name: "Carros", on_menu: true
    # seminovos.sub_categories.find_or_create_by name: "Motos", on_menu: true
    # seminovos.sub_categories.find_or_create_by name: "Caminhões", on_menu: true
  usados = veiculo.categories.find_or_create_by name: "Usados", on_menu: true
    # usados.sub_categories.find_or_create_by name: "Carros", on_menu: true
    # usados.sub_categories.find_or_create_by name: "Motos", on_menu: true
    # usados.sub_categories.find_or_create_by name: "Caminhões", on_menu: true
    # usados.sub_categories.find_or_create_by name: "ônibus", on_menu: true

# --
imoveis = Manager::Ad::Department.find_or_create_by(name: 'Imóveis', on_menu: true, raw_order: 6)
  venda = imoveis.categories.find_or_create_by name: "Venda", on_menu: true
    # venda.sub_categories.find_or_create_by name: "Casa", on_menu: true
    # venda.sub_categories.find_or_create_by name: "Apartamento", on_menu: true
    # venda.sub_categories.find_or_create_by name: "Sala Comercial", on_menu: true
    # venda.sub_categories.find_or_create_by name: "Terreno", on_menu: true
  aluguel = imoveis.categories.find_or_create_by name: "Aluguel", on_menu: true
    # aluguel.sub_categories.find_or_create_by name: "Casa", on_menu: true
    # aluguel.sub_categories.find_or_create_by name: "Apartamento", on_menu: true
    # aluguel.sub_categories.find_or_create_by name: "Sala Comercial", on_menu: true

# --
bebes = Manager::Ad::Department.find_or_create_by(name: 'Bebês e Brinquedos', on_menu: true, raw_order: 7)
  bebe = bebes.categories.find_or_create_by name: "Bebês", on_menu: true
    # bebe.sub_categories.find_or_create_by name: 'Carrinhos', on_menu: true
    # bebe.sub_categories.find_or_create_by name: 'Bebê Conforto', on_menu: true
    # bebe.sub_categories.find_or_create_by name: 'Andadores', on_menu: true
    # bebe.sub_categories.find_or_create_by name: 'Berços e Cercados', on_menu: true
    # bebe.sub_categories.find_or_create_by name: 'Banheiras', on_menu: true
  brinquedos = bebes.categories.find_or_create_by name: "Brinquedos", on_menu: true
    # brinquedos.sub_categories.find_or_create_by name: 'Carrinhos e Miniaturas', on_menu: true
    # brinquedos.sub_categories.find_or_create_by name: 'Bonecos', on_menu: true
    # brinquedos.sub_categories.find_or_create_by name: 'Bonecas', on_menu: true
    # brinquedos.sub_categories.find_or_create_by name: 'Jogos', on_menu: true
    # brinquedos.sub_categories.find_or_create_by name: 'Brinquedos em Geral', on_menu: true

# --
alimentos = Manager::Ad::Department.find_or_create_by(name: 'Alimentos e Bebidas', on_menu: true, raw_order: 8)
  alimentos.categories.find_or_create_by name: "Alimentos Básicos", on_menu: true
  alimentos.categories.find_or_create_by name: "Bebidas", on_menu: true
  alimentos.categories.find_or_create_by name: "Carnes", on_menu: true
  alimentos.categories.find_or_create_by name: "Padaria", on_menu: true
  alimentos.categories.find_or_create_by name: "Frios", on_menu: true
  alimentos.categories.find_or_create_by name: "Hortifruti", on_menu: true

# --
todos = Manager::Ad::Department.find_or_create_by(name: 'Todos os Departamentos', on_menu: true, raw_order: 9)
  todos.categories.find_or_create_by name: "Agropecuária", on_menu: true
  todos.categories.find_or_create_by name: "Comércio e Indústria", on_menu: true
  todos.categories.find_or_create_by name: "Ferramentas", on_menu: true
