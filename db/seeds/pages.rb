Apartment::Tenant.switch!('public')

# Geral
Manager::Page.find_or_create_by(title: "Home", slug: "home", description: "Descrição da página Home")
Manager::Page.find_or_create_by(title: "Departamento", slug: "departamento", description: "Descrição da página Departamento")
Manager::Page.find_or_create_by(title: "Categoria", slug: "categoria", description: "Descrição da página Categoria")
Manager::Page.find_or_create_by(title: "Sub-Categoria", slug: "sub-categoria", description: "Descrição da página Sub-Categoria")
Manager::Page.find_or_create_by(title: "Produto", slug: "produto", description: "Descrição da página Produto")

# Institucional
Manager::Page.find_or_create_by(title: "Sobre", slug: "sobre", description: "Descrição da página Sobre")
Manager::Page.find_or_create_by(title: "Atendimento", slug: "atendimento", description: "Descrição da página Atendimento")
Manager::Page.find_or_create_by(title: "Como Anunciar", slug: "como-anunciar", description: "Descrição da página Como Anunciar")
Manager::Page.find_or_create_by(title: "Política de Privacidade", slug: "politica-de-privacidade", description: "Descrição da página Política de Privacidade" )
Manager::Page.find_or_create_by(title: "Termos e Condições de Uso", slug: "termos-e-condicoes-de-uso", description: "Descrição da página Termos e Condições de Uso " )

#conheca
Manager::Page.find_or_create_by(title: "Conheça o Clictem", slug: "conheca", description: "Descrição da página Conheça o Clictem" )
Manager::Page.find_or_create_by(title: "O Que é?", slug: "o-que-e", description: "Descrição da página O Que é?" )
Manager::Page.find_or_create_by(title: "Recursos e Funcionalidades", slug: "funcionalidades", description: "Descrição da página Recursos e Funcionalidades" )
Manager::Page.find_or_create_by(title: "Como Funciona?", slug: "como-funciona", description: "Descrição da página Como Funciona?" )
Manager::Page.find_or_create_by(title: "Feito Para Consumidores", slug: "para-consumidores", description: "Descrição da página Feito Para Consumidores" )
Manager::Page.find_or_create_by(title: "Feito Para Empresas", slug: "para-empresas", description: "Descrição da página Feito Para Empresas" )
Manager::Page.find_or_create_by(title: "O Melhor do Marketing Digital", slug: "marketing-digital", description: "Descrição da página O Melhor do Marketing Digital" )
Manager::Page.find_or_create_by(title: "Relacionamento com Clientes", slug: "relacionamento", description: "Descrição da página Relacionamento com Clientes" )
Manager::Page.find_or_create_by(title: "Como Podemos lhe Ajudar?", slug: "como-podemos-ajudar", description: "Descrição da página Como Podemos lhe Ajudar?" )
Manager::Page.find_or_create_by(title: "Planos e Serviços", slug: "planos-e-servicos", description: "Descrição da página Planos e Serviços" )
Manager::Page.find_or_create_by(title: "O Funil de Vendas", slug: "funil-vendas", description: "Descrição da página O Funil de Vendas" )
