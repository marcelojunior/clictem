# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180103004357) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "fuzzystrmatch"
  enable_extension "pg_trgm"
  enable_extension "unaccent"

  create_table "admins", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["confirmation_token"], name: "index_admins_on_confirmation_token", unique: true, using: :btree
  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  add_index "admins", ["unlock_token"], name: "index_admins_on_unlock_token", unique: true, using: :btree

  create_table "banner_types", force: true do |t|
    t.string   "name"
    t.decimal  "value",       precision: 12, scale: 2, default: 0.0
    t.string   "size"
    t.boolean  "department"
    t.boolean  "geral"
    t.boolean  "home"
    t.string   "placeholder"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "banners", force: true do |t|
    t.string   "name"
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "client_name"
    t.string   "url"
    t.string   "contract"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image"
    t.string   "image_topo"
    t.string   "image_menu"
    t.string   "image_user"
    t.integer  "user_id"
    t.string   "banner_position"
    t.integer  "width"
    t.integer  "height"
    t.integer  "department_id"
    t.integer  "banner_type_id"
    t.decimal  "discount",        precision: 12, scale: 2, default: 0.0
    t.integer  "discount_type",                            default: 0
  end

  add_index "banners", ["user_id"], name: "index_banners_on_user_id", using: :btree

  create_table "bills", force: true do |t|
    t.integer  "user_id"
    t.decimal  "value",         precision: 12, scale: 2, default: 0.0
    t.integer  "order_id"
    t.integer  "sms_buy_id"
    t.decimal  "commission",    precision: 12, scale: 2, default: 0.0
    t.date     "due"
    t.boolean  "paid",                                   default: true
    t.boolean  "boolean",                                default: true
    t.string   "note"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "discount",      precision: 12, scale: 2, default: 0.0
    t.integer  "discount_type",                          default: 0
  end

  add_index "bills", ["order_id"], name: "index_bills_on_order_id", using: :btree
  add_index "bills", ["sms_buy_id"], name: "index_bills_on_sms_buy_id", using: :btree
  add_index "bills", ["user_id"], name: "index_bills_on_user_id", using: :btree

  create_table "blog_authors", force: true do |t|
    t.string   "name"
    t.text     "bio"
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.string   "title"
    t.string   "subtitle"
  end

  create_table "blog_posts", force: true do |t|
    t.integer  "department_id"
    t.string   "title"
    t.text     "text"
    t.string   "tags"
    t.string   "image1"
    t.string   "image_fb"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "author_id"
    t.string   "subtitle"
    t.datetime "published_at"
  end

  add_index "blog_posts", ["author_id"], name: "index_blog_posts_on_author_id", using: :btree
  add_index "blog_posts", ["department_id"], name: "index_blog_posts_on_department_id", using: :btree

  create_table "cities", force: true do |t|
    t.integer  "state_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "comments", force: true do |t|
    t.integer  "user_id"
    t.text     "text"
    t.integer  "product_id"
    t.integer  "question_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "deleted",     default: false
    t.integer  "seller_id"
  end

  add_index "comments", ["product_id"], name: "index_comments_on_product_id", using: :btree
  add_index "comments", ["question_id"], name: "index_comments_on_question_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "customers", force: true do |t|
    t.integer  "user_id"
    t.integer  "owner"
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "cellphone"
    t.date     "birthday"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",     default: true
    t.string   "company"
    t.string   "genre"
  end

  add_index "customers", ["user_id"], name: "index_customers_on_user_id", using: :btree

  create_table "customers_groups", id: false, force: true do |t|
    t.integer "customer_id"
    t.integer "group_id"
  end

  add_index "customers_groups", ["customer_id", "group_id"], name: "customers_groups_index", unique: true, using: :btree

  create_table "customers_sms_campaigns", id: false, force: true do |t|
    t.integer "campaign_id"
    t.integer "customer_id"
  end

  add_index "customers_sms_campaigns", ["campaign_id", "customer_id"], name: "index_customers_sms_campaigns_on_campaign_id_and_customer_id", using: :btree
  add_index "customers_sms_campaigns", ["customer_id"], name: "index_customers_sms_campaigns_on_customer_id", using: :btree

  create_table "departments_users_premium", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "department_id"
  end

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "groups", force: true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "funnel",     default: false
    t.integer  "level",      default: 0
  end

  add_index "groups", ["user_id"], name: "index_groups_on_user_id", using: :btree

  create_table "groups_sms_campaigns", id: false, force: true do |t|
    t.integer "group_id"
    t.integer "campaign_id"
  end

  add_index "groups_sms_campaigns", ["campaign_id", "group_id"], name: "groups_sms_campaigns_index", unique: true, using: :btree

  create_table "inboxes", force: true do |t|
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "manager_ad_brands", force: true do |t|
    t.string   "name"
    t.integer  "department_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.string   "keywords"
    t.string   "description"
  end

  add_index "manager_ad_brands", ["department_id"], name: "index_manager_ad_brands_on_department_id", using: :btree
  add_index "manager_ad_brands", ["slug"], name: "index_manager_ad_brands_on_slug", unique: true, using: :btree

  create_table "manager_ad_brands_departments", id: false, force: true do |t|
    t.integer "brand_id"
    t.integer "department_id"
  end

  add_index "manager_ad_brands_departments", ["brand_id"], name: "index_manager_ad_brands_departments_on_brand_id", using: :btree
  add_index "manager_ad_brands_departments", ["department_id"], name: "index_manager_ad_brands_departments_on_department_id", using: :btree

  create_table "manager_ad_campaigns", force: true do |t|
    t.integer  "user_id"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "themed_campaign_id"
  end

  add_index "manager_ad_campaigns", ["product_id"], name: "index_manager_ad_campaigns_on_product_id", using: :btree
  add_index "manager_ad_campaigns", ["user_id"], name: "index_manager_ad_campaigns_on_user_id", using: :btree

  create_table "manager_ad_categories", force: true do |t|
    t.string   "name"
    t.integer  "department_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "on_menu",        default: false
    t.integer  "raw_order"
    t.string   "slug"
    t.string   "keywords"
    t.string   "description"
    t.integer  "products_count", default: 0,     null: false
  end

  add_index "manager_ad_categories", ["department_id"], name: "index_manager_ad_categories_on_department_id", using: :btree
  add_index "manager_ad_categories", ["slug"], name: "index_manager_ad_categories_on_slug", unique: true, using: :btree

  create_table "manager_ad_cupoms", force: true do |t|
    t.integer  "user_id"
    t.integer  "product_id"
    t.integer  "seller_id"
    t.string   "code"
    t.date     "due"
    t.decimal  "price",       precision: 12, scale: 2, default: 0.0
    t.decimal  "offer_price", precision: 12, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "used"
    t.integer  "offer_id"
  end

  add_index "manager_ad_cupoms", ["product_id"], name: "index_manager_ad_cupoms_on_product_id", using: :btree
  add_index "manager_ad_cupoms", ["user_id"], name: "index_manager_ad_cupoms_on_user_id", using: :btree

  create_table "manager_ad_departments", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.boolean  "on_menu",                                 default: false
    t.integer  "raw_order"
    t.string   "keywords"
    t.string   "description"
    t.string   "icon"
    t.integer  "products_count",                          default: 0,     null: false
    t.decimal  "banner_value",   precision: 12, scale: 2, default: 0.0
  end

  add_index "manager_ad_departments", ["slug"], name: "index_manager_ad_departments_on_slug", unique: true, using: :btree

  create_table "manager_ad_departments_users", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "department_id"
  end

  create_table "manager_ad_image_temps", force: true do |t|
    t.string   "token"
    t.text     "base64"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "manager_ad_images", force: true do |t|
    t.integer  "product_id"
    t.string   "file"
    t.text     "base64"
    t.boolean  "highlight",  default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "waiting",    default: true
    t.string   "token"
  end

  add_index "manager_ad_images", ["product_id"], name: "index_manager_ad_images_on_product_id", using: :btree

  create_table "manager_ad_info_clicks", force: true do |t|
    t.integer  "product_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "seller_id"
  end

  add_index "manager_ad_info_clicks", ["product_id"], name: "index_manager_ad_info_clicks_on_product_id", using: :btree
  add_index "manager_ad_info_clicks", ["seller_id"], name: "index_manager_ad_info_clicks_on_seller_id", using: :btree
  add_index "manager_ad_info_clicks", ["user_id"], name: "index_manager_ad_info_clicks_on_user_id", using: :btree

  create_table "manager_ad_interests", force: true do |t|
    t.integer  "product_id"
    t.integer  "user_id"
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "cellphone"
    t.text     "note"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "manager_ad_interests", ["product_id"], name: "index_manager_ad_interests_on_product_id", using: :btree
  add_index "manager_ad_interests", ["user_id"], name: "index_manager_ad_interests_on_user_id", using: :btree

  create_table "manager_ad_offer_campaigns", force: true do |t|
    t.integer  "user_id"
    t.string   "title"
    t.date     "date_start"
    t.date     "date_end"
    t.boolean  "wday0",      default: true
    t.boolean  "wday1",      default: true
    t.boolean  "wday2",      default: true
    t.boolean  "wday3",      default: true
    t.boolean  "wday4",      default: true
    t.boolean  "wday5",      default: true
    t.boolean  "wday6",      default: true
    t.boolean  "has_cupom",  default: false
    t.boolean  "repeat",     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "manager_ad_offer_campaigns", ["user_id"], name: "index_manager_ad_offer_campaigns_on_user_id", using: :btree

  create_table "manager_ad_offers", force: true do |t|
    t.integer  "product_id"
    t.integer  "user_id"
    t.date     "date_start"
    t.date     "date_end"
    t.boolean  "wday0",                                      default: true
    t.boolean  "wday1",                                      default: true
    t.boolean  "wday2",                                      default: true
    t.boolean  "wday3",                                      default: true
    t.boolean  "wday4",                                      default: true
    t.boolean  "wday5",                                      default: true
    t.boolean  "wday6",                                      default: true
    t.boolean  "has_cupom",                                  default: false
    t.boolean  "repeat",                                     default: false
    t.decimal  "price",             precision: 12, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "offer_campaign_id"
    t.integer  "cupom_limit",                                default: 0
    t.integer  "cupom_generated",                            default: 0
    t.boolean  "has_cupom_limit",                            default: false
    t.string   "cupom_note"
  end

  add_index "manager_ad_offers", ["product_id"], name: "index_manager_ad_offers_on_product_id", using: :btree
  add_index "manager_ad_offers", ["user_id"], name: "index_manager_ad_offers_on_user_id", using: :btree

  create_table "manager_ad_products", force: true do |t|
    t.string   "name"
    t.integer  "department_id"
    t.integer  "sub_category_id"
    t.string   "image1"
    t.string   "image2"
    t.string   "image3"
    t.string   "image4"
    t.string   "code"
    t.decimal  "price",               precision: 12, scale: 2, default: 0.0
    t.decimal  "offer_price",         precision: 12, scale: 2, default: 0.0
    t.boolean  "on_request"
    t.boolean  "is_old"
    t.text     "description"
    t.text     "payment_description"
    t.integer  "views_count",                                  default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_service",                                   default: false
    t.integer  "user_id"
    t.date     "offer_start"
    t.string   "slug"
    t.integer  "brand_id"
    t.boolean  "published",                                    default: false
    t.string   "brand_sugest"
    t.integer  "category_id"
    t.integer  "interest_count",                               default: 0
    t.boolean  "sold",                                         default: false
    t.integer  "raw_order",                                    default: 0
    t.boolean  "from_only",                                    default: false
    t.date     "offer_end"
    t.boolean  "is_offer",                                     default: false
    t.integer  "state_id"
    t.integer  "city_id"
    t.text     "script"
    t.datetime "deleted_at"
    t.boolean  "has_cupom",                                    default: false
    t.integer  "cupom_limit",                                  default: 0
    t.integer  "cupom_generated",                              default: 0
    t.boolean  "has_cupom_limit",                              default: false
    t.string   "cupom_note"
    t.integer  "current_offer_id"
    t.boolean  "has_interest",                                 default: false
    t.string   "interest_title"
    t.text     "interest_text"
  end

  add_index "manager_ad_products", ["brand_id"], name: "index_manager_ad_products_on_brand_id", using: :btree
  add_index "manager_ad_products", ["category_id"], name: "index_manager_ad_products_on_category_id", using: :btree
  add_index "manager_ad_products", ["city_id"], name: "index_manager_ad_products_on_city_id", using: :btree
  add_index "manager_ad_products", ["deleted_at"], name: "index_manager_ad_products_on_deleted_at", using: :btree
  add_index "manager_ad_products", ["department_id"], name: "index_manager_ad_products_on_department_id", using: :btree
  add_index "manager_ad_products", ["slug", "id"], name: "index_manager_ad_products_on_slug_and_id", unique: true, using: :btree
  add_index "manager_ad_products", ["state_id"], name: "index_manager_ad_products_on_state_id", using: :btree
  add_index "manager_ad_products", ["sub_category_id"], name: "index_manager_ad_products_on_sub_category_id", using: :btree
  add_index "manager_ad_products", ["user_id"], name: "index_manager_ad_products_on_user_id", using: :btree

  create_table "manager_ad_sub_categories", force: true do |t|
    t.string   "name"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "on_menu",        default: false
    t.integer  "raw_order"
    t.string   "slug"
    t.string   "keywords"
    t.string   "description"
    t.integer  "products_count", default: 0,     null: false
  end

  add_index "manager_ad_sub_categories", ["category_id"], name: "index_manager_ad_sub_categories_on_category_id", using: :btree
  add_index "manager_ad_sub_categories", ["slug"], name: "index_manager_ad_sub_categories_on_slug", unique: true, using: :btree

  create_table "manager_ad_themed_campaigns", force: true do |t|
    t.string   "title"
    t.string   "subdomain"
    t.text     "description"
    t.string   "color1",                               default: "#f17420"
    t.string   "color2",                               default: "#16556a"
    t.string   "color3",                               default: "#898b8d"
    t.string   "img_bg"
    t.string   "img_1"
    t.string   "img_2"
    t.string   "img_3"
    t.string   "img_4"
    t.string   "img_5"
    t.string   "img_6"
    t.date     "date_start"
    t.date     "date_end"
    t.boolean  "active",                               default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "img_7"
    t.boolean  "hidden",                               default: false
    t.decimal  "value",       precision: 12, scale: 2, default: 0.0
  end

  create_table "manager_ad_themed_campaigns_users", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "themed_campaign_id"
    t.boolean "pago",               default: false
  end

  create_table "notifications", force: true do |t|
    t.integer  "user_id"
    t.string   "subject"
    t.text     "body"
    t.boolean  "view",       default: false
    t.string   "to"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "order_checklists", force: true do |t|
    t.integer  "order_id"
    t.string   "name"
    t.boolean  "done",       default: false
    t.string   "resp"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "raw_order"
  end

  add_index "order_checklists", ["order_id"], name: "index_order_checklists_on_order_id", using: :btree

  create_table "orders", force: true do |t|
    t.integer  "user_id"
    t.decimal  "value",           precision: 12, scale: 2, default: 0.0
    t.integer  "installment"
    t.text     "note"
    t.boolean  "done",                                     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "kind",                                     default: "Plano"
    t.date     "date_start"
    t.date     "date_end"
    t.string   "banner_position"
    t.string   "plan"
    t.integer  "banner_type_id"
  end

  create_table "organizations", force: true do |t|
    t.string   "name"
    t.string   "fullname"
    t.string   "color1"
    t.string   "color2"
    t.string   "color3"
    t.string   "facebook"
    t.string   "phone"
    t.string   "email"
    t.string   "email_comercial"
    t.string   "domain"
    t.boolean  "active"
    t.string   "business_name"
    t.string   "cnpj"
    t.string   "address_street"
    t.string   "address_number"
    t.string   "address_additional"
    t.string   "address_neighborhood"
    t.integer  "city_id"
    t.integer  "state_id"
    t.string   "zip_code"
    t.string   "title_site"
    t.string   "ti_email"
    t.string   "mkt_email"
    t.string   "fin_email"
    t.text     "order_plan_checklist"
    t.text     "order_banner_checklist"
    t.text     "google_maps"
    t.text     "about"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "commission_plan"
    t.float    "commission_banner"
    t.float    "commission_sms"
    t.float    "sms_unit_value"
    t.string   "fb_app_id"
    t.string   "fb_app_secret"
    t.string   "news_link"
  end

  add_index "organizations", ["city_id"], name: "index_organizations_on_city_id", using: :btree
  add_index "organizations", ["state_id"], name: "index_organizations_on_state_id", using: :btree

  create_table "pages", force: true do |t|
    t.string   "title"
    t.string   "description"
    t.string   "keywords"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
  end

  create_table "redactor_assets", force: true do |t|
    t.integer  "user_id"
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "redactor_assets", ["assetable_type", "assetable_id"], name: "idx_redactor_assetable", using: :btree
  add_index "redactor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_redactor_assetable_type", using: :btree
  add_index "redactor_assets", ["user_id"], name: "index_redactor_assets_on_user_id", using: :btree

  create_table "sms_buys", force: true do |t|
    t.integer  "user_id"
    t.integer  "qtd"
    t.boolean  "active",                              default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "value",      precision: 12, scale: 2, default: 0.0
  end

  add_index "sms_buys", ["user_id"], name: "index_sms_buys_on_user_id", using: :btree

  create_table "sms_campaigns", force: true do |t|
    t.integer  "user_id"
    t.text     "message"
    t.string   "link"
    t.integer  "contacts_count"
    t.date     "schedule_date"
    t.time     "schedule_time"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status",         default: "avaliando"
    t.boolean  "birthday",       default: false
  end

  create_table "sms_messages", force: true do |t|
    t.integer  "campaign_id"
    t.integer  "user_id"
    t.integer  "contact_id"
    t.string   "cellphone"
    t.text     "text"
    t.string   "status",         default: "aguardando"
    t.text     "balance_origin", default: "franchise"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sms_messages", ["campaign_id"], name: "index_sms_messages_on_campaign_id", using: :btree
  add_index "sms_messages", ["contact_id"], name: "index_sms_messages_on_contact_id", using: :btree
  add_index "sms_messages", ["user_id"], name: "index_sms_messages_on_user_id", using: :btree

  create_table "sms_responses", force: true do |t|
    t.integer  "message_id"
    t.integer  "customer_id"
    t.text     "text"
    t.string   "uniqid"
    t.integer  "user_id"
    t.integer  "campaign_id"
    t.boolean  "read",        default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "states", force: true do |t|
    t.string   "symbol"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                                              default: "",         null: false
    t.string   "encrypted_password",                                 default: "",         null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                      default: 0,          null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.integer  "roles_mask"
    t.string   "last_name"
    t.string   "business_name"
    t.string   "trade_name"
    t.string   "cpf"
    t.string   "cnpj"
    t.string   "ie"
    t.string   "genre"
    t.date     "birthday"
    t.string   "phone"
    t.string   "cellphone"
    t.boolean  "want_email",                                         default: true
    t.boolean  "want_sms",                                           default: true
    t.string   "address_street"
    t.string   "address_number"
    t.string   "address_district"
    t.string   "address_compl"
    t.string   "address_zipcode"
    t.integer  "city_id"
    t.integer  "state_id"
    t.string   "resp_name"
    t.date     "resp_birthday"
    t.string   "resp_cpf"
    t.string   "resp_email"
    t.string   "subdomain"
    t.string   "plan",                                               default: "free"
    t.integer  "ad_limit",                                           default: 25
    t.string   "provider"
    t.string   "uid"
    t.string   "kind",                                               default: "pf"
    t.string   "website"
    t.integer  "products_count"
    t.string   "logo"
    t.string   "fb_token"
    t.string   "origin_subdomain"
    t.string   "bg_image"
    t.boolean  "bg_repeat",                                          default: false
    t.string   "color1"
    t.string   "color2"
    t.string   "color3"
    t.string   "stamp"
    t.text     "description"
    t.text     "keywords"
    t.integer  "mall_id"
    t.boolean  "is_mall",                                            default: false
    t.string   "area"
    t.boolean  "show_info",                                          default: true
    t.integer  "sms_franchise_balance",                              default: 0
    t.integer  "sms_franchise_total",                                default: 0
    t.integer  "sms_hired_balance",                                  default: 0
    t.date     "sms_hired_due"
    t.boolean  "sms_birthday",                                       default: false
    t.text     "sms_birthday_message"
    t.string   "resp2_name"
    t.string   "resp2_email"
    t.boolean  "has_sms",                                            default: true
    t.string   "phone2"
    t.text     "script"
    t.boolean  "has_cupoms",                                         default: true
    t.integer  "offer_campaign_limit",                               default: 0
    t.integer  "offer_cupoms_limit",                                 default: 0
    t.date     "contract_start"
    t.date     "contract_end"
    t.decimal  "contract_value",            precision: 12, scale: 2, default: 0.0
    t.boolean  "satisfaction",                                       default: true
    t.decimal  "contract_monthly",          precision: 12, scale: 2, default: 0.0
    t.integer  "contract_period",                                    default: 12
    t.text     "note"
    t.string   "slug"
    t.boolean  "seller",                                             default: false
    t.string   "resp3_name"
    t.string   "resp3_email"
    t.boolean  "active",                                             default: true
    t.integer  "priority",                                           default: 0
    t.datetime "deleted_at"
    t.string   "associated_code"
    t.string   "deal_level",                                         default: "Stand By"
    t.text     "deal_note"
    t.text     "deal_suggest_plan_note"
    t.decimal  "deal_suggest_plan_value",   precision: 12, scale: 2, default: 0.0
    t.text     "deal_suggest_banner_note"
    t.decimal  "deal_suggest_banner_value", precision: 12, scale: 2, default: 0.0
    t.string   "resp_cellphone",                                     default: ""
    t.string   "resp2_cellphone",                                    default: ""
    t.string   "resp3_cellphone",                                    default: ""
    t.date     "cancel_date"
    t.decimal  "discount",                  precision: 12, scale: 2, default: 0.0
    t.integer  "discount_type",                                      default: 0
    t.boolean  "premium",                                            default: false
  end

  add_index "users", ["city_id"], name: "index_users_on_city_id", using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["deleted_at"], name: "index_users_on_deleted_at", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["mall_id"], name: "index_users_on_mall_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["state_id"], name: "index_users_on_state_id", using: :btree

  create_table "versions", force: true do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  create_table "visits", force: true do |t|
    t.integer  "ad"
    t.integer  "category_id"
    t.integer  "department_id"
    t.integer  "owner"
    t.integer  "banner_id"
    t.integer  "visitor"
    t.string   "cluster"
    t.text     "product"
    t.string   "age"
    t.text     "interest"
    t.text     "brand"
    t.text     "career"
    t.string   "gender"
    t.string   "income"
    t.string   "ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "post"
    t.boolean  "ad_deleted",    default: false
  end

  add_index "visits", ["banner_id"], name: "index_visits_on_banner_id", using: :btree
  add_index "visits", ["category_id"], name: "index_visits_on_category_id", using: :btree
  add_index "visits", ["department_id"], name: "index_visits_on_department_id", using: :btree

end
