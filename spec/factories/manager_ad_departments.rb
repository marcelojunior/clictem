# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :manager_ad_department, :class => 'Manager::Ad::Department' do
    name "Moda e Beleza"
    on_menu true
    raw_order 1
    description "Moda e Beleza tem produtos de moda e beleza"
  end
end
