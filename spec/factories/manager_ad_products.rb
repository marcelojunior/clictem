# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :manager_ad_product, :class => 'Manager::Ad::Product' do
    name "MyString"
    department nil
    category nil
    image1 "MyString"
    image2 "MyString"
    image3 "MyString"
    image4 "MyString"
    code "MyString"
    price "9.99"
    offer_price "9.99"
    on_request false
    is_old false
    description "MyText"
    payment_description "MyString"
    views_count 1
  end
end
