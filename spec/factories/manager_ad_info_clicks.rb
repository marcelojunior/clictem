# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :manager_ad_info_click, :class => 'Manager::Ad::InfoClick' do
    product nil
    user nil
  end
end
