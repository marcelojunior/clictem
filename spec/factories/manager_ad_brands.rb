# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :manager_ad_brand, :class => 'Manager::Ad::Brand' do
    name "MyString"
    name_url "MyString"
    department nil
  end
end
