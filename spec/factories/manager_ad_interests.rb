# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :manager_ad_interest, :class => 'Manager::Ad::Interest' do
    product nil
    user nil
    name "MyString"
    email "MyString"
    phone "MyString"
    cellphone "MyString"
    note "MyText"
  end
end
