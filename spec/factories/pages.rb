# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :page, :class => 'Pages' do
    title "MyString"
    description "MyString"
    keywords "MyString"
    content "MyText"
  end
end
