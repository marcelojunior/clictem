# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :manager_ad_sub_category, :class => 'Manager::Ad::SubCategory' do
    name "MyString"
    name_url "MyString"
    category nil
  end
end
