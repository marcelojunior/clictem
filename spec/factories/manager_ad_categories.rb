# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :manager_ad_category, :class => 'Manager::Ad::Category' do
    name "MyString"
    name_url "MyString"
    parent_id 1
    department nil
  end
end
