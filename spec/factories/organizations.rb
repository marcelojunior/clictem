FactoryGirl.define do
  factory :organization do
    name "MyString"
fullname "MyString"
color1 "MyString"
color2 "MyString"
color3 "MyString"
facebook "MyString"
phone "MyString"
email "MyString"
email_comercial "MyString"
domain "MyString"
active false
business_name "MyString"
cnpj "MyString"
address_street "MyString"
address_number "MyString"
address_additional "MyString"
address_neighborhood "MyString"
city nil
state nil
zip_code "MyString"
title_site "MyString"
ti_email "MyString"
mkt_email "MyString"
fin_email "MyString"
order_plan_checklist "MyText"
order_banner_checklist "MyText"
google_maps "MyText"
about "MyText"
  end

end
