FactoryGirl.define do
  factory :themed_campaign do
    title "MyString"
slug "MyString"
description "MyText"
color1 "MyString"
color2 "MyString"
color3 "MyString"
img_bg "MyString"
img_1 "MyString"
img_2 "MyString"
img_3 "MyString"
img_4 "MyString"
img_5 "MyString"
img_6 "MyString"
date_start "2015-04-13"
date_end "2015-04-13"
active false
  end

end
