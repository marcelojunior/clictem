require 'rails_helper'

RSpec.describe Manager::Ad::Department, :type => :model do
  before do
    @dpto = build(:manager_ad_department)
  end

  subject { @dpto }

  it { should respond_to(:name) }
  it { should respond_to(:slug) }
  it { should be_valid }


  describe "when name is not present" do
    before { @dpto.name = " " }
    it { should_not be_valid }
  end

  describe "when name is already taken" do
    before do
      dpto_with_same_name = @dpto.dup
      dpto_with_same_name.name = @dpto.name.upcase
      dpto_with_same_name.save
    end

    it { should_not be_valid }
  end
end
