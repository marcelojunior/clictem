require 'rails_helper'

RSpec.describe Manager::Page, :type => :model do
  before do
    @page = Manager::Page.new(title: "Home")
  end

  subject { @page }

  it { should respond_to(:title) }
  it { should be_valid }

  describe "when title is not present" do
    before { @page.title = " " }
    it { should_not be_valid }
  end

  describe "when title is already taken" do
    before do
      page_with_same_title = @page.dup
      page_with_same_title.title = @page.title.upcase
      page_with_same_title.save
    end

    it { should_not be_valid }
  end
end
