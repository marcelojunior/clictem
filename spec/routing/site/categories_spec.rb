require 'rails_helper'

RSpec.describe "routing to categories", :type => :routing do

  departments = %w(casa-e-decoracao informatica moda esporte-e-saude veiculos imoveis bebes-e-brinquedos alimentos-e-bebidas beleza-e-perfumaria eletronicos agropecuaria diversos equipamentos-comerciais ferramentas literatura)

  departments.each do |dpto|
    it "rota dos departamentos /#{dpto} to site/products#index" do
      expect(:get => "#{dpto}")
        .to route_to("site/products#index", :department => "#{dpto}" )
    end
  end

  it "rota das categorias /moda-e-beleza/categoria/shampoo to site/products#index" do
    expect(:get => 'moda-e-beleza/categoria/shampoo')
      .to route_to("site/products#index",
            department: 'moda-e-beleza',
            category: 'shampoo'
          )
  end

  it "rota das subcategorias /moda-e-beleza/categoria/shampoo/feminino to site/products#index" do
    expect(:get => 'moda-e-beleza/categoria/shampoo/feminino')
      .to route_to("site/products#index",
            department: 'moda-e-beleza',
            category: 'shampoo',
            subcategory: 'feminino'
          )
  end

  it "rota das ofertas /moda-e-beleza/oferta/beleza/nome-do-produto' to site/products#show" do
    expect(:get => 'moda-e-beleza/oferta/beleza/nome-do-produto')
      .to route_to("site/products#show",
            department: 'moda-e-beleza',
            category: 'beleza',
            product: 'nome-do-produto'
          )
  end

  # it "rota dos anuncios /moda-e-beleza/servico/shampoo/nome-do-servico' to site/products#show" do
  #   expect(:get => 'moda-e-beleza/servico/massagem/nome-do-servico').to route_to("site/products#show",
  #                                                                         department: 'moda-e-beleza',
  #                                                                         category: 'massagem',
  #                                                                         product: 'nome-do-servico'
  #                                                                       )
  # end

  # it "rota dos anuncios /moda-e-beleza/produto/shampoo/nome-do-produto' to site/products#show" do
  #   expect(:get => 'moda-e-beleza/produto/beleza/nome-do-produto').to route_to("site/products#show",
  #                                                                         department: 'moda-e-beleza',
  #                                                                         category: 'shampoo',
  #                                                                         product: 'nome-do-produto'
  #                                                                       )
  # end

  it "nao deve rotear se nao tiver departamento ou estiver errado" do
    expect(:get => "/moda").not_to be_routable
  end
end
