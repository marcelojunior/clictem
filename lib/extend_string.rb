#encoding: utf-8
class String

  # Remover acentos.
  def removeaccents
    text = self
    text = text.gsub(/(á|à|ã|â|ä)/, 'a').gsub(/(é|è|ê|ë)/, 'e').gsub(/(í|ì|î|ï)/, 'i').gsub(/(ó|ò|õ|ô|ö)/, 'o').gsub(/(ú|ù|û|ü)/, 'u')
    text = text.gsub(/(Á|À|Ã|Â|Ä)/, 'A').gsub(/(É|È|Ê|Ë)/, 'E').gsub(/(Í|Ì|Î|Ï)/, 'I').gsub(/(Ó|Ò|Õ|Ô|Ö)/, 'O').gsub(/(Ú|Ù|Û|Ü)/, 'U')
    text = text.gsub(/ñ/, 'n').gsub(/Ñ/, 'N')
    text = text.gsub(/ç/, 'c').gsub(/Ç/, 'C')
    text
  end

  # Converter de string para dinheiro
  def to_money
    if self != nil
      if I18n.t('number.format.separator') == ','
        self.gsub('.', '').gsub(',', '.').to_f
      else
        self.gsub(',', '').to_f
      end
    end
  end




  # Formata o número de telefone
  def pretty_phone
    phone = self
    phone = phone.gsub(/\)(\d)/, ') \1')
    pos   = phone.size - 4
    phone[pos] = "-#{phone[pos]}"
    phone
  end

  # Formata número de telefone com DDD
  def pretty_phone_with_ddd
    phone = self.only_numbers
    phone[0] != '('
    phone[0] = "(#{phone[0]}"
    phone[3] = ") #{phone[3]}"
    phone[7] = "#{phone[7]}-"
    phone
  end




  # Extrai o DDD de um telefone
  def phone_ddd
    if self.scan( /\(\d{2}\)/ ).first
      self.scan( /\(\d{2}\)/ ).first.gsub(/(\(|\))/, '')
    else
      nil
    end
  end


  # Extrai o número de um telefone
  def phone_number
    self.gsub( /\(\d{2}\)/ , '').gsub(/-/, '')
  end



  # Exibe somente números de CNPJ e CPF
  def document_only_numbers
    self.gsub(/[\.\/\-\(\)]/, '')
  end


  # Retorna somente os números do telefone
  def only_numbers
    self.gsub(/(–|-|\.|\s|\(|\)|\[|\]|[a-zA-Z])*/, '')
  end

end
