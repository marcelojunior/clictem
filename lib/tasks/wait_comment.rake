#encoding: utf-8

task  :wait_comment => :environment  do |t,args|
    if Ec2RakeTaks.valid?
        User.all.each do |user|
            comments = []
            date      = Date.today - 3.days

            user.comments.where(created_at: (date.beginning_of_day..date.end_of_day) ).each do |comment|
                if comment.anwsers.count == 0
                    comments << comment
                end
            end

            if comments.size > 0
                email = Mails::UserMailer.wait_comment(user, comments).deliver
                user.notifications.new.save_email(email)
            end
        end
    end
end
