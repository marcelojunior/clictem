#encoding: utf-8

task  :user_miss => :environment  do |t,args|
    if Ec2RakeTaks.valid?
        date = Date.today - 7.days
        User.where(last_sign_in_at: (date.beginning_of_day..date.end_of_day)).each do |user|
            email = Mails::UserMailer.user_miss(user).deliver
            user.notifications.new.save_email(email)
        end
    end
end
