#encoding: utf-8

# - Lista de usuários que não tem produto cadastrado
# - Lista de usuários grátis PF com produtos cadastrados
# - Lista de usuários grátis PJ com produtos cadastrados

task  :gerar_perfilusuarios => :environment  do |t,args|
  require 'csv'
  if Ec2RakeTaks.valid?
    time_now_1 = Time.now.beginning_of_month
    time_now_2 = Time.now.end_of_month

    prods = Manager::Ad::Product.pluck(:user_id).uniq
    users = User.where('id not in (?)', prods)

    free_pf = User.where(id: prods, plan: 'free', kind: 'pf')
    free_pj = User.where(id: prods, plan: 'free', kind: 'pj')


    [['users', users], ['free_pf', free_pf], ['free_pj', free_pj]].each do |vars|
      tag      = vars[0]
      variable = vars[1]

      FileUtils.mkdir_p 'public/csv'
      file = "#{Rails.root}/public/csv/prefil_#{tag}.csv"

      CSV.open( file, 'w', col_sep: ';', encoding: "windows-1252" ) do |writer|
        writer << ['Nome','E-Mail', 'Fone', 'Cidade', 'Facebook', 'Anúncios']

        variable.each_with_index do |user, i|
          city  = ''
          city  = City.find(user.city_id).name if user.city_id
          facebook = (user.provider == 'facebook' ? 'Sim' : 'Não')
          ads = user.products.count
          writer << [user.fullname, user.email, user.phone, city, facebook, ads ]
        end
      end
    end
  end

end
