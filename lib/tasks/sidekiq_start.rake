desc "Start sidekiq daemon."
task :sidekiq_start do
    puts "Executando Sidekiq"
    sh "bundle exec sidekiq -d -C config/sidekiq.yml -e production"
end
