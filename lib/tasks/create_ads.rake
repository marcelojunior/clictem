#encoding: utf-8

task  :create_ads => :environment  do |t,args|
    if Ec2RakeTaks.valid?

        User.where(created_at: (Date.yesterday.beginning_of_day..Date.yesterday.end_of_day)).each do |user|
            if user.products.count < 2
                email = Mails::UserMailer.create_ads(user).deliver
                user.notifications.new.save_email(email)
            end
        end

    end
end
