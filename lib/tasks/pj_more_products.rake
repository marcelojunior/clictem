#encoding: utf-8

task  :pj_more_products => :environment  do |t,args|
    if Ec2RakeTaks.valid?
        if Date.today.wday == 2 # Verifica se é terça
            User.where(kind: 'pj', plan: ['basico', 'top']).each do |user|
                if user.products.count < 40
                    email = Mails::UserMailer.pj_more_products(user).deliver
                    user.notifications.new.save_email(email)
                end
            end
        end
    end
end
