class DepartmentsConstraint
  def self.matches?(request)
    departments = ["eletronicos", "informatica", "moda", "beleza-e-perfumaria", "casa-e-decoracao", "esporte-e-saude", "veiculos", "alimentos-e-bebidas", "diversos", "imoveis"]
    departments.include?(request.subdomains(0).first) and
    request.subdomains(0).first != "www" and
    DOMAINS.include?(request.subdomains(0).first) == false
  end
end
