class UsersConstraint
  def self.matches?(request)
    request.subdomains(0).first.present? and
    request.subdomains(0).first != "www" and
    DOMAINS.include?(request.subdomains(0).first) == false
  end
end
