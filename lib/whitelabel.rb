class Whitelabel
    attr_reader :configs

    def initialize(domain)
        domain = domain.split('.').first
        @configs = YAML.load_file("config/Whitelabel/#{domain}.yml")
    end

    # Leitura dos valores
    def read(attribute)
        @configs[attribute]
    end

end
