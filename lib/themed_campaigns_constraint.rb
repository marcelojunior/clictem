class ThemedCampaignsConstraint
  def self.matches?(request)
    campaigns = Manager::Ad::ThemedCampaign.pluck(:subdomain)
    campaigns.include?(request.subdomains(0).first) and
    request.subdomains(0).first != "www" and
    DOMAINS.include?(request.subdomains(0).first) == false
  end
end
