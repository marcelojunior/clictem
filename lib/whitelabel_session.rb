class WhitelabelSession

    def initialize(domain)
        unless sessions["whitelabel_#{domain}"]
            Whitelabel.new(domain)
        else
            sessions["whitelabel_#{domain}"]
        end
    end

end
