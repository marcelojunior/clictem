class Ec2RakeTaks
    require 'socket'

    def self.valid?
        instances = ENV['RAKE_INSTANCES'].split(',')
        if Rails.env.production?
            local = Socket.gethostname
            instances.include?(local)
        else
            true
        end
    end

end
