class SqsCheck

    def self.exists?(sqs_msg)
        hash = Digest::SHA1.hexdigest(sqs_msg.message_id)
        exists = true

        unless Rails.cache.dalli.get(hash)
            Rails.cache.dalli.set(hash, true)
            exists = false
        end

        exists
    end

end
