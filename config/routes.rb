require 'sidekiq/web'
require 'users_constraint'
require 'departments_constraint'
require 'themed_campaigns_constraint'

# Sobrescrevendo a classe para poder reutilizar o método .domain
class ActionDispatch::Routing::Mapper
  def domain(request)
    request.protocol + request.domain + (request.port.nil? ? '' : ":#{request.port}")
  end

  def current_domain(request)
    request.domain + (request.port.nil? ? '' : ":#{request.port}")
  end
end

Rails.application.routes.draw do

  # devise_for :admins

  if Rails.env.production?
    # Redirecionar .com para .com.br
    constraints(host: "clictem.com") do
      match "/(*path)" => redirect {|params, req|

        if req.subdomains(0).first.present? and
           req.subdomains(0).first != "www" and
           req.subdomains(0).first != "clictem" and
           req.subdomains(0).first != "lvh"
          subdomain = "#{req.subdomains(0).first}."
        else
          subdomain = ''
        end

        "http://#{subdomain}clictem.com.br/#{params[:path]}"},  via: [:get, :post]
    end
  end


  # Redirecionar www para non-www somente se for algum subdominio
  constraints(host: /www.(?!cdlxanxere|cdlxaxim|clictem|xanxeretem)/i) do
    match "/(*path)" => redirect {|params, req|

      if req.subdomains(0).first.present? and
         req.subdomains(0).first != "www" and
         DOMAINS.include?(req.subdomains(0).first) == false
        subdomain = "#{req.subdomains(0).first}."

      elsif req.subdomains(0).first == 'www' and
            DOMAINS.include?(req.subdomains(0)[1].first) == false
        subdomain = "#{req.subdomains(0)[1]}."

      else
        subdomain = ''
      end

      "http://#{subdomain}#{current_domain(req)}/#{params[:path]}"},  via: [:get, :post]
  end


  namespace :site do
    get 'visits/new'
    get 'brands', to: 'brands#index', as: :brands
    resources :comments
  end



  devise_for :users, :path => '',
    :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" },
    :path_names => {
      sign_in: 'login',
      sign_out: 'logout',
      sign_up: 'cadastre-se',
      confirmation: "confirme-seu-email",
      password: "alterar-senha"
    }




  # mount MailPreview => 'mail_view' if Rails.env.development?
  # mount RailsAdmin::Engine => '/manager-admin', as: 'rails_admin'


  # mount RedactorRails::Engine => '/redactor_rails'
  mount Sidekiq::Web, at: '/sidekiq-monitor'
  # mount UserImpersonate::Engine => "/impersonate", as: "impersonate_engine"
  get "/404", :to => "errors#not_found", as: :error_404
  get "/422", :to => "errors#unacceptable", as: :error_422
  get "/500", :to => "errors#internal_error", as: :error_500

  # config/routes.rb
  get 'switch_user' => 'switch_user#set_current_user'



  # Manager
  namespace :manager do
    get '/', to: 'users#associados', as: 'dashboard'

    resources :pages
    get '/users/clients', to: 'users#clients', as: :user_clients
    get '/users/associados', to: 'users#associados', as: :user_associados
    get '/users/prospection', to: 'users#prospection', as: :user_prospection
    resources :users
    resources :banners
    resources :banner_types
    resources :cupoms
    resources :themed_campaigns
    resources :orders
    resources :bills
    match "/change_password/:id" => "users#change_password", as: :change_password, via: :all

    namespace :ad do
      resources :departments
      resources :categories
      resources :brands
      resources :sub_categories
      resources :products
    end

    namespace :blog do
      resources :posts
      resources :authors
    end

    namespace :sms do
      resources :buys
      resources :campaigns
      resources :reports
      match 'contacts-import', to: 'contacts#import', as: :contacts_import, via: [:get, :post]
    end
  end



  namespace :api, defaults: {format: :json} do
    get 'departments', to: 'departments#index', as: :departments
    resources :products
    devise_scope :user do
      resource :session, only: [:create, :destroy]
    end
  end


  scope 'minha-conta' do

    get '/' => 'site/usuarios#resumo', as: :site_minha_conta
    get '/anuncios' => 'site/usuarios#anuncios', as: :site_anuncios

    # Rotas temporárias
    match "/usuarios/:action" => "site/usuarios#:action", as: :site_usuarios, via: :all
    match "/produto/:action" => "site/produto#:action", as: :site_produtos, via: :all
    match "/download-cupons/:id" => "site/produto#download_cupons", as: :site_download_cupons, via: :all
    match "/atrair/:action" => "site/atrair#:action", as: :site_atrair, via: :all
    match "/analisar/:action" => "site/analisar#:action", as: :site_analisar, via: :all

    get 'customers-group-count', to: 'customers#count', as: :customers_count
    get 'customers-xls', to: 'customers#export_excel', as: :customers_xls

    namespace :sms do
      resources :campaigns, path: 'campanhas'
      resources :buys, path: 'contratar'
      get 'index', as: :index
      put 'sms-update-user', to: 'buys#update_user', as: :update_user
      match 'aniversario', to: 'campaigns#birthday', as: :birthday, via: [:get, :put]
    end
    resources :groups, path: 'grupos'
    resources :customers, path: 'clientes'

    # Campanhas promocionais
    namespace :offers, path: 'ofertas' do
      resources :campaigns, path: 'campanhas'
    end

  end


  # Rotas Institucionais
  get "/sobre" => "site/page#sobre", as: :page_sobre
  get "/atendimento" => "site/page#atendimento", as: :page_atendimento
  get "/como-anunciar" => "site/page#como_anunciar", as: :page_como_anunciar
  get "/politica-de-privacidade" => "site/page#politica", as: :page_politica
  get "/termos-e-condicoes-de-uso" => "site/page#termos", as: :page_termos
  get "/produtos-e-servicos-ilegais" => "site/page#ilegais", as: :page_ilegais
  get "/acesso" => "site/page#acesso", as: :page_acesso
  get "/planos" => "site/page#planos", as: :page_planos
  match "/altera_plano" => "site/page#altera_plano", as: :page_altera_plano, via: :all
  get "/downloads" => "site/page#downloads", as: :page_downloads
  get "/links" => "site/page#links", as: :page_links
  get "/servicos" => "site/page#servicos", as: :page_servicos
  get "/spc/:cidade" => "site/page#spc", as: :page_spc



  # Blog
  # get '/post/:id', to: 'site/blog#show', as: :blog_post
  # get '/blog/:id', to: 'site/blog#index', as: :blog_posts

  # /blog/
  get '/blog', to: 'site/blog#authors', as: :blog
  # /blog/o-nome-do-blog
  get '/blog/:slug_blog', to: 'site/blog#index', as: :blog_author
  # /blog/conteudo/o-titulo-do-post
  get '/blog/conteudo/:slug_post', to: 'site/blog#show', as: :blog_post



  get "/busca" => "site/products#search", as: :site_search
  match "/send-interest" => "site/products#send_interest", as: :send_interest, via: :all
  get "/show-user-info" => "site/products#show_user_info", as: :show_user_info
  get "/produto-nao-encontrado" => "site/products#not_found", as: :product_not_found

  # enviar info do usuário
  get "/show-user-phone/:user_id/(:product_id)" => "site/products#show_user_phone", as: :show_user_phone, format: :json
  # post "/send-track"
  # psot ""

  get "sitemap" => "site/page#sitemap"


  get "/u/:user", to: redirect {|params, req|  "http://#{params[:user]}.#{req.host_with_port}" }#, :status => :moved_permanently

  # Contratação de plano
  match "/contratar" => "site/contratar#planos", as: :contratar_planos, via: :all
  match "/contratar-banners" => "site/contratar#banners", as: :contratar_banners, via: :all
  match "/contratar-dados" => "site/contratar#dados", as: :contratar_dados, via: :all


  resource :inbox, controller: 'inbox' , only: [:show,:create]



  # Rota de Departamentos com subdominio
  constraints DepartmentsConstraint do
    get '/', to: redirect {|params, req| "#{domain(req)}/#{req.subdomain}"}
    get "/:category", to: "site/products#index"
    # get "/servicos", to: "site/products#services", as: :services
    # get "/servico/:category/:product", to: "site/products#show", as: :service
  end

  # Rota para página temática
  constraints ThemedCampaignsConstraint  do
    get "/", to: "site/themed_campaigns#index", as: :themed_campaign
    post "/participar", to: "site/themed_campaigns#create", as: :themed_campaign_create
    get "/participar", to: "site/themed_campaigns#new", as: :themed_campaign_new
  end

  # Rota para lista de produtos do usuário
  constraints UsersConstraint  do
    get "/", to: "site/usuarios#produtos", as: :user_produtos
    get "/:category/:product", to: redirect {|params, req|  "#{domain(req)}/#{req.subdomain}/anuncio/#{params[:category]}/#{params[:product]}" }
  end

  root to: "site/home#index"



  # Ofertas
  get '/ofertas', to: "site/products#offers", as: :ofertas
  get '/cupons', to: "site/products#cupoms", as: :cupons



  # Rota de Departamentos com path
  scope ":department" do
    get '/(:servicos)', to: "site/products#index", as: :department
    get "/categoria/:category/(:servicos)", to: "site/products#index", as: :category
    get "/anuncio/:category/:product", to: "site/products#show", as: :ad
  end




  # get '*unmatched_route', :to => 'application#raise_not_found!'

end
