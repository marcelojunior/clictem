Rails.application.configure do
  config.action_dispatch.tld_length = 1
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false


  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # config.cache_store = :dalli_store, 'localhost:11211', { :compress => true }

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # ActionMailer Config
  config.action_mailer.default_url_options = { :host => 'lvh.me:3000' }
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = { address: "localhost", port: 1025 }
  # change to true to allow email to be sent during development
  config.action_mailer.perform_deliveries = true
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default :charset => "utf-8"

  config.action_mailer.asset_host = "http://localhost"

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true



  config.after_initialize do
    Bullet.enable = false
    # Bullet.alert = true
    Bullet.bullet_logger = true
    Bullet.console = true
    # Bullet.growl = true
    # Bullet.xmpp = { :account  => 'bullets_account@jabber.org',
                    # :password => 'bullets_password_for_jabber',
                    # :receiver => 'your_account@jabber.org',
                    # :show_online_status => true }
    Bullet.rails_logger = true
    # Bullet.bugsnag = true
    # Bullet.airbrake = true
    Bullet.add_footer = true
    # Bullet.stacktrace_includes = [ 'your_gem', 'your_middleware' ]
  end
end


# Devise.setup do |config|
#   config.omniauth :facebook, "770727769658195", "00805b2a3ffcb16244a59075c89e8d3d"
# end

# Devise.setup do |config|
#   config.omniauth :facebook, nil, nil, setup: true
# end
