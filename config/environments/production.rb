Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.
  config.action_dispatch.tld_length = 2

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Enable Rack::Cache to put a simple HTTP cache in front of your application
  # Add `rack-cache` to your Gemfile before enabling this.
  # For large-scale production use, consider using a caching reverse proxy like nginx, varnish or squid.
  # config.action_dispatch.rack_cache = true

  # Disable Rails's static asset server (Apache or nginx will already do this).
  # Serving static assets and setting cache headers
  # which will be used by cloudfront as well
  config.serve_static_files = false

  # Compress JavaScripts and CSS.
  # config.assets.js_compressor = :uglifier
  # config.assets.css_compressor = :sass

  # Compress JavaScripts and CSS.
  # Disable variable name manging to make AngularJS short dependency syntax work.
  config.assets.js_compressor = Uglifier.new(mangle: false)

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = false

  # Generate digests for assets URLs.
  config.assets.digest = true

  #Needs to be false on Heroku
  config.assets.initialize_on_precompile = false

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # Set to :debug to see everything in the log.
  config.log_level = :info
  # config.logger = Logger.new(STDOUT)
  # Prepend all log lines with the following tags.
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups.
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)
  # config.logger = Logger.new(config.paths["log"].first, 10, 1.megabytes)

  # Use a different cache store in production.
   config.cache_store = :mem_cache_store


  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  config.action_controller.asset_host = "http://#{ENV['AWS_S3_BUCKET']}.s3.amazonaws.com"
  config.action_mailer.asset_host = config.action_controller.asset_host
  # config.action_controller.asset_host = "d2dtx4vsn1wc1g.cloudfront.net"

  # Precompile additional assets.
  # application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
  # config.assets.precompile += Ckeditor.assets
  config.assets.precompile += %w(
                                 redactor-rails/*
                                 whitelabel/*
                                )

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false

 # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # ActionMailer Config
  # Setup for production - deliveries, no errors raised
  # config.action_mailer.default_url_options = { host: 'clictem.com.br' }
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.perform_deliveries = true
  config.action_mailer.raise_delivery_errors = false
  config.action_mailer.default :charset => "utf-8"

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Disable automatic flushing of the log to improve performance.
  # config.autoflush_log = false

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false


  options = {
      :enabled => true,
      :remove_multi_spaces => true,
      :remove_comments => true,
      :remove_intertag_spaces => true,
      :remove_quotes => false,
      :compress_css => false,
      :compress_javascript => false,
      :simple_doctype => false,
      :remove_script_attributes => false,
      :remove_style_attributes => false,
      :remove_link_attributes => false,
      :remove_form_attributes => false,
      :remove_input_attributes => false,
      :remove_javascript_protocol => false,
      :remove_http_protocol => false,
      :remove_https_protocol => false,
      :preserve_line_breaks => false,
      :simple_boolean_attributes => false
    }

  # endpoint    = ENV['MEMCACHED_URL']
  # elasticache = Dalli::ElastiCache.new(endpoint)

  # config.cache_store = :dalli_store, elasticache.servers, {:expires_in => 1.day, :compress => true}

  config.static_cache_control = "public, max-age=2592000"
end


# Clictem / anuncios.link
# Devise.setup do |config|
#   config.omniauth :facebook, "755322341198738", "70807dd54d8b77a7742bb2e71101e9b7"
# end


# CDL Xaxim
# Devise.setup do |config|
#   config.omniauth :facebook, "780208352092916", "b489aaeb73c3a8f02d0aa8bd814be2b0"
# end


# CDL Xanxerê
# Devise.setup do |config|
#   config.omniauth :facebook, "413801972144676", "6f2f3da8908408885c1a7e46eec02c2b"
# end
