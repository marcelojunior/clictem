# Upload direto para o S3
S3DirectUpload.config do |c|
  c.access_key_id = ENV['AWS_ACCESS_KEY_ID']
  c.secret_access_key = ENV['AWS_SECRET_ACCESS_KEY']
  c.bucket = ENV['AWS_S3_BUCKET']
  c.region = ENV['AWS_S3_REGION']
  c.url = "https://s3-sa-east-1.amazonaws.com/#{ENV['AWS_S3_BUCKET']}"
end
