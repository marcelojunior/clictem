DOMAINS = ['cdlxaxim', 'cdlxanxere', 'clictem', 'xanxeretem']

# current_org.domain = request.host.split('.')[-1 - ActionDispatch::Http::URL.tld_length]

WHITELABEL = true
WHITELABEL_NAME = 'cdlxanxere'
WHITELABEL_FULLNAME = 'CDL Xanxerê'
WHITELABEL_COLOR1 = '#4b8ede'
WHITELABEL_COLOR2 = '#0e59a5'
WHITELABEL_COLOR3 = '#6d6e71'
WHITELABEL_FB = 'cdlspc.xanxere'
WHITELABEL_FONE = '(49) 3433-1471'
WHITELABEL_EMAIL = 'cdl@xanxere.cdl-sc.org.br'
WHITELABEL_EMAIL_COMERCIAL = 'cdl@xanxere.cdl-sc.org.br'
WHITELABEL_LOGO  = 'whitelabel/logo_cdlxanxere.png'
# current_org.domain = (Rails.env.development? ? 'public.local:3000' : 'cdlxanxere.com.br')
WHITELABEL_BUSINESS_NAME = 'Camara de Dirigentes Logistas de Xanxerê'
WHITELABEL_CNPJ = '82.831.553/0001-50'
WHITELABEL_STREET = 'Av. Brasil'
WHITELABEL_NUMBER = '260'
WHITELABEL_COMPL = 'Sala 504'
WHITELABEL_NEIGHBORHOOD = 'Centro'
WHITELABEL_CITY = 'Xanxerê'
WHITELABEL_UF = 'SC'
WHITELABEL_CEP = '89820-000'
WHITELABEL_TITLE = 'CDL Xanxerê - Faça bons negócios!'
WHITELABEL_PLAN_0_VALUE = 19.9
WHITELABEL_PLAN_1_VALUE = 29.9
WHITELABEL_PLAN_2_VALUE = 15 # Parceiro de sucesso
WHITELABEL_PLAN_3_VALUE = 2.99 #Associado
WHITELABEL_PLAN_4_VALUE = 10 # Parceiro de Departamento
WHITELABEL_PLAN_0_SMS = 0.12
WHITELABEL_PLAN_1_SMS = 0
WHITELABEL_PLAN_2_SMS = 0
WHITELABEL_PLAN_3_SMS = 0
WHITELABEL_TI_EMAIL = 'Marcelo <marcelo.machado.jr@gmailcom>'
WHITELABEL_MKT_EMAIL = 'Clictem <clictem@clictem.com>'
WHITELABEL_FIN_EMAIL = 'Sandro Mattiolo <sm@mattiolo.com.br>, Fabio <fabio@moschetta.com.br>, CDL <cdl@xanxere.cdl-sc.org.br>, CDL <financeiro@xanxere.cdl-sc.org.br>'
WHITELABEL_ORDER_PLAN_CHECKLIST = [
    {name: 'Associar a CDL', resp: 'Financeiro', order: 1},
    {name: 'Solicitar artes do anunciante', resp: 'Marketing', order: 2},
    {name: 'Página - Domínio', resp: 'Marketing', order: 3},
    {name: 'Página - Cabeçalho', resp: 'Marketing', order: 4},
    {name: 'Página - Avatar', resp: 'Marketing', order: 5},
    {name: 'Página - Selo', resp: 'Marketing', order: 6},
    {name: 'Página - Cores da página', resp: 'Marketing', order: 7},
    {name: 'Avisar Anunciante', resp: 'Financeiro', order: 8}
]
WHITELABEL_ORDER_BANNER_CHECKLIST = [
    {name: 'Associar a CDL', resp: 'Financeiro', order: 1},
    {name: 'Solicitar artes do anunciante', resp: 'Marketing', order: 2},
    {name: 'Postar no site', resp: 'Marketing', order: 3},
    {name: 'Avisar Anunciante', resp: 'Financeiro', order: 4}
]
WHITELABEL_DEFAULT_CITY = 4730 # Xanxerê
WHITELABEL_MAPS="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1779.4361758263146!2d-52.40540973015317!3d-26.875796370641947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1435585827633"
WHITELABEL_ABOUT='O site da CDL Xanxerê é uma vitrine virtual que tem como papel levar marcas regionais para o ambiente global da internet. Pensada para comerciantes, prestadores de serviço e anunciantes, o portal é uma ferramenta que aproxima as empresas dos seus clientes, encurtando caminhos.'
WHITELABEL_SPC="https://spcxanxere.cdl-sc.org.br/spc-web/login/SC016"
WHITELABEL_COMMISSION_BANNER = 50.0
WHITELABEL_COMMISSION_PLAN = 10.0
WHITELABEL_COMMISSION_SMS = 0.14
WHITELABEL_NEWS='http://xanxere.cdl-sc.org.br/?modo=noticias'
WHITELABEL_ANALYTICS="UA-54782697-3"
