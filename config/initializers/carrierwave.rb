CarrierWave.configure do |config|

  # config.cache_dir = "#{Rails.root}/tmp/uploads"
  # config.root = Rails.root.join('tmp')
  # config.cache_dir = 'carrierwave'

  config.fog_credentials = {
    provider: 'AWS',
    aws_access_key_id: ENV['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
    region: ENV['AWS_S3_REGION']
  }

  # For testing, upload files to local `tmp` folder.
  if Rails.env.test? || Rails.env.development?
    config.storage = :file
    config.enable_processing = true
    # config.root = "#{Rails.root}/tmp"
    config.cache_dir = "#{Rails.root}/tmp/uploads"
  elsif Rails.env.production?
    config.storage = :fog
    config.fog_directory = ENV['AWS_S3_BUCKET']
    # config.asset_host = "https://s3-sa-east-1.amazonaws.com/#{ENV['AWS_S']3_BUCKET}" #'http://assets-cdn.clictem.com'
    # config.asset_host = "//cdn.clictem.com"
    config.asset_host = "//#{ENV['AWS_S3_BUCKET']}.s3.amazonaws.com"
    config.fog_public = true
    config.fog_attributes = { 'Cache-Control' => 'max-age=315576000' }
    config.cache_dir = "#{Rails.root}/tmp/uploads"
  end
end



# CORS Original do S3
# <?xml version="1.0" encoding="UTF-8"?>
# <CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
#     <CORSRule>
#         <AllowedOrigin>*</AllowedOrigin>
#         <AllowedMethod>GET</AllowedMethod>
#         <MaxAgeSeconds>3000</MaxAgeSeconds>
#         <AllowedHeader>Authorization</AllowedHeader>
#     </CORSRule>
# </CORSConfiguration>
