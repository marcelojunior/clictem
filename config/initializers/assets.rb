Rails.application.config.assets.version = '1.0'

Rails.application.config.assets.precompile += %w(
            manager/theme/main.css
            manager/main.js
            my_account.js
            my_account.css
            print.css
            whitelabel/*
    )

Rails.application.config.angular_templates.module_name    = 'templates'
# Rails.application.config.angular_templates.ignore_prefix  = 'templates/'
Rails.application.config.angular_templates.markups        = %w(erb haml slim)
# Rails.application.config.angular_templates.htmlcompressor = {
#   :remove_quotes => true
# }

Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'components')

Rails.application.config.assets.precompile << %r(bootstrap-sass-official/assets/fonts/bootstrap/[\w-]+\.(?:eot|svg|ttf|woff2?)$)

Rails.application.assets.context_class.class_eval do
  include ApplicationHelper
  include ActionView::Helpers
  include Rails.application.routes.url_helpers
end
