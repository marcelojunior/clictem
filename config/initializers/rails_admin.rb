# RailsAdmin.config do |config|

#   ### Popular gems integration

#   ## == Devise ==
#   config.authenticate_with do
#     warden.authenticate! scope: :user
#   end
#   config.current_user_method(&:current_user)

#   ## == Cancan ==
#   config.authorize_with :cancan

#   ## == PaperTrail ==
#   config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

#   config.main_app_name = ['Clictem', 'Admin']

#   ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

#   config.actions do
#     dashboard                     # mandatory
#     index                         # mandatory
#     new
#     export
#     bulk_delete
#     show
#     edit
#     delete
#     show_in_app

#     ## With an audit adapter, you can add:
#     history_index
#     history_show
#   end

#   config.model User do
#     list do
#       field :id
#       field :email
#       field :name
#       field :cnpj
#     end

#     edit do
#       field :logo
#       field :email
#       field :name
#       field :cnpj
#       field :password
#       field :password_confirmation
#       field :cnpj
#       field :business_name
#       field :ie
#       field :phone
#       field :address_street
#       field :address_number
#       field :address_district
#       field :address_zipcode
#       field :city
#       field :state
#       field :kind
#       field :products
#     end
#   end


#   config.model Manager::Ad::Product do
#     list do
#       field :id
#       field :name
#       field :user
#     end

#     edit do
#       field :user
#       field :department
#       field :category
#       field :sub_category
#       field :name
#       field :brand
#       field :on_request, :enum do
#         enum do
#           [['Exibir valor', false], ['Consulte o Preço', true]]
#         end
#         default_value false
#       end
#       field :payment_description
#       field :offer_price
#       field :price
#       field :code
#       field :is_old, :enum do
#         enum do
#           [['Produto usado', false], ['Produto novo', true]]
#         end
#         default_value false
#       end
#       field :description, :ck_editor
#       field :image1
#       field :image2
#       field :image3
#       field :image4
#       field :published
#     end
#   end


#   # Categoria
#   config.model Manager::Ad::Category do
#     object_label_method do
#       :category_title
#     end
#   end

#   def category_title
#     "#{self.department.name} / #{self.name}"
#   end

#   # Subcategoria
#   config.model Manager::Ad::SubCategory do
#     object_label_method do
#       :sub_category_title
#     end
#   end

#   def sub_category_title
#     "#{self.category.department.name} / #{self.category.name} / #{self.name}"
#   end




# end
