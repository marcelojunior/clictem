ENV.each do |key, value|
  env key, value
end

if ENV['PWD'] =~ /clictem_production/

  every 10.minutes do
    command "cd #{ENV['PWD']} && bundle exec rake send_sms RAILS_ENV=production"
  end

  every 10.minutes do
    command "cd #{ENV['PWD']} && bundle exec rake sms_update_status RAILS_ENV=production"
  end

  every 10.minutes do
    command "cd #{ENV['PWD']} && bundle exec rake sms_response RAILS_ENV=production"
  end

  every 10.minutes do
    command "cd #{ENV['PWD']} && bundle exec rake intercom_update RAILS_ENV=production"
  end

  every 1.day, at: '3:00 am' do
    command "cd #{ENV['PWD']} && bundle exec rake reorder_products RAILS_ENV=production"
  end

  every 1.day, at: '1:00 pm' do
    command "cd #{ENV['PWD']} && bundle exec rake poor_views RAILS_ENV=production"
  end

  every 1.day, at: '11:00 am' do
    command "cd #{ENV['PWD']} && bundle exec rake sms_birthday RAILS_ENV=production"
  end

  every 1.day, at: '2:00 pm' do
    command "cd #{ENV['PWD']} && bundle exec rake pj_more_products RAILS_ENV=production"
  end

  every 1.day, at: '1:00 pm' do
    command "cd #{ENV['PWD']} && bundle exec rake report_complete RAILS_ENV=production"
  end

  every 1.day, at: '1:00 pm' do
    command "cd #{ENV['PWD']} && bundle exec rake report_basic RAILS_ENV=production"
  end

  every 1.day, at: '4:00 am' do
    command "cd #{ENV['PWD']} && bundle exec rake offer_update_all RAILS_ENV=production"
  end

  every 1.day, at: '4:00 am' do
    command "cd #{ENV['PWD']}/log && rm *"
  end

end
