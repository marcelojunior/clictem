regex = /(\(\d{2}\)\s*)(\d{4})(\-\d{4})/
Customer.all.each do |customer|
  if (customer.cellphone =~ regex)
    puts "Customer #{customer.name} - #{customer.cellphone}"
    customer.cellphone = customer.cellphone.gsub(regex, '\1 9\2\3')
    customer.save
    puts "Customer #{customer.name} - #{customer.cellphone}"
  end
end


regex = /(\(\d{2}\))\s*(\d{4})(\-)(\d{1})(\d{4})/
Customer.all.each do |customer|
  if (customer.cellphone =~ regex)
    puts "Customer #{customer.name} - #{customer.cellphone}"
    customer.cellphone = customer.cellphone.gsub(regex, '\1 \2\4-\5')
    customer.save
    puts "Customer #{customer.name} - #{customer.cellphone}"
  end
end


regex = /(\(\d{2}\))\s*(\d{4})(\-)(\d{4})/
Customer.all.each do |customer|
  if (customer.cellphone =~ regex)
    puts "Customer #{customer.name} - #{customer.cellphone}"
    customer.cellphone = customer.cellphone.gsub(regex, '\1 9\2-\4')
    customer.save
    puts "Customer #{customer.name} - #{customer.cellphone}"
  end
end
