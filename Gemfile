source 'https://rubygems.org'
ruby '2.2.5'

gem 'net-ssh'

gem 'rails', '4.1.9'

gem 'pg', '~> 0.18.1' #0.18.3

gem 'puma', '~> 2.14.0' #, require: false

# https://devcenter.heroku.com/articles/rails-unicorn
# gem 'rack-timeout', '~> 0.2.4'

# Use SCSS for stylesheets
# gem 'bootstrap-sass', '~> 3.3.3'
# gem 'autoprefixer-rails', '~> 5.1.7'
gem 'sass', '~> 3.4.18'
gem 'sass-rails', '~> 5.0.4'
gem 'compass-rails', '~> 2.0.5'
gem 'oily_png', '~> 1.1.2', require: false

gem 'uglifier', '~> 2.5.0'
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer',  platforms: :ruby, require: false

gem "asset_sync", '~> 1.1.0'
gem 'jquery-rails', '~> 3.1.4' #4.0.5

# Impede a exclusão definitiva de um registro
gem "paranoia", "~> 2.1.3"

# Clonagem de registros
gem 'deep_cloneable', '~> 2.1.1'

# Paginação
gem 'kaminari', '~> 0.16.3'
gem 'kaminari-bootstrap', '~> 3.0.1'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.2.3'

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.10', require: false

# Gerar código de barras
# gem 'barby', '~> 0.6.2'

gem 'intercom-rails', '0.2.28'
gem 'intercom', "~> 2.4.4"

gem 'whenever'

gem 'fake'

# gem 'rails_admin', '~> 0.6.6'
gem 'devise', '~> 3.5.6' #3.5.2
gem 'cancan', '~> 1.6.10'
gem 'paper_trail', '3.0.2', require: false
gem 'foreman', '~> 0.75.0', require: false
gem 'omniauth-facebook', '~> 2.0.0'
gem "koala", "~> 1.10.0rc"

# Formatação de dinheiro
gem 'brazilian-rails', '~> 3.3.0'

# Datepicker
gem 'momentjs-rails', '~> 2.9',  :github => 'derekprior/momentjs-rails' # 2.10.3 6db559c
gem 'datetimepicker-rails', github: 'zpaulovics/datetimepicker-rails', branch: 'master', submodules: true

gem 'carrierwave', '~> 0.10.0'
gem 'carrierwave-processing'
gem 'carrierwave-base64'
gem 's3_direct_upload'
gem "mini_magick", '~> 4.0.1', require: false
gem "fog", "~> 1.24.0", require: false

gem 'sidekiq', '~> 4.1.1'
gem 'sidetiq', '~> 0.7.2'
gem 'sinatra', require: false
# gem 'slim', require: false
# gem 'redis', require: false
# gem 'shoryuken', '1.0.2'

#  https://github.com/influitive/apartment
# Multitenancy for Rails and ActiveRecord
gem 'apartment', '~> 1.0.2'

# Gráficos
gem 'gchartrb'  # E-Mail
gem "chartkick" # Dashboard

# Agrupar por data
gem 'groupdate'

# Parse de arquivos Excel
gem 'roo-xls', require: false
gem 'roo', '2.1.1', require: false

# Monitorar erros
# gem 'airbrake'
# gem 'bugsnag'

# Acessar conta de outros usuários
gem "switch_user"

# gem 'ckeditor_rails', '4.4.3'
# gem 'redactor-rails', '0.4.5'

# Create ENV variables
# gem 'figaro', '~> 1.1.0'
gem 'dotenv-rails'

# Integrating with mandrill api
gem 'mandrill-api', '~> 1.0.53'
gem 'mandrill-rails'

# Serialização dos objetos
gem "active_model_serializers", "~> 0.8.0"

# Create slugs
gem 'friendly_id', '~> 5.0.4' #5.1.0

gem 'meta-tags', '~> 2.0.0'

# Utilizado nas buscas
gem 'pg_search', '~> 0.7.9' # 1.0.5

# Evitar erro Cross-Origin Resource Sharing (CORS)
gem 'rack-cors', :require => 'rack/cors'

# Caches
# heroku addons:add memcachier:dev
gem 'dalli', '2.7.2'
gem 'dalli-elasticache', '0.1.2'
# gem 'memcachier', '0.0.2', require: false
gem 'kgio', '2.9.2', require: false
gem 'rack-cache', require: false

# TODO ver com REDIS = http://goo.gl/Io0uRy

# gem "rack-offline"

gem 'inherited_resources', '~> 1.6.0'
gem 'responders', '~> 1.1.2' #1.1.2
gem 'has_scope', '~> 0.6.0.rc', require: false
gem 'simple_form', '~> 3.1.0.rc2'
gem 'cocoon', '~> 1.2.6' # Criar mestre-detalhe dinamicamente em forms

gem 'angular-rails-templates', '0.1.2'
gem 'ngannotate-rails', '~> 0.9.7'
# gem 'htmlcompressor', '~> 0.1.2'

group :development, :test do
  gem 'rspec', '~> 3.3.0'
  gem 'rspec-rails', '~> 3.3.3'
  gem "factory_girl_rails", "~> 4.5.0"

  gem 'pry', '~> 0.10.2'
  gem 'pry-rails', '~> 0.3.4'
  gem 'pry-rescue', '~> 1.4.1'

  gem "spring-commands-rspec", '~> 1.0.4', require: false

  # gem 'thin', '~> 1.6.3'
  # gem 'sqlite3', '~> 1.3.9'

  gem 'habtm_generator', require: false
end

group :development do
  gem 'better_errors', '~> 2.1.1'
  gem 'binding_of_caller', require: false
  gem 'quiet_assets', '~> 1.1.0'

  # help to kill N+1 queries and unused eager loading
  gem "bullet", '~> 4.14.4'

  # Find index missings
  gem "lol_dba", '~> 2.0.0'

  # Web Console is a debugging tool for your Ruby on Rails applications.
  gem 'web-console', '~> 2.2.1'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring', '~> 1.3.4', require: false

  gem 'guard', '~> 2.13.0'
  gem 'guard-bundler', '~> 2.1.0', require: false
  gem 'guard-rails', '~> 0.7.1'
  gem 'guard-rspec', '~> 4.5.0', require: false
  # gem 'mail_view'
  # gem 'mailcatcher', '~> 0.5.12'
  gem 'rb-fchange', :require=>false
  gem 'rb-fsevent', :require=>false
  gem 'rb-inotify', :require=>false

  # https://github.com/dejan/rails_panel
  gem 'meta_request', '~> 0.3.4'

  # Monitorar performance das páginas
  # gem 'rack-mini-profiler'

  # gem 'thin'
end

group :production do
  gem 'rails_12factor', '~> 0.0.3', require: false
  # gem 'unicorn', '~> 4.8.3'
  gem 'newrelic_rpm', '~> 3.10.0.279'

  gem 'rollbar', '~> 2.11.4'

  # gem "unicorn", '~> 4.8.3'
  # gem "unicorn-rails"
end

group :test do
  gem 'capybara', '~> 2.5.0', require: false
  gem 'database_cleaner', '~> 1.5.0', require: false
  gem 'faker', '~> 1.4.1'
  gem 'launchy', '~> 2.4.2'
  gem 'selenium-webdriver', '~> 2.42.0', require: false
end


# Avaliar gem instaladas
gem 'gem_bench', :group => :console, require: false



# https://github.com/kangguru/rack-google-analytics
# https://github.com/mbleigh/acts-as-taggable-on
# https://github.com/iain/http_accept_language
# https://github.com/brainspec/enumerize
# https://github.com/ncuesta/dropzonejs-rails
# https://github.com/parndt/seo_meta
# https://github.com/ctide/devise_lastseenable
# https://github.com/bmabey/email-spec
# https://github.com/dejan/rails_panel
